# Contour

## Introduction
App for creating 3D-models using embedded primitives and custom created user snippets.

## Stack

### IOS
**VIPER** - respecting Uncle Bob.💥	

**Realm** - fastest database.💫

**SwiftLint** - appreciating clean code.🤙

**PinLayout** - extra fast layout. 🚄

**Metal** - extra fast GPU drawing. 💣

## Design

**Figma**

In this project i was trying to implement user-friendly interface.

You can check design [here](https://www.figma.com/file/nKOja1Gb6VZNt9DL0qRyMZ/Contour?node-id=0%3A1) 

Course project for Computer Graphics, BMSTU, 2020
