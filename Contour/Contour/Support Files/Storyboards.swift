enum Storyboard {
    case menu
    case createProject
    case canvas
    case addPrimitive
    case tutorial
    case objectList
}

extension Storyboard {
    var name: String {
        switch self {
        case .menu:
            return Constants.menu
        case .createProject:
            return Constants.createProject
        case .canvas:
            return Constants.canvas
        case .addPrimitive:
            return Constants.addPrimitive
        case .tutorial:
            return Constants.tutorial
        case .objectList:
            return Constants.objectList
        }
    }
}

private struct Constants {
    static let menu = "Menu"
    static let createProject = "CreateProject"
    static let canvas = "Canvas"
    static let addPrimitive = "AddPrimitive"
    static let tutorial = "Tutorial"
    static let objectList = "ObjectList"
}
