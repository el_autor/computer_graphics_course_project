final class OpenProjectInteractor {

    weak var output: OpenProjectInteractorOutput?

}

extension OpenProjectInteractor: OpenProjectInteractorInput {
    func loadExistingProjects() -> [OpenProjectExistingProjectData] {
        let projects = DataService.shared.getAllProjects()

        return projects.map {
            OpenProjectExistingProjectData(projectName: $0.name ?? String(), authorName: $0.authorName ?? String(), touchedTime: $0.lastModified)
        }
    }
}
