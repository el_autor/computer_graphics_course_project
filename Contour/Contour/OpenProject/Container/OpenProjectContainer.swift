import UIKit

final class OpenProjectContainer {
    let viewController: UIViewController

    class func assemble(context: OpenProjectContext) -> OpenProjectContainer {
        let viewController = OpenProjectViewController()

        let presenter = OpenProjectPresenter()
        let interactor = OpenProjectInteractor()
        let router = OpenProjectRouter()

        presenter.interactor = interactor
        presenter.router = router
        presenter.view = viewController

        interactor.output = presenter

        router.viewController = viewController

        viewController.output = presenter

        return OpenProjectContainer(viewController: viewController)
    }

    private init(viewController: OpenProjectViewController) {
        self.viewController = viewController
    }
}

struct OpenProjectContext {

}
