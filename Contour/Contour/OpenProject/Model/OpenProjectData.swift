import UIKit

struct OpenProjectExistingProjectData {
    var projectName: String

    var authorName: String

    var touchedTime: String
}
