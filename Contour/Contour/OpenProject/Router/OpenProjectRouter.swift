import UIKit

final class OpenProjectRouter {
    weak var viewController: UIViewController?
}

extension OpenProjectRouter: OpenProjectRouterInput {
    func showMenuScreen() {
        viewController?.dismiss(animated: true, completion: nil)
    }

    func showProject(projectData: OpenProjectExistingProjectData) {
        let container = CanvasContainer.assemble(context: CanvasContext(projectName: projectData.projectName,
                                                                        authorName: projectData.authorName))

        container.viewController.modalPresentationStyle = .fullScreen

        viewController?.present(container.viewController, animated: true, completion: nil)
    }
}
