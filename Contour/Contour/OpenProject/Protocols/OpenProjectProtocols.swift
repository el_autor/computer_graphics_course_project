import Foundation

protocol OpenProjectViewInput: AnyObject {

}

protocol OpenProjectViewOutput: AnyObject {
    func didTapBackToMenuButton()

    func didLoadView()

    func getProjectCollectionViewSize() -> Int

    func project(for indexpath: IndexPath) -> OpenProjectExistingProjectData

    func didTapCell(indexPath: IndexPath)
}

protocol OpenProjectInteractorInput: AnyObject {
    func loadExistingProjects() -> [OpenProjectExistingProjectData]
}

protocol OpenProjectInteractorOutput: AnyObject {

}

protocol OpenProjectRouterInput: AnyObject {
    func showMenuScreen()

    func showProject(projectData: OpenProjectExistingProjectData)
}
