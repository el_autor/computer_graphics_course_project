import UIKit
import PinLayout

final class OpenProjectViewController: UIViewController {

    var output: OpenProjectViewOutput?

    private weak var titleLabel: UILabel!

    private weak var projectsCollectionView: UICollectionView!

    private weak var backToMenuButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupSubviews()

        output?.didLoadView()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        layoutTitleLabel()
        layoutProjectsCollectionView()
        layoutBackToMenuButton()
    }

    private func layoutTitleLabel() {
        titleLabel.pin
            .top(Constants.TitleLabel.topConstant)
            .hCenter()
            .sizeToFit()
    }

    private func layoutProjectsCollectionView() {
        projectsCollectionView.pin
            .vCenter()
            .hCenter()
            .width(Constants.ProjectsCollectionView.widthConstant)
            .height(Constants.ProjectsCollectionView.heightConstant)
    }

    private func layoutBackToMenuButton() {
        backToMenuButton.pin
            .right(Constants.BackToMenuButton.rightConstant)
            .bottom(Constants.BackToMenuButton.bottomConstant)
            .width(Constants.BackToMenuButton.widthConstant)
            .height(Constants.BackToMenuButton.heightConstant)
    }

    private func setupView() {
        view.backgroundColor = Constants.backgroundColor
    }

    private func setupSubviews() {
        setupTitleLabel()
        setupProjectsCollectionView()
        setupBackToMenuButton()
    }

    private func setupTitleLabel() {
        let label = UILabel()

        titleLabel = label
        view.addSubview(titleLabel)

        titleLabel.font = Constants.TitleLabel.font
        titleLabel.text = Constants.TitleLabel.text
        titleLabel.textAlignment = .center
    }

    private func setupProjectsCollectionView() {
        let flowLayout = UICollectionViewFlowLayout()

        flowLayout.itemSize = calcCellSize()
        flowLayout.minimumLineSpacing = Constants.ProjectsCollectionView.minimumLineSpacing
        flowLayout.minimumInteritemSpacing = Constants.ProjectsCollectionView.minimumInteritemSpacing

        let collectionView = UICollectionView(frame: .zero,
                                              collectionViewLayout: flowLayout)

        projectsCollectionView = collectionView
        view.addSubview(projectsCollectionView)

        projectsCollectionView.register(OpenProjectCell.self,
                                        forCellWithReuseIdentifier: Constants.ProjectsCollectionView.cellReuseIdentifier)

        projectsCollectionView.delegate = self
        projectsCollectionView.dataSource = self

        projectsCollectionView.layer.cornerRadius = Constants.ProjectsCollectionView.cornerRadius
        projectsCollectionView.backgroundColor = Constants.backgroundColor
        projectsCollectionView.showsVerticalScrollIndicator = false
    }

    private func calcCellSize() -> CGSize {
        let collectionViewWidth = Constants.ProjectsCollectionView.widthConstant.of(UIScreen.main.bounds.width)
        let collectionViewHeight = Constants.ProjectsCollectionView.heightConstant.of(UIScreen.main.bounds.height)

        let totalSpacingInRow: CGFloat = Constants.ProjectsCollectionView.spacingInRows * CGFloat(Constants.ProjectsCollectionView.itemsInRow - 1)
        let totalSpacingRows: CGFloat = Constants.ProjectsCollectionView.rowsSpacing * CGFloat(Constants.ProjectsCollectionView.rows - 1)

        return CGSize(width: (collectionViewWidth - totalSpacingInRow) / CGFloat(Constants.ProjectsCollectionView.itemsInRow),
                      height: (collectionViewHeight - totalSpacingRows) / CGFloat(Constants.ProjectsCollectionView.rows))
    }

    private func setupBackToMenuButton() {
        let button = UIButton()

        backToMenuButton = button
        view.addSubview(backToMenuButton)

        backToMenuButton.backgroundColor = Constants.BackToMenuButton.backgroundColor
        backToMenuButton.layer.cornerRadius = Constants.BackToMenuButton.cornerRadius
        backToMenuButton.titleLabel?.font = Constants.BackToMenuButton.font
        backToMenuButton.setTitleColor(.black, for: .normal)
        backToMenuButton.setTitle(Constants.BackToMenuButton.title, for: .normal)

        backToMenuButton.addTarget(self, action: #selector(didTapBackToMenuButton), for: .touchUpInside)
    }

    @objc
    private func didTapBackToMenuButton() {
        output?.didTapBackToMenuButton()
    }
}

extension OpenProjectViewController: OpenProjectViewInput {

}

extension OpenProjectViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        output?.didTapCell(indexPath: indexPath)
    }
}

extension OpenProjectViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return output?.getProjectCollectionViewSize() ?? .zero
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.ProjectsCollectionView.cellReuseIdentifier, for: indexPath) as? OpenProjectCell else {
            return OpenProjectCell()
        }

        guard let viewModel = output?.project(for: indexPath) else {
            return cell
        }

        cell.configure(model: viewModel)

        return cell
    }
}

extension OpenProjectViewController {
    private struct Constants {
        static let backgroundColor: UIColor = UIColor(red: 94 / 255,
                                                      green: 92 / 255,
                                                      blue: 230 / 255,
                                                      alpha: 1)

        struct TitleLabel {
            static let font: UIFont? = UIFont(name: "Chalkduster", size: 35)
            static let text: String = "Choose project"

            static let topConstant: Percent = 8%
        }

        struct ProjectsCollectionView {
            static let cellReuseIdentifier: String = "OpenObject"

            static let cornerRadius: CGFloat = 10

            static let widthConstant: Percent = 80%
            static let heightConstant: Percent = 60%

            static let minimumLineSpacing: CGFloat = 5
            static let minimumInteritemSpacing: CGFloat = 5

            static let rowsSpacing: CGFloat = 5
            static let spacingInRows: CGFloat = 5

            static let itemsInRow: Int = 5
            static let rows: Int = 3
        }

        struct BackToMenuButton {
            static let backgroundColor: UIColor = UIColor(red: 48 / 255,
                                                          green: 209 / 255,
                                                          blue: 88 / 255,
                                                          alpha: 1)

            static let font: UIFont? = UIFont(name: "DMSans-Regular", size: 25)
            static let cornerRadius: CGFloat = 15
            static let title: String = "To menu"

            static let bottomConstant: CGFloat = 50
            static let rightConstant: CGFloat = 50
            static let widthConstant: CGFloat = 202
            static let heightConstant: CGFloat = 48
        }
    }
}
