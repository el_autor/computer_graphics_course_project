import UIKit
import PinLayout

final class OpenProjectCell: UICollectionViewCell {

    private weak var projectNameLabel: UILabel!

    private weak var authorNameLabel: UILabel!

    private weak var touchedDateLabel: UILabel!

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupCell()
        setupSubviews()
    }

    required init?(coder: NSCoder) {
        fatalError("Coder not implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        layoutProjectNameLabel()
        layoutAuthorNameLabel()
        layoutTouchedDateLabel()
    }

    private func layoutProjectNameLabel() {
        projectNameLabel.pin
            .top(Constants.ProjectNameLabel.topConstant)
            .hCenter()
            .width(Constants.ProjectNameLabel.widthConstant)
            .sizeToFit()
    }

    private func layoutAuthorNameLabel() {
        authorNameLabel.pin
            .top(Constants.AuthorNameLabel.topConstant)
            .left(Constants.AuthorNameLabel.leftConstant)
            .width(Constants.AuthorNameLabel.widthConstant)
            .sizeToFit()
    }

    private func layoutTouchedDateLabel() {
        touchedDateLabel.pin
            .top(Constants.TouchedDateLabel.topConstant)
            .left(Constants.TouchedDateLabel.leftConstant)
            .width(Constants.TouchedDateLabel.widthConstant)
            .height(Constants.TouchedDateLabel.heightConstant)
    }

    private func setupCell() {
        clipsToBounds = true
        backgroundColor = Constants.backgroundColor
        layer.cornerRadius = Constants.cornerRadius
    }

    private func setupSubviews() {
        setupProjectNameLabel()
        setupAuthorNameLabel()
        setupTouchedDateLabel()
    }

    private func setupProjectNameLabel() {
        let label = UILabel()

        projectNameLabel = label
        addSubview(projectNameLabel)

        projectNameLabel.font = Constants.ProjectNameLabel.font
        projectNameLabel.textAlignment = .center
    }

    private func setupAuthorNameLabel() {
        let label = UILabel()

        authorNameLabel = label
        addSubview(authorNameLabel)

        authorNameLabel.font = Constants.AuthorNameLabel.font
        authorNameLabel.textAlignment = .left
    }

    private func setupTouchedDateLabel() {
        let label = UILabel()

        touchedDateLabel = label
        addSubview(touchedDateLabel)

        touchedDateLabel.font = Constants.TouchedDateLabel.font
        touchedDateLabel.textAlignment = .left
        touchedDateLabel.text = Constants.TouchedDateLabel.text
        touchedDateLabel.numberOfLines = 2
    }

    func configure(model: OpenProjectExistingProjectData) {
        projectNameLabel.text = model.projectName
        authorNameLabel.text = Constants.AuthorNameLabel.text + model.authorName
        touchedDateLabel.text = "Touched: " + model.touchedTime
    }
}

extension OpenProjectCell {
    private struct Constants {
        static let backgroundColor: UIColor = UIColor(red: 48 / 255,
                                                      green: 209 / 255,
                                                      blue: 88 / 255,
                                                      alpha: 1)

        static let cornerRadius: CGFloat = 10

        struct ProjectNameLabel {
            static let font: UIFont? = UIFont(name: "DMSans-Regular", size: 25)

            static let topConstant: Percent = 25%
            static let widthConstant: Percent = 100%
        }

        struct AuthorNameLabel {
            static let font: UIFont? = UIFont(name: "DMSans-Regular", size: 20)
            static let text: String = "Author: "

            static let topConstant: Percent = 50%
            static let leftConstant: Percent = 10%
            static let widthConstant: Percent = 50%
        }

        struct TouchedDateLabel {
            static let font: UIFont? = UIFont(name: "DMSans-Regular", size: 20)
            static let text: String = "Touched: "
            static let linesNumber: Int = 2

            static let topConstant: Percent = 60%
            static let leftConstant: Percent = 10%
            static let widthConstant: Percent = 80%
            static let heightConstant: Percent = 40%
        }
    }
}
