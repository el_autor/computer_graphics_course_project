import Foundation

final class OpenProjectPresenter {

    weak var view: OpenProjectViewInput?
    var interactor: OpenProjectInteractorInput?
    var router: OpenProjectRouterInput?

    private var model: [OpenProjectExistingProjectData]?

    init() {

    }
}

extension OpenProjectPresenter: OpenProjectInteractorOutput {

}

extension OpenProjectPresenter: OpenProjectViewOutput {
    func didTapBackToMenuButton() {
        router?.showMenuScreen()
    }

    func didLoadView() {
        model = interactor?.loadExistingProjects()
    }

    func getProjectCollectionViewSize() -> Int {
        return model?.count ?? .zero
    }

    func project(for indexpath: IndexPath) -> OpenProjectExistingProjectData {
        guard let model = model else {
            return OpenProjectExistingProjectData(projectName: "Default",
                                                  authorName: "Default",
                                                  touchedTime: "Default")
        }

        return model[indexpath.row]
    }

    func didTapCell(indexPath: IndexPath) {
        guard let model = model else {
            return
        }

        router?.showProject(projectData: model[indexPath.row])
    }
}
