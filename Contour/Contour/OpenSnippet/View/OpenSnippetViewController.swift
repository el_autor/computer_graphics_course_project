import UIKit
import PinLayout

final class OpenSnippetViewController: UIViewController {
	var output: OpenSnippetViewOutput?

    private weak var titleLabel: UILabel!

    private weak var snippetsCollectionView: UICollectionView!

    private weak var backToMenuButton: UIButton!

	override func viewDidLoad() {
		super.viewDidLoad()

        setupView()
        setupSubviews()

        output?.didLoadView()
	}

    private func setupView() {
        view.backgroundColor = Constants.backgroundColor
    }

    private func setupSubviews() {
        setupTitleLabel()
        setupProjectsCollectionView()
        setupBackToMenuButton()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        layoutTitleLabel()
        layoutProjectsCollectionView()
        layoutBackToMenuButton()
    }

    private func layoutTitleLabel() {
        titleLabel.pin
            .top(Constants.TitleLabel.topConstant)
            .hCenter()
            .sizeToFit()
    }

    private func layoutProjectsCollectionView() {
        snippetsCollectionView.pin
            .vCenter()
            .hCenter()
            .width(Constants.SnippetsCollectionView.widthConstant)
            .height(Constants.SnippetsCollectionView.heightConstant)
    }

    private func layoutBackToMenuButton() {
        backToMenuButton.pin
            .right(Constants.BackToMenuButton.rightConstant)
            .bottom(Constants.BackToMenuButton.bottomConstant)
            .width(Constants.BackToMenuButton.widthConstant)
            .height(Constants.BackToMenuButton.heightConstant)
    }

    private func setupTitleLabel() {
        let label = UILabel()

        titleLabel = label
        view.addSubview(titleLabel)

        titleLabel.font = Constants.TitleLabel.font
        titleLabel.text = Constants.TitleLabel.text
        titleLabel.textAlignment = .center
    }

    private func setupProjectsCollectionView() {
        let flowLayout = UICollectionViewFlowLayout()

        flowLayout.itemSize = calcCellSize()
        flowLayout.minimumLineSpacing = Constants.SnippetsCollectionView.minimumLineSpacing
        flowLayout.minimumInteritemSpacing = Constants.SnippetsCollectionView.minimumInteritemSpacing

        let collectionView = UICollectionView(frame: .zero,
                                              collectionViewLayout: flowLayout)

        snippetsCollectionView = collectionView
        view.addSubview(snippetsCollectionView)

        snippetsCollectionView.register(OpenProjectCell.self,
                                        forCellWithReuseIdentifier: Constants.SnippetsCollectionView.cellReuseIdentifier)

        snippetsCollectionView.delegate = self
        snippetsCollectionView.dataSource = self

        snippetsCollectionView.layer.cornerRadius = Constants.SnippetsCollectionView.cornerRadius
        snippetsCollectionView.backgroundColor = Constants.backgroundColor
        snippetsCollectionView.showsVerticalScrollIndicator = false
    }

    private func calcCellSize() -> CGSize {
        let collectionViewWidth = Constants.SnippetsCollectionView.widthConstant.of(UIScreen.main.bounds.width)
        let collectionViewHeight = Constants.SnippetsCollectionView.heightConstant.of(UIScreen.main.bounds.height)

        let totalSpacingInRow: CGFloat = Constants.SnippetsCollectionView.spacingInRows * CGFloat(Constants.SnippetsCollectionView.itemsInRow - 1)
        let totalSpacingRows: CGFloat = Constants.SnippetsCollectionView.rowsSpacing * CGFloat(Constants.SnippetsCollectionView.rows - 1)

        return CGSize(width: (collectionViewWidth - totalSpacingInRow) / CGFloat(Constants.SnippetsCollectionView.itemsInRow),
                      height: (collectionViewHeight - totalSpacingRows) / CGFloat(Constants.SnippetsCollectionView.rows))
    }

    private func setupBackToMenuButton() {
        let button = UIButton()

        backToMenuButton = button
        view.addSubview(backToMenuButton)

        backToMenuButton.backgroundColor = Constants.BackToMenuButton.backgroundColor
        backToMenuButton.layer.cornerRadius = Constants.BackToMenuButton.cornerRadius
        backToMenuButton.titleLabel?.font = Constants.BackToMenuButton.font
        backToMenuButton.setTitleColor(.black, for: .normal)
        backToMenuButton.setTitle(Constants.BackToMenuButton.title, for: .normal)

        backToMenuButton.addTarget(self, action: #selector(didTapBackToMenuButton), for: .touchUpInside)
    }

    @objc
    private func didTapBackToMenuButton() {
        output?.didTapBackToMenuButton()
    }
}

extension OpenSnippetViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        output?.didTapCell(indexPath: indexPath)
    }
}

extension OpenSnippetViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return output?.getProjectCollectionViewSize() ?? .zero
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.SnippetsCollectionView.cellReuseIdentifier, for: indexPath) as? OpenProjectCell else {
            return OpenProjectCell()
        }

        guard let viewModel = output?.snippet(for: indexPath) else {
            return cell
        }

        cell.configure(model: viewModel)
        cell.backgroundColor = Constants.BackToMenuButton.backgroundColor

        return cell
    }
}

extension OpenSnippetViewController: OpenSnippetViewInput {
}

extension OpenSnippetViewController {
    private struct Constants {
        static let backgroundColor: UIColor = UIColor(red: 94 / 255,
                                                      green: 92 / 255,
                                                      blue: 230 / 255,
                                                      alpha: 1)

        struct TitleLabel {
            static let font: UIFont? = UIFont(name: "Chalkduster", size: 35)
            static let text: String = "Choose snippet"

            static let topConstant: Percent = 8%
        }

        struct SnippetsCollectionView {
            static let cellReuseIdentifier: String = "OpenObject"

            static let cornerRadius: CGFloat = 10

            static let widthConstant: Percent = 80%
            static let heightConstant: Percent = 60%

            static let minimumLineSpacing: CGFloat = 5
            static let minimumInteritemSpacing: CGFloat = 5

            static let rowsSpacing: CGFloat = 5
            static let spacingInRows: CGFloat = 5

            static let itemsInRow: Int = 5
            static let rows: Int = 3
        }

        struct BackToMenuButton {
            static let backgroundColor: UIColor = UIColor(red: 10 / 255,
                                                          green: 132 / 255,
                                                          blue: 255 / 255,
                                                          alpha: 1)

            static let font: UIFont? = UIFont(name: "DMSans-Regular", size: 25)
            static let cornerRadius: CGFloat = 15
            static let title: String = "To menu"

            static let bottomConstant: CGFloat = 50
            static let rightConstant: CGFloat = 50
            static let widthConstant: CGFloat = 202
            static let heightConstant: CGFloat = 48
        }
    }
}
