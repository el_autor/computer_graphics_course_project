import UIKit

final class OpenSnippetRouter {
    weak var viewController: UIViewController?
}

extension OpenSnippetRouter: OpenSnippetRouterInput {
    func showMenuScreen() {
        viewController?.dismiss(animated: true, completion: nil)
    }

    func showProject(projectData: OpenProjectExistingProjectData) {
        let container = SnippetCanvasContainer.assemble(with: SnippetCanvasContext(name: projectData.projectName,
                                                                                   authorName: projectData.authorName))

        container.viewController.modalPresentationStyle = .fullScreen

        viewController?.present(container.viewController, animated: true, completion: nil)
    }
}
