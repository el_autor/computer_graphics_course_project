import Foundation

final class OpenSnippetPresenter {
	weak var view: OpenSnippetViewInput?

	private let router: OpenSnippetRouterInput
	private let interactor: OpenSnippetInteractorInput

    private var model: [OpenProjectExistingProjectData]?

    init(router: OpenSnippetRouterInput, interactor: OpenSnippetInteractorInput) {
        self.router = router
        self.interactor = interactor
    }
}

extension OpenSnippetPresenter: OpenSnippetViewOutput {
    func didTapBackToMenuButton() {
        router.showMenuScreen()
    }

    func didTapCell(indexPath: IndexPath) {
        guard let model = model else {
            return
        }

        router.showProject(projectData: model[indexPath.row])
    }

    func getProjectCollectionViewSize() -> Int {
        return model?.count ?? 0
    }

    func snippet(for indexpath: IndexPath) -> OpenProjectExistingProjectData {
        guard let model = model else {
            return OpenProjectExistingProjectData(projectName: "Default", authorName: "Default", touchedTime: "Default")
        }

        return model[indexpath.row]
    }

    func didLoadView() {
        model = interactor.loadExistingSnippets()
    }
}

extension OpenSnippetPresenter: OpenSnippetInteractorOutput {
}
