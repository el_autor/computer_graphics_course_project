import UIKit

final class OpenSnippetContainer {
	let viewController: UIViewController
	private(set) weak var router: OpenSnippetRouterInput!

	class func assemble(with context: OpenSnippetContext) -> OpenSnippetContainer {
        let router = OpenSnippetRouter()
        let interactor = OpenSnippetInteractor()
        let presenter = OpenSnippetPresenter(router: router, interactor: interactor)
        let viewController = OpenSnippetViewController()

        viewController.output = presenter
        presenter.view = viewController
        interactor.output = presenter
        router.viewController = viewController

        return OpenSnippetContainer(view: viewController, router: router)
	}

    private init(view: UIViewController, router: OpenSnippetRouterInput) {
		self.viewController = view
		self.router = router
	}
}

struct OpenSnippetContext {
}
