import Foundation

final class OpenSnippetInteractor {
	weak var output: OpenSnippetInteractorOutput?
}

extension OpenSnippetInteractor: OpenSnippetInteractorInput {
    func loadExistingSnippets() -> [OpenProjectExistingProjectData] {
        let projects = DataService.shared.getAllSnippets()

        return projects.map {
            OpenProjectExistingProjectData(projectName: $0.name ?? String(), authorName: $0.authorName ?? String(), touchedTime: $0.lastModified)
        }
    }
}
