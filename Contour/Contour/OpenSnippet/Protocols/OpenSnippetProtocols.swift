import Foundation

protocol OpenSnippetViewInput: AnyObject {
}

protocol OpenSnippetViewOutput: AnyObject {
    func didTapBackToMenuButton()

    func didTapCell(indexPath: IndexPath)

    func getProjectCollectionViewSize() -> Int

    func snippet(for: IndexPath) -> OpenProjectExistingProjectData

    func didLoadView()
}

protocol OpenSnippetInteractorInput: AnyObject {
    func loadExistingSnippets() -> [OpenProjectExistingProjectData]
}

protocol OpenSnippetInteractorOutput: AnyObject {
}

protocol OpenSnippetRouterInput: AnyObject {
    func showMenuScreen()

    func showProject(projectData: OpenProjectExistingProjectData)
}
