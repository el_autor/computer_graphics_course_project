import UIKit

class AddPrimitivePresenter {
    weak var view: AddPrimitiveViewInput?

    var router: AddPrimitiveRouterInput?

    var model: [Shape]?

    init(model: [Shape]) {
        self.model = model
    }
}

extension AddPrimitivePresenter: AddPrimitiveViewOutput {
    func didTapToCanvasButton() {
        router?.showCanvas(with: nil)
    }

    func primitive(at indexPath: IndexPath) -> Shape? {
        return model?[indexPath.row]
    }

    func drawPrimitive(at indexPath: IndexPath) {
        router?.showCanvas(with: model?[indexPath.row])
    }

    func getModelsTotal() -> Int {
        return model?.count ?? .zero
    }
}
