import UIKit

class AddPrimitiveViewController: UIViewController {

    var output: AddPrimitiveViewOutput?

    @IBOutlet private weak var primitivesCollectionView: UICollectionView!

    @IBOutlet private weak var toCanvasButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupSubviews()
    }

    private func setupView() {
        view.backgroundColor = Constants.backgroundColor
    }

    private func setupPrimitivesCollectionView() {
        primitivesCollectionView.delegate = self
        primitivesCollectionView.dataSource = self

        if let layout = primitivesCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
        }

        primitivesCollectionView.register(UINib(nibName: "PrimitiveCell", bundle: nil), forCellWithReuseIdentifier: Constants.cellReuseIdentifier)

        primitivesCollectionView.backgroundColor = Constants.backgroundColor

        primitivesCollectionView.showsVerticalScrollIndicator = false
        primitivesCollectionView.showsHorizontalScrollIndicator = false
    }

    private func setupToCanvasButton() {
        toCanvasButton.backgroundColor = Constants.ToCanvasButton.backgroundColor

        toCanvasButton.layer.cornerRadius = Constants.ToCanvasButton.cornerRadius

        toCanvasButton.setTitle(Constants.ToCanvasButton.title, for: .normal)
        toCanvasButton.setTitleColor(.black, for: .normal)
        toCanvasButton.titleLabel?.font = Constants.ToCanvasButton.font

        toCanvasButton.addTarget(self, action: #selector(didTapToCanvasButton), for: .touchUpInside)
    }

    private func setupSubviews() {
        setupPrimitivesCollectionView()
        setupToCanvasButton()
    }

    @objc
    private func didTapToCanvasButton() {
        output?.didTapToCanvasButton()
    }
}

extension AddPrimitiveViewController: AddPrimitiveViewInput {

}

extension AddPrimitiveViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        output?.drawPrimitive(at: indexPath)
    }
}

extension AddPrimitiveViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return output?.getModelsTotal() ?? .zero
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.cellReuseIdentifier, for: indexPath) as? PrimitiveCell else {
            return UICollectionViewCell()
        }

        guard let viewModel = output?.primitive(at: indexPath) else {
            return cell
        }

        cell.configure(with: viewModel)

        return cell
    }
}

extension AddPrimitiveViewController {
    private struct Constants {
        static let backgroundColor: UIColor = UIColor(red: 94 / 255,
                                                      green: 92 / 255,
                                                      blue: 230 / 255,
                                                      alpha: 1)

        static let cellReuseIdentifier: String = "Primitive"

        struct ToCanvasButton {
            static let backgroundColor: UIColor = UIColor(red: 255 / 255,
                                                          green: 214 / 255,
                                                          blue: 10 / 255,
                                                          alpha: 1)

            static let font: UIFont? = UIFont(name: "DMSans-Regular", size: 25)
            static let cornerRadius: CGFloat = 15
            static let title: String = "To canvas"
        }
    }
}
