import UIKit

class PrimitiveCell: UICollectionViewCell {

    @IBOutlet private weak var view: UIView!

    @IBOutlet private weak var shapeImageView: UIImageView!

    @IBOutlet private weak var shapeLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

        setupView()
        setupSubviews()
    }

    private func setupView() {
        view.layer.cornerRadius = Constants.cornerRadius
        view.backgroundColor = Constants.backgroundColor
    }

    private func setupShapeLabel() {
        shapeLabel.textAlignment = .center
        shapeLabel.font = Constants.ShapeLabel.font
    }

    private func setupShapeImageView() {
        shapeImageView.contentMode = .scaleAspectFit
    }

    private func setupSubviews() {
        setupShapeLabel()
        setupShapeImageView()
    }

    func configure(with data: Shape) {
        guard let name = data.name,
              let imageName = data.imageName else {
            return
        }

        shapeLabel.text = name
        shapeImageView.image = UIImage(named: imageName)
    }
}

extension PrimitiveCell {
    private struct Constants {
        static let cornerRadius: CGFloat = 15
        static let backgroundColor: UIColor = .white

        struct ShapeLabel {
            static let font: UIFont? = UIFont(name: "DMSans-Regular", size: 30)
        }
    }
}
