import UIKit

class AddPrimitiveContainer {
    let viewController: UIViewController

    class func assemble() -> AddPrimitiveContainer {
        let storyboard = UIStoryboard(name: Storyboard.addPrimitive.name, bundle: nil)

        guard let viewController = storyboard.instantiateViewController(withIdentifier: Storyboard.addPrimitive.name) as? AddPrimitiveViewController else {
            fatalError("AddPrimitiveContainer: view controller must be type AddPrimitiveViewController")
        }

        let presenter = AddPrimitivePresenter(model: DataService.shared.getShapes())
        let router = AddPrimitiveRouter()

        presenter.view = viewController
        presenter.router = router

        router.viewController = viewController

        viewController.output = presenter

        return AddPrimitiveContainer(viewController: viewController)
    }

    private init(viewController: AddPrimitiveViewController) {
        self.viewController = viewController
    }
}
