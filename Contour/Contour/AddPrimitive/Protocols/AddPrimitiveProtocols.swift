import Foundation

protocol AddPrimitiveViewInput: class {

}

protocol AddPrimitiveViewOutput: class {
    func didTapToCanvasButton()

    func primitive(at indexPath: IndexPath) -> Shape?

    func drawPrimitive(at indexPath: IndexPath)

    func getModelsTotal() -> Int
}

protocol AddPrimitiveRouterInput: class {
    func showCanvas(with addition: Shape?)
}
