import UIKit

class AddPrimitiveRouter {
    weak var viewController: UIViewController?

    init() {

    }
}

extension AddPrimitiveRouter: AddPrimitiveRouterInput {
    func showCanvas(with addition: Shape?) {
        guard let presentingVC = viewController?.presentingViewController as? CanvasViewController else {
            return
        }

        guard let primitive = addition else {
            viewController?.dismiss(animated: true)
            return
        }

        viewController?.dismiss(animated: true) {
            [unowned presentingVC] in
            presentingVC.output?.didAddedPrimitive(primitive)
        }
    }
}
