import RealmSwift

class DataService {
    static let shared = DataService()

    private lazy var database: Realm = {
        guard let realm = try? Realm() else {
            fatalError("Failed to init Realm")
        }

        return realm
    }()

    private init() {

    }

    private func createVertex(id: Int, x: Float, y: Float, z: Float) -> Vertex {
        let vertex = Vertex()

        vertex.id = id

        vertex.x = x
        vertex.y = y
        vertex.z = z

        return vertex
    }

    private func createConnection(start: Vertex, end: Vertex) -> Connection {
        let connection = Connection()

        connection.startVertex = start
        connection.endVertex = end

        return connection
    }

    private func createVerge(id: Int, connections: [Connection]) -> Verge {
        let verge = Verge()

        verge.id = id

        for connection in connections {
            verge.connections.append(connection)
        }

        return verge
    }

    private func calcCenter(vertices: [Vertex]) -> Vertex {
        guard !vertices.isEmpty else {
            return Vertex()
        }

        var xMin = vertices[.zero].x, xMax = xMin
        var yMin = vertices[.zero].y, yMax = yMin
        var zMin = vertices[.zero].z, zMax = zMin

        for vertex in vertices {
            if vertex.x < xMin {
                xMin = vertex.x
            } else if vertex.x > xMax {
                xMax = vertex.x
            }

            if vertex.y < yMin {
                yMin = vertex.y
            } else if vertex.y > yMax {
                yMax = vertex.y
            }

            if vertex.z < zMin {
                zMin = vertex.z
            } else if vertex.z > zMax {
                zMax = vertex.z
            }
        }

        let center = Vertex()

        center.x = (xMin + xMax) / 2
        center.y = (yMin + yMax) / 2
        center.z = (zMin + zMax) / 2

        return center
    }

    private func createPrimitive(verges: [Verge], vertices: [Vertex]) -> Primitive {
        let primitive = Primitive()

        for verge in verges {
            primitive.verges.append(verge)
        }

        for vertex in vertices {
            primitive.vertices.append(vertex)
        }

        primitive.center = calcCenter(vertices: vertices)

        let color = RealmColor()

        color.r = 0.5
        color.g = 0.5
        color.b = 0.5

        primitive.color = color

        return primitive
    }

    private func createRotationMatrix(angleX: Float, angleY: Float, angleZ: Float) -> [[Float]] {
        return [[cos(angleY) * cos(angleZ),
                 cos(angleX) * sin(angleZ) + sin(angleX) * sin(angleY) * cos(angleZ),
                 sin(angleX) * sin(angleZ) - cos(angleX) * sin(angleY) * cos(angleZ)],
                [-cos(angleY) * sin(angleZ),
                 cos(angleX) * cos(angleZ) + sin(angleX) * sin(angleY) * sin(angleZ),
                 sin(angleX) * cos(angleZ) - cos(angleX) * sin(angleY) * sin(angleZ)],
                [sin(angleY), -sin(angleX) * cos(angleY), cos(angleX) * cos(angleY)]]
    }

    private func createMatrixFromPoint(vertex: Vertex) -> [[Float]] {
        return [[vertex.x],
                [vertex.y],
                [vertex.z]]
    }

    private func multiplyMatrices(matrixA: [[Float]], matrixB: [[Float]]) -> [[Float]] {
        var result = [[Float]](repeating: [Float](repeating: 0, count: matrixB[0].count),
                                    count: matrixA.count)

        for i in 0..<matrixA.count {
            for j in 0..<matrixB[0].count {
                for k in 0..<matrixA[0].count {
                    result[i][j] += matrixA[i][k] * matrixB[k][j]
                }
            }
        }

        return result
    }

    private func move(primitive: Primitive, deltaX: Float, deltaY: Float, deltaZ: Float) {
        primitive.center.x += deltaX
        primitive.center.y += deltaY
        primitive.center.z += deltaZ

        primitive.vertices.forEach { (vertex) in
            vertex.x += deltaX
            vertex.y += deltaY
            vertex.z += deltaZ
        }
    }

    private func makeSnippetCopy(snippet: Snippet) -> Snippet {
        let snippetCopy = Snippet()

        for primitive in snippet.primitives {
            snippetCopy.primitives.append(makePrimitiveCopy(primitive: primitive))
        }

        return snippetCopy
    }

    private func makePrimitiveCopy(primitive: Primitive) -> Primitive {
        let primitiveCopy = Primitive()

        primitiveCopy.name = primitive.name
        primitiveCopy.color = makeColorCopy(color: primitive.color)
        primitiveCopy.center = makeVertexCopy(vertex: primitive.center)

        for vertex in primitive.vertices {
            primitiveCopy.vertices.append(makeVertexCopy(vertex: vertex))
        }

        for verge in primitive.verges {
            primitiveCopy.verges.append(makeVergeCopy(vertices: Array(primitiveCopy.vertices), verge: verge))
        }

        return primitiveCopy
    }

    private func makeVertexCopy(vertex: Vertex) -> Vertex {
        let vertexCopy = Vertex()

        vertexCopy.x = vertex.x
        vertexCopy.y = vertex.y
        vertexCopy.z = vertex.z

        return vertexCopy
    }

    private func makeColorCopy(color: RealmColor) -> RealmColor {
        let colorCopy = RealmColor()

        colorCopy.r = color.r
        colorCopy.g = color.g
        colorCopy.b = color.b

        return colorCopy
    }

    private func makeVergeCopy(vertices: [Vertex], verge: Verge) -> Verge {
        let vergeCopy = Verge()

        vergeCopy.id = verge.id

        for connection in verge.connections {
            vergeCopy.connections.append(createConnection(start: vertices[connection.startVertex.id],
                                                          end: vertices[connection.endVertex.id]))
        }

        return vergeCopy
    }
}

extension DataService: DataServiceInput {
    func addShape(with shape: Shape) {
        do {
            try database.write {
                database.create(Shape.self, value: shape, update: .error)
            }
        } catch {
            fatalError("Failed to save data - \(error)")
        }
    }

    func getShapes() -> [Shape] {
        let result = database.objects(Shape.self).sorted(byKeyPath: #keyPath(Shape.index), ascending: true)

        return result.map { $0 }
    }

    func loadInitialData() {
        do {
            try database.write {
                database.create(Shape.self, value: [Primitives.cube.rawValue, "cube", 0], update: .error)
                database.create(Shape.self, value: [Primitives.pyramid.rawValue, "pyramid", 1], update: .error)
                database.create(Shape.self, value: [Primitives.sphere.rawValue, "sphere", 2], update: .error)
                database.create(Shape.self, value: [Primitives.facet.rawValue, "facet", 3], update: .error)
                database.create(Shape.self, value: [Primitives.cone.rawValue, "cone", 4], update: .error)
            }
        } catch {
            fatalError("Failed to save data - \(error)")
        }
    }

    func projectIsUnique(name: String, authorName: String) -> Bool {
        let projects = database.objects(Project.self).filter("name == %@ AND authorName == %@", name, authorName)

        if projects.isEmpty {
            return true
        }

        return false
    }

    func createProject(name: String, authorName: String) {
        guard projectIsUnique(name: name, authorName: authorName) else {
            return
        }

        let project = Project()
        let dateFormatter = DateFormatter()

        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short

        project.name = name
        project.authorName = authorName
        project.lastModified = dateFormatter.string(from: Date())

        do {
            try database.write {
                database.create(Project.self, value: project, update: .error)
            }
        } catch {
            fatalError("Failed to create new project!")
        }
    }

    func snippetIsUnique(name: String, authorName: String) -> Bool {
        let snippets = database.objects(Snippet.self).filter("name == %@ AND authorName == %@", name, authorName)

        if snippets.isEmpty {
            return true
        }

        return false
    }

    func createSnippet(name: String, authorName: String) {
        guard snippetIsUnique(name: name, authorName: authorName) else {
            return
        }

        let snippet = Snippet()
        let dateFormatter = DateFormatter()

        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short

        snippet.name = name
        snippet.authorName = authorName
        snippet.lastModified = dateFormatter.string(from: Date())

        do {
            try database.write {
                database.create(Snippet.self, value: snippet, update: .error)
            }
        } catch {
            fatalError("Failed to create new snippet!")
        }
    }

    func deletePrimitive(primitive: Primitive) {
        do {
            try database.write {
                database.delete(primitive)
            }
        } catch {
            fatalError("Can not delete primitive")
        }
    }

    func getAllPrimitives(projectName: String, authorName: String) -> [Primitive] {
        let project = database.objects(Project.self).filter("name == %@ AND authorName == %@", projectName, authorName)

        guard !project.isEmpty,
              let primitives = project.first?.primitives else {
            return []
        }

        let dateFormatter = DateFormatter()

        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short

        do {
            try database.write {
                project.first?.lastModified = dateFormatter.string(from: Date())
            }
        } catch {
            fatalError("Cant reset touched project date")
        }

        return Array(primitives)
    }

    func getSnippetPrimitives(name: String, authorName: String) -> [Primitive] {
        let snippet = database.objects(Snippet.self).filter("name == %@ AND authorName == %@", name, authorName)

        guard !snippet.isEmpty,
              let primitives = snippet.first?.primitives else {
            return []
        }

        let dateFormatter = DateFormatter()

        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short

        do {
            try database.write {
                snippet.first?.lastModified = dateFormatter.string(from: Date())
            }
        } catch {
            fatalError("Cant reset touched snippet date")
        }

        return Array(primitives)
    }

    func addPrimitive(projectName: String, authorName: String, primitive: Primitive) {
        let project = database.objects(Project.self).filter(("name == %@ AND authorName == %@"), projectName, authorName)

        guard !project.isEmpty else {
            return
        }

        guard let uniqueID = project.first?.uniquePrimitiveID else {
            return
        }

        do {
            try database.write {
                primitive.id = uniqueID
                project.first?.uniquePrimitiveID = uniqueID + 1

                project.first?.primitives.append(primitive)
            }
        } catch {
            fatalError("Failed to add new primitive")
        }
    }

    func createCube(center: CGPoint, side: CGFloat) -> Primitive {
        let vertices = [createVertex(id: 0, x: Float(center.x - side / 2), y: Float(center.y + side / 2), z: Float(side / 2)),
                        createVertex(id: 1, x: Float(center.x + side / 2), y: Float(center.y + side / 2), z: Float(side / 2)),
                        createVertex(id: 2, x: Float(center.x + side / 2), y: Float(center.y - side / 2), z: Float(side / 2)),
                        createVertex(id: 3, x: Float(center.x - side / 2), y: Float(center.y - side / 2), z: Float(side / 2)),
                        createVertex(id: 4, x: Float(center.x - side / 2), y: Float(center.y + side / 2), z: -Float(side / 2)),
                        createVertex(id: 5, x: Float(center.x + side / 2), y: Float(center.y + side / 2), z: -Float(side / 2)),
                        createVertex(id: 6, x: Float(center.x + side / 2), y: Float(center.y - side / 2), z: -Float(side / 2)),
                        createVertex(id: 7, x: Float(center.x - side / 2), y: Float(center.y - side / 2), z: -Float(side / 2))]

        let connectionsVergeA1 = [createConnection(start: vertices[0], end: vertices[1]),
                                 createConnection(start: vertices[1], end: vertices[2]),
                                 createConnection(start: vertices[2], end: vertices[0])]

        let connectionsVergeA2 = [createConnection(start: vertices[2], end: vertices[0]),
                                 createConnection(start: vertices[0], end: vertices[3]),
                                 createConnection(start: vertices[3], end: vertices[2])]

        let connectionsVergeB1 = [createConnection(start: vertices[4], end: vertices[5]),
                                 createConnection(start: vertices[5], end: vertices[6]),
                                 createConnection(start: vertices[6], end: vertices[4])]

        let connectionsVergeB2 = [createConnection(start: vertices[6], end: vertices[4]),
                                 createConnection(start: vertices[4], end: vertices[7]),
                                 createConnection(start: vertices[7], end: vertices[6])]

        let connectionsVergeC1 = [createConnection(start: vertices[0], end: vertices[4]),
                                 createConnection(start: vertices[4], end: vertices[7]),
                                 createConnection(start: vertices[7], end: vertices[0])]

        let connectionsVergeC2 = [createConnection(start: vertices[7], end: vertices[0]),
                                 createConnection(start: vertices[0], end: vertices[3]),
                                 createConnection(start: vertices[3], end: vertices[7])]

        let connectionsVergeD1 = [createConnection(start: vertices[1], end: vertices[5]),
                                 createConnection(start: vertices[5], end: vertices[6]),
                                 createConnection(start: vertices[6], end: vertices[1])]

        let connectionsVergeD2 = [createConnection(start: vertices[6], end: vertices[1]),
                                 createConnection(start: vertices[1], end: vertices[2]),
                                 createConnection(start: vertices[2], end: vertices[6])]

        let connectionsVergeE1 = [createConnection(start: vertices[2], end: vertices[3]),
                                 createConnection(start: vertices[3], end: vertices[7]),
                                 createConnection(start: vertices[7], end: vertices[2])]

        let connectionsVergeE2 = [createConnection(start: vertices[7], end: vertices[2]),
                                 createConnection(start: vertices[2], end: vertices[6]),
                                 createConnection(start: vertices[6], end: vertices[7])]

        let connectionsVergeF1 = [createConnection(start: vertices[1], end: vertices[0]),
                                 createConnection(start: vertices[0], end: vertices[4]),
                                 createConnection(start: vertices[4], end: vertices[1])]

        let connectionsVergeF2 = [createConnection(start: vertices[4], end: vertices[1]),
                                 createConnection(start: vertices[1], end: vertices[5]),
                                 createConnection(start: vertices[5], end: vertices[4])]

        let verges = [createVerge(id: 1, connections: connectionsVergeA1),
                      createVerge(id: 2, connections: connectionsVergeA2),
                      createVerge(id: 3, connections: connectionsVergeB1),
                      createVerge(id: 4, connections: connectionsVergeB2),
                      createVerge(id: 5, connections: connectionsVergeC1),
                      createVerge(id: 6, connections: connectionsVergeC2),
                      createVerge(id: 7, connections: connectionsVergeD1),
                      createVerge(id: 8, connections: connectionsVergeD2),
                      createVerge(id: 9, connections: connectionsVergeE1),
                      createVerge(id: 10, connections: connectionsVergeE2),
                      createVerge(id: 11, connections: connectionsVergeF1),
                      createVerge(id: 12, connections: connectionsVergeF2)]

        let primitive = createPrimitive(verges: verges, vertices: vertices)

        primitive.name = "Cube"

        return primitive
    }

    func createPyramid(center: CGPoint, height: CGFloat, baseSide: CGFloat) -> Primitive {
        let vertices = [createVertex(id: 0,
                                     x: Float(center.x - baseSide / 2),
                                     y: Float(center.y + height / 2),
                                     z: Float(baseSide / 2)),
                        createVertex(id: 1,
                                     x: Float(center.x + baseSide / 2),
                                     y: Float(center.y + height / 2),
                                     z: Float(baseSide / 2)),
                        createVertex(id: 2,
                                     x: Float(center.x + baseSide / 2),
                                     y: Float(center.y + height / 2),
                                     z: Float(-baseSide / 2)),
                        createVertex(id: 3,
                                     x: Float(center.x - baseSide / 2),
                                     y: Float(center.y + height / 2),
                                     z: Float(-baseSide / 2)),
                        createVertex(id: 4,
                                     x: Float(center.x),
                                     y: Float(center.y - height / 2),
                                     z: 0)]

        let connectionsVergeA1 = [createConnection(start: vertices[0], end: vertices[1]),
                                 createConnection(start: vertices[1], end: vertices[3]),
                                 createConnection(start: vertices[3], end: vertices[0])]

        let connectionsVergeA2 = [createConnection(start: vertices[1], end: vertices[3]),
                                 createConnection(start: vertices[3], end: vertices[2]),
                                 createConnection(start: vertices[2], end: vertices[1])]

        let connectionsVergeB = [createConnection(start: vertices[0], end: vertices[4]),
                                 createConnection(start: vertices[4], end: vertices[3]),
                                 createConnection(start: vertices[3], end: vertices[0])]

        let connectionsVergeC = [createConnection(start: vertices[4], end: vertices[3]),
                                 createConnection(start: vertices[3], end: vertices[2]),
                                 createConnection(start: vertices[2], end: vertices[4])]

        let connectionsVergeD = [createConnection(start: vertices[4], end: vertices[2]),
                                 createConnection(start: vertices[2], end: vertices[1]),
                                 createConnection(start: vertices[1], end: vertices[4])]

        let connectionsVergeE = [createConnection(start: vertices[4], end: vertices[1]),
                                 createConnection(start: vertices[1], end: vertices[0]),
                                 createConnection(start: vertices[0], end: vertices[4])]

        let verges = [createVerge(id: 1, connections: connectionsVergeA1),
                      createVerge(id: 2, connections: connectionsVergeA2),
                      createVerge(id: 3, connections: connectionsVergeB),
                      createVerge(id: 4, connections: connectionsVergeC),
                      createVerge(id: 5, connections: connectionsVergeD),
                      createVerge(id: 6, connections: connectionsVergeE)]

        let primitive = createPrimitive(verges: verges, vertices: vertices)

        primitive.name = "Pyramid"

        return primitive
    }

    func createFacet(pointA: Point, pointB: Point, pointC: Point) -> Primitive {
        let vertices = [createVertex(id: 0,
                                     x: pointA.x,
                                     y: pointA.y,
                                     z: pointA.z),
                        createVertex(id: 1,
                                     x: pointB.x,
                                     y: pointB.y,
                                     z: pointB.z),
                        createVertex(id: 2,
                                     x: pointC.x,
                                     y: pointC.y,
                                     z: pointC.z)]

        let connectionsVerge = [createConnection(start: vertices[0], end: vertices[1]),
                                createConnection(start: vertices[1], end: vertices[2]),
                                createConnection(start: vertices[2], end: vertices[0])]

        let verges = [createVerge(id: 1, connections: connectionsVerge)]

        let primitive = createPrimitive(verges: verges, vertices: vertices)

        primitive.name = "Facet"

        return primitive
    }

    func createSphere(center: Point, radius: Float) -> Primitive {
        var vertices: [[Vertex]] = []

        for alpha in stride(from: -90, through: 90, by: 10) {
            var halfLeft: [Vertex] = []
            var halfRight: [Vertex] = []
            let alphaRadians = Float(alpha) * Float.pi / 180.0

            for beta in stride(from: -90, through: 90, by: 10) {
                let vertexRight = Vertex()

                vertexRight.x = center.x + radius * cos(Float(beta) * Float.pi / 180.0) * cos(alphaRadians)
                vertexRight.y = center.y + radius * sin(Float(beta) * Float.pi / 180.0)
                vertexRight.z = center.z + radius * sin(alphaRadians) * cos(Float(beta) * Float.pi / 180.0)

                halfRight.append(vertexRight)

                let vertexLeft = Vertex()

                vertexLeft.x = center.x - radius * cos(Float(beta) * Float.pi / 180.0) * cos(alphaRadians)
                vertexLeft.y = center.y + radius * sin(Float(beta) * Float.pi / 180.0)
                vertexLeft.z = center.z - radius * sin(alphaRadians) * cos(Float(beta) * Float.pi / 180.0)

                halfLeft.append(vertexLeft)
            }

            vertices.append(halfLeft.reversed() + halfRight)
        }

        var verges: [Verge] = []
        var vergeID = 1
        let ringVerticesCount = vertices.first?.count ?? 0

        for i in 0..<vertices.count - 1 {
            for j in 0..<ringVerticesCount - 1 {
                let connectionsVergeA: [Connection] = [
                    createConnection(start: vertices[i][j], end: vertices[i + 1][j]),
                    createConnection(start: vertices[i + 1][j], end: vertices[i][j + 1]),
                    createConnection(start: vertices[i][j + 1], end: vertices[i][j])
                ]

                verges.append(createVerge(id: vergeID, connections: connectionsVergeA))

                let connectionsVergeB: [Connection] = [
                    createConnection(start: vertices[i + 1][j + 1], end: vertices[i + 1][j]),
                    createConnection(start: vertices[i + 1][j], end: vertices[i][j + 1]),
                    createConnection(start: vertices[i][j + 1], end: vertices[i + 1][j + 1])
                ]

                verges.append(createVerge(id: vergeID + 1, connections: connectionsVergeB))

                vergeID += 2
            }
        }

        let primitive = createPrimitive(verges: verges, vertices: vertices.flatMap { return $0 })

        primitive.name = "Sphere"

        return primitive
    }

    func createCone(center: Point, height: CGFloat, baseRadius: CGFloat) -> Primitive {
        var vertices: [Vertex] = []

        let topVertex = createVertex(id: 1,
                                     x: center.x,
                                     y: center.y - Float(height / 2),
                                     z: 0)

        var halfLeft: [Vertex] = []
        var halfRight: [Vertex] = []

        for alpha in stride(from: -90, through: 90, by: 10) {
            let alphaRadians = Float(alpha) * Float.pi / 180.0
            let vertexRight = Vertex()

            vertexRight.x = center.x + Float(baseRadius) * cos(alphaRadians)
            vertexRight.y = center.y + Float(height / 2)
            vertexRight.z = center.z + Float(baseRadius) * sin(alphaRadians)
            halfRight.append(vertexRight)

            let vertexLeft = Vertex()

            vertexLeft.x = center.x - Float(baseRadius) * cos(alphaRadians)
            vertexLeft.y = center.y + Float(height / 2)
            vertexLeft.z = center.z + Float(baseRadius) * sin(alphaRadians)
            halfLeft.append(vertexLeft)
        }

        vertices = halfRight + halfLeft.reversed()

        var verges: [Verge] = []
        var vergeID = 1

        for i in 0..<vertices.count - 1 {
            var connectionsA: [Connection] = []

            if i % 2 == 0 {
                connectionsA = [
                    createConnection(start: topVertex, end: vertices[i]),
                    createConnection(start: vertices[i], end: vertices[i + 1]),
                    createConnection(start: vertices[i + 1], end: topVertex)
                ]
            } else {
                connectionsA = [
                    createConnection(start: topVertex, end: vertices[i + 1]),
                    createConnection(start: vertices[i + 1], end: vertices[i]),
                    createConnection(start: vertices[i], end: topVertex)
                ]
            }

            verges.append(createVerge(id: vergeID, connections: connectionsA))

            let connectionsB: [Connection] = [
                createConnection(start: vertices[i + 1], end: vertices[i]),
                createConnection(start: vertices[i], end: vertices[(i + vertices.count / 2) % vertices.count]),
                createConnection(start: vertices[(i + vertices.count / 2) % vertices.count], end: vertices[i + 1])
            ]

            verges.append(createVerge(id: vergeID + 1, connections: connectionsB))

            vergeID += 2
        }

        vertices.append(topVertex)

        let primitive = createPrimitive(verges: verges, vertices: vertices)

        primitive.name = "Cone"

        return primitive
    }

    func movePrimitive(primitive: Primitive, deltaX: Float, deltaY: Float, deltaZ: Float) {
        do {
            try database.write {
                move(primitive: primitive,
                     deltaX: deltaX,
                     deltaY: deltaY,
                     deltaZ: deltaZ)
            }
        } catch {
            fatalError("Failed to move primitive")
        }
    }

    func scalePrimitive(primitive: Primitive, koefX: Float, koefY: Float, koefZ: Float) {
        do {
            let deltaX = primitive.center.x
            let deltaY = primitive.center.y
            let deltaZ = primitive.center.z

            try database.write {
                move(primitive: primitive,
                     deltaX: -deltaX,
                     deltaY: -deltaY,
                     deltaZ: -deltaZ)

                primitive.vertices.forEach { (vertex) in
                    vertex.x *= koefX
                    vertex.y *= koefY
                    vertex.z *= koefZ
                }

                move(primitive: primitive,
                     deltaX: deltaX,
                     deltaY: deltaY,
                     deltaZ: deltaZ)
            }
        } catch {
            fatalError("Failed to scale primitive")
        }
    }

    func rotatePrimitive(primitive: Primitive, angleX: Float, angleY: Float, angleZ: Float) {
        do {
            let deltaX = primitive.center.x
            let deltaY = primitive.center.y
            let deltaZ = primitive.center.z

            let rotationMatrix = createRotationMatrix(angleX: angleX * Float.pi / 180.0,
                                                      angleY: angleY * Float.pi / 180.0,
                                                      angleZ: angleZ * Float.pi / 180.0)

            try database.write {
                move(primitive: primitive,
                     deltaX: -deltaX,
                     deltaY: -deltaY,
                     deltaZ: -deltaZ)

                primitive.vertices.forEach { (vertex) in
                    let pointMatrix = createMatrixFromPoint(vertex: vertex)
                    let newPoint = multiplyMatrices(matrixA: rotationMatrix, matrixB: pointMatrix)

                    vertex.x = newPoint[0][0]
                    vertex.y = newPoint[1][0]
                    vertex.z = newPoint[2][0]
                }

                move(primitive: primitive,
                     deltaX: deltaX,
                     deltaY: deltaY,
                     deltaZ: deltaZ)
            }
        } catch {
            fatalError("Failed to rotate primitive")
        }
    }

    func setPrimitiveNewCenter(primitive: Primitive, x: Float, y: Float, z: Float) {
        do {
            let deltaX = x - primitive.center.x
            let deltaY = y - primitive.center.y
            let deltaZ = z - primitive.center.z

            try database.write {
                move(primitive: primitive,
                     deltaX: deltaX,
                     deltaY: deltaY,
                     deltaZ: deltaZ)
            }
        } catch {
            fatalError("Failed to set new primitive center")
        }
    }

    func setPrimitiveColor(primitive: Primitive, r: Float, g: Float, b: Float) {
        do {
            try database.write {
                primitive.color.r = r
                primitive.color.g = g
                primitive.color.b = b
            }
        } catch {
            fatalError("Failed to set new primitive color")
        }
    }

    func scaleProject(projectName: String, authorName: String, scaleFactor: Float, center: Point) {
        let primitives = DataService.shared.getAllPrimitives(projectName: projectName,
                                                             authorName: authorName)

        do {
            try database.write {
                primitives.forEach { (primitive) in
                    primitive.center.x = primitive.center.x * scaleFactor + (1 - scaleFactor) * center.x
                    primitive.center.y = primitive.center.y * scaleFactor + (1 - scaleFactor) * center.y
                    primitive.center.z = primitive.center.z * scaleFactor + (1 - scaleFactor) * center.z

                    primitive.vertices.forEach { (vertex) in
                        vertex.x = vertex.x * scaleFactor + (1 - scaleFactor) * center.x
                        vertex.y = vertex.y * scaleFactor + (1 - scaleFactor) * center.y
                        vertex.z = vertex.z * scaleFactor + (1 - scaleFactor) * center.z
                    }
                }
            }

        } catch {
            fatalError("Failed to scale project")
        }
    }

    func scaleSnippet(name: String, authorName: String, scaleFactor: Float, center: Point) {
        let primitives = DataService.shared.getSnippetPrimitives(name: name, authorName: authorName)

        do {
            try database.write {
                primitives.forEach { (primitive) in
                    primitive.center.x = primitive.center.x * scaleFactor + (1 - scaleFactor) * center.x
                    primitive.center.y = primitive.center.y * scaleFactor + (1 - scaleFactor) * center.y
                    primitive.center.z = primitive.center.z * scaleFactor + (1 - scaleFactor) * center.z

                    primitive.vertices.forEach { (vertex) in
                        vertex.x = vertex.x * scaleFactor + (1 - scaleFactor) * center.x
                        vertex.y = vertex.y * scaleFactor + (1 - scaleFactor) * center.y
                        vertex.z = vertex.z * scaleFactor + (1 - scaleFactor) * center.z
                    }
                }
            }

        } catch {
            fatalError("Failed to scale snippet")
        }
    }

    func moveVertex(vertex: Vertex, deltaX: Float, deltaY: Float, deltaZ: Float) {
        do {
            try database.write {
                vertex.x += deltaX
                vertex.y += deltaY
                vertex.z += deltaZ
            }
        } catch {
            fatalError("Failed to move vertex")
        }
    }

    func getAllProjects() -> [Project] {
        return Array(database.objects(Project.self))
    }

    func getSnippet(name: String, authorName: String) -> Snippet? {
        return database.objects(Snippet.self).filter("name == %@ AND authorName == %@", name, authorName).first
    }

    func addFacetToSnippet(snippet: Snippet, pointA: Point, pointB: Point, pointC: Point) {
        let newFacet = createFacet(pointA: pointA, pointB: pointB, pointC: pointC)

        do {
            try database.write {
                snippet.primitives.append(newFacet)
            }
        } catch {
            fatalError("Cant add facet to snippet")
        }
    }

    func getProject(name: String, authorName: String) -> Project? {
        return database.objects(Project.self).filter("name == %@ AND authorName == %@", name, authorName).first
    }

    func getAllSnippets() -> [Snippet] {
        return Array(database.objects(Snippet.self))
    }

    func appendSnippetToProject(project: Project, snippetName: String, authorName: String) {
        guard let snippet = getSnippet(name: snippetName,
                                       authorName: authorName) else {
            return
        }

        let snippetCopy = makeSnippetCopy(snippet: snippet)

        do {
            try database.write {
                project.primitives.append(objectsIn: snippetCopy.primitives)
            }
        } catch {
            fatalError("Cant append snippet to project")
        }
    }
}

extension DataService: AuthorizationServiceInput {
    func isAuthorized() -> Bool {
        guard let isAuthorizedObject = UserDefaults.standard.value(forKeyPath: Constants.authKey) else {
            return false
        }

        guard let isAuthorized = isAuthorizedObject as? Bool else {
            return false
        }

        return isAuthorized
    }

    func authorize() {
        UserDefaults.standard.setValue(true, forKey: Constants.authKey)
    }
}

extension DataService {
    private enum Primitives: String {
        case cube = "Cube"

        case pyramid = "Pyramid"

        case sphere = "Sphere"

        case cone = "Cone"

        case facet = "Facet"
    }

    private struct Constants {
        static var authKey: String = "isAuthorized"
    }
}
