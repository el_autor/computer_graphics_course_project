import CoreGraphics

protocol DataServiceInput {
    func addShape(with shape: Shape)

    func getShapes() -> [Shape]

    func loadInitialData()

    func projectIsUnique(name: String, authorName: String) -> Bool

    func createProject(name: String, authorName: String)

    func snippetIsUnique(name: String, authorName: String) -> Bool

    func createSnippet(name: String, authorName: String)

    func deletePrimitive(primitive: Primitive)

    func addPrimitive(projectName: String, authorName: String, primitive: Primitive)

    func createCube(center: CGPoint, side: CGFloat) -> Primitive

    func createPyramid(center: CGPoint, height: CGFloat, baseSide: CGFloat) -> Primitive

    func createFacet(pointA: Point, pointB: Point, pointC: Point) -> Primitive

    func createSphere(center: Point, radius: Float) -> Primitive

    func createCone(center: Point, height: CGFloat, baseRadius: CGFloat) -> Primitive

    func movePrimitive(primitive: Primitive, deltaX: Float, deltaY: Float, deltaZ: Float)

    func scalePrimitive(primitive: Primitive, koefX: Float, koefY: Float, koefZ: Float)

    func rotatePrimitive(primitive: Primitive, angleX: Float, angleY: Float, angleZ: Float)

    func setPrimitiveNewCenter(primitive: Primitive, x: Float, y: Float, z: Float)

    func setPrimitiveColor(primitive: Primitive, r: Float, g: Float, b: Float)

    func scaleProject(projectName: String, authorName: String, scaleFactor: Float, center: Point)

    func scaleSnippet(name: String, authorName: String, scaleFactor: Float, center: Point)

    func moveVertex(vertex: Vertex, deltaX: Float, deltaY: Float, deltaZ: Float)

    func getAllPrimitives(projectName: String, authorName: String) -> [Primitive]

    func getSnippetPrimitives(name: String, authorName: String) -> [Primitive]

    func getSnippet(name: String, authorName: String) -> Snippet?

    func addFacetToSnippet(snippet: Snippet, pointA: Point, pointB: Point, pointC: Point)

    func getAllProjects() -> [Project]

    func getProject(name: String, authorName: String) -> Project?

    func getAllSnippets() -> [Snippet]

    func appendSnippetToProject(project: Project, snippetName: String, authorName: String)
}

protocol AuthorizationServiceInput {
    func isAuthorized() -> Bool

    func authorize()

}
