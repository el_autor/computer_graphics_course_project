import RealmSwift

class Shape: Object {
    @objc dynamic var name: String?

    @objc dynamic var imageName: String?

    @objc dynamic var index: Int = 0
}

class Vertex: Object {
    @objc dynamic var id: Int = 0

    @objc dynamic var x: Float = 0

    @objc dynamic var y: Float = 0

    @objc dynamic var z: Float = 0

    let primitive = LinkingObjects(fromType: Primitive.self, property: "vertices")
}

class Connection: Object {
    @objc dynamic var startVertex: Vertex!

    @objc dynamic var endVertex: Vertex!

    let verge = LinkingObjects(fromType: Verge.self, property: "connections")
}

class Verge: Object {
    @objc dynamic var id: Int = 0

    let connections = List<Connection>()

    let primitive = LinkingObjects(fromType: Primitive.self, property: "verges")
}

class Primitive: Object {
    @objc dynamic var id: Int = 0

    @objc dynamic var name: String = String()

    @objc dynamic var center: Vertex!

    @objc dynamic var color: RealmColor!

    let vertices = List<Vertex>()

    let verges = List<Verge>()
}

class Project: Object {
    @objc dynamic var name: String?

    @objc dynamic var authorName: String?

    @objc dynamic var uniquePrimitiveID: Int = 0

    @objc dynamic var lastModified: String = String()

    let primitives = List<Primitive>()
}

class Snippet: Object {
    @objc dynamic var name: String?

    @objc dynamic var authorName: String?

    @objc dynamic var uniquePrimitiveID: Int = 0

    @objc dynamic var lastModified: String = String()

    let primitives = List<Primitive>()
}

class RealmColor: Object {
    @objc dynamic var r: Float = 0

    @objc dynamic var g: Float = 0

    @objc dynamic var b: Float = 0
}
