import Foundation

final class ModifyVertexInteractor {
	weak var output: ModifyVertexInteractorOutput?

    private var vertex: Vertex

    init(vertex: Vertex) {
        self.vertex = vertex
    }
}

extension ModifyVertexInteractor: ModifyVertexInteractorInput {
    func moveVertex(deltaX: Float, deltaY: Float, deltaZ: Float) {
        DataService.shared.moveVertex(vertex: vertex,
                                      deltaX: deltaX,
                                      deltaY: deltaY,
                                      deltaZ: deltaZ)
    }

    func getCoordinates() -> Point {
        return Point(x: vertex.x,
                     y: vertex.y,
                     z: vertex.z)
    }
}
