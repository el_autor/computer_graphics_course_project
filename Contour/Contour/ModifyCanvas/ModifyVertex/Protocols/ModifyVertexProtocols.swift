import Foundation

protocol ModifyVertexViewInput: AnyObject {
    func setMoveTextFields()

    func getFirstEntryText() -> String?

    func getSecondEntryText() -> String?

    func getThirdEntryText() -> String?

    func showTextFields()

    func setCoordinatesLabelText(text: String)
}

protocol ModifyVertexViewOutput: AnyObject {
    func didTapBackButton()

    func didTapApplyButton()

    func didTapMoveButton()

    func didLoadView()
}

protocol ModifyVertexInteractorInput: AnyObject {
    func moveVertex(deltaX: Float, deltaY: Float, deltaZ: Float)

    func getCoordinates() -> Point
}

protocol ModifyVertexInteractorOutput: AnyObject {
}

protocol ModifyVertexRouterInput: AnyObject {
    func goToBackScreen()

    func goToCanvas()
}
