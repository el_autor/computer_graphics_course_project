import UIKit
import PinLayout

final class ModifyVertexViewController: UIViewController {
	var output: ModifyVertexViewOutput?

    // MARK: UI elements

    private weak var backButton: UIButton!

    private weak var descriptionLabel: UILabel!

    private weak var frameView: UIView!

    private weak var vertexNameLabel: UILabel!

    private weak var coordinatesLabel: UILabel!

    private weak var moveButton: UIButton!

    private weak var firstParameterTextField: UITextField!

    private weak var secondParameterTextField: UITextField!

    private weak var thirdParameterTextField: UITextField!

    private weak var applyButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupSubviews()

        output?.didLoadView()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        layoutBackButton()
        layoutDescriptionLabel()
        layoutFrameView()
        layoutVertexNameLabel()
        layoutCoordinatesLabel()
        layoutMoveButton()
        layoutFirstParameterTextField()
        layoutSecondParameterTextField()
        layoutThirdParameterTextField()
        layoutApplyButton()
    }

    private func setupView() {
        view.backgroundColor = Constants.backgroundColor
    }

    private func setupSubviews() {
        setupBackButton()
        setupDescriptionLabel()
        setupFrameView()
        setupVertexNameLabel()
        setupCoordinatesLabel()
        setupMoveButton()
        setupFirstParameterTextField()
        setupSecondParameterTextField()
        setupThirdParameterTextField()
        setupApplyButton()
    }

    private func setupBackButton() {
        let button = UIButton()

        backButton = button
        view.addSubview(backButton)

        backButton.setImage(UIImage(systemName: "chevron.backward",
                                    withConfiguration: UIImage.SymbolConfiguration(weight: .bold)),
                                    for: .normal)

        backButton.tintColor = .white
        backButton.contentVerticalAlignment = .fill
        backButton.contentHorizontalAlignment = .fill

        backButton.addTarget(self, action: #selector(didTapBackButton), for: .touchUpInside)
    }

    private func setupDescriptionLabel() {
        let label = UILabel()

        descriptionLabel = label
        view.addSubview(label)

        descriptionLabel.textAlignment = .center
        descriptionLabel.text = "Modify vertex"
        descriptionLabel.font = UIFont(name: "DMSans-Bold", size: 25)
        descriptionLabel.textColor = .white
    }

    private func setupFrameView() {
        let frame = UIView()

        frameView = frame
        view.addSubview(frame)

        frameView.backgroundColor = Constants.FrameView.backgroundColor
        frameView.layer.cornerRadius = 20
    }

    private func setupVertexNameLabel() {
        let label = UILabel()

        vertexNameLabel = label
        frameView.addSubview(vertexNameLabel)

        vertexNameLabel.textAlignment = .center
        vertexNameLabel.font = UIFont(name: "DMSans-Bold", size: 25)
        vertexNameLabel.textColor = .black
        vertexNameLabel.text = "Vertex"
    }

    private func setupCoordinatesLabel() {
        let label = UILabel()

        coordinatesLabel = label
        frameView.addSubview(coordinatesLabel)

        coordinatesLabel.textAlignment = .center
        coordinatesLabel.font = UIFont(name: "DMSans-Bold", size: 20)
        coordinatesLabel.textColor = .black
    }

    private func setupMoveButton() {
        let button = UIButton()

        moveButton = button
        frameView.addSubview(moveButton)

        moveButton.backgroundColor = Constants.backgroundColor
        moveButton.setTitle("Move", for: .normal)
        moveButton.layer.cornerRadius = 10
        moveButton.titleLabel?.font = UIFont(name: "DMSans-Bold", size: 20)
        moveButton.setTitleColor(.white, for: .normal)

        moveButton.addTarget(self, action: #selector(didTapMoveButton), for: .touchUpInside)
    }

    private func setupFirstParameterTextField() {
        let textField = UITextField()

        firstParameterTextField = textField
        frameView.addSubview(firstParameterTextField)

        textField.isHidden = true

        textField.attributedPlaceholder = NSAttributedString(string: String(),
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])

        textField.clipsToBounds = true
        textField.textColor = .black
        textField.layer.cornerRadius = 10
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.white.cgColor
        textField.backgroundColor = UIColor.white
        textField.font = UIFont(name: "DMSans-Regular", size: 20)

        textField.leftView = UIView(frame: CGRect(x: .zero,
                                                      y: .zero,
                                                      width: 10,
                                                      height: .zero))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: .zero,
                                                       y: .zero,
                                                       width: 10,
                                                       height: .zero))
        textField.rightViewMode = .unlessEditing

        textField.clearButtonMode = .whileEditing
        textField.autocorrectionType = .no
    }

    private func setupSecondParameterTextField() {
        let textField = UITextField()

        secondParameterTextField = textField
        frameView.addSubview(secondParameterTextField)

        textField.isHidden = true

        textField.attributedPlaceholder = NSAttributedString(string: String(),
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])

        textField.clipsToBounds = true
        textField.textColor = .black
        textField.layer.cornerRadius = 10
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.white.cgColor
        textField.backgroundColor = UIColor.white
        textField.font = UIFont(name: "DMSans-Regular", size: 20)

        textField.leftView = UIView(frame: CGRect(x: .zero,
                                                      y: .zero,
                                                      width: 10,
                                                      height: .zero))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: .zero,
                                                       y: .zero,
                                                       width: 10,
                                                       height: .zero))
        textField.rightViewMode = .unlessEditing

        textField.clearButtonMode = .whileEditing
        textField.autocorrectionType = .no
    }

    private func setupThirdParameterTextField() {
        let textField = UITextField()

        thirdParameterTextField = textField
        frameView.addSubview(thirdParameterTextField)

        textField.isHidden = true

        textField.attributedPlaceholder = NSAttributedString(string: String(),
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])

        textField.clipsToBounds = true
        textField.textColor = .black
        textField.layer.cornerRadius = 10
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.white.cgColor
        textField.backgroundColor = UIColor.white
        textField.font = UIFont(name: "DMSans-Regular", size: 20)

        textField.leftView = UIView(frame: CGRect(x: .zero,
                                                      y: .zero,
                                                      width: 10,
                                                      height: .zero))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: .zero,
                                                       y: .zero,
                                                       width: 10,
                                                       height: .zero))
        textField.rightViewMode = .unlessEditing

        textField.clearButtonMode = .whileEditing
        textField.autocorrectionType = .no
    }

    private func setupApplyButton() {
        let button = UIButton()

        applyButton = button
        view.addSubview(button)

        applyButton.backgroundColor = Constants.FrameView.backgroundColor
        applyButton.setTitle("Apply", for: .normal)
        applyButton.layer.cornerRadius = 10
        applyButton.titleLabel?.font = UIFont(name: "DMSans-Regular", size: 20)
        applyButton.setTitleColor(.black, for: .normal)

        applyButton.addTarget(self, action: #selector(didTapApplyButton), for: .touchUpInside)
    }

    private func layoutBackButton() {
        backButton.pin
            .height(25)
            .width(20)
            .top(2%)
            .left(2%)
    }

    private func layoutDescriptionLabel() {
        descriptionLabel.pin
            .top(3%)
            .height(4%)
            .width(50%)
            .hCenter()
    }

    private func layoutFrameView() {
        frameView.pin
            .hCenter()
            .vCenter()
            .width(75%)
            .height(80%)
    }

    private func layoutVertexNameLabel() {
        vertexNameLabel.pin
            .top(5%)
            .height(4%)
            .hCenter()
            .width(60%)
    }

    private func layoutCoordinatesLabel() {
        coordinatesLabel.pin
            .below(of: vertexNameLabel).marginTop(5%)
            .height(4%)
            .hCenter()
            .width(90%)
    }

    private func layoutMoveButton() {
        moveButton.pin
            .below(of: coordinatesLabel).marginTop(5%)
            .hCenter()
            .width(60%)
            .height(7%)
    }

    private func layoutFirstParameterTextField() {
        firstParameterTextField.pin
            .left(10%)
            .below(of: moveButton).marginTop(7.5%)
            .width(20%)
            .height(7%)
    }

    private func layoutSecondParameterTextField() {
        secondParameterTextField.pin
            .right(of: firstParameterTextField).marginLeft(10%)
            .below(of: moveButton).marginTop(7.5%)
            .width(20%)
            .height(7%)
    }

    private func layoutThirdParameterTextField() {
        thirdParameterTextField.pin
            .right(of: secondParameterTextField).marginLeft(10%)
            .below(of: moveButton).marginTop(7.5%)
            .width(20%)
            .height(7%)
    }

    private func layoutApplyButton() {
        applyButton.pin
            .below(of: frameView).marginTop(2.5%)
            .height(5%)
            .hCenter()
            .width(30%)
    }

    private func setPlaceholders(first: String, second: String, third: String) {
        firstParameterTextField.placeholder = first
        secondParameterTextField.placeholder = second
        thirdParameterTextField.placeholder = third
    }

    @objc
    private func didTapBackButton() {
        output?.didTapBackButton()
    }

    @objc
    private func didTapApplyButton() {
        output?.didTapApplyButton()
    }

    @objc
    private func didTapMoveButton() {
        output?.didTapMoveButton()
    }
}

extension ModifyVertexViewController: ModifyVertexViewInput {
    func setMoveTextFields() {
        setPlaceholders(first: "dx", second: "dy", third: "dz")
    }

    func getFirstEntryText() -> String? {
        return firstParameterTextField.text
    }

    func getSecondEntryText() -> String? {
        return secondParameterTextField.text
    }

    func getThirdEntryText() -> String? {
        return thirdParameterTextField.text
    }

    func showTextFields() {
        firstParameterTextField.isHidden = false
        secondParameterTextField.isHidden = false
        thirdParameterTextField.isHidden = false
    }

    func setCoordinatesLabelText(text: String) {
        coordinatesLabel.text = text
    }
}

extension ModifyVertexViewController {
    private struct Constants {
        static let backgroundColor: UIColor = UIColor(red: 94 / 255,
                                                      green: 92 / 255,
                                                      blue: 230 / 255,
                                                      alpha: 1)

        struct FrameView {
            static let backgroundColor: UIColor = UIColor(red: 1,
                                                   green: 211 / 255,
                                                   blue: 180 / 255,
                                                   alpha: 1)
        }
    }
}
