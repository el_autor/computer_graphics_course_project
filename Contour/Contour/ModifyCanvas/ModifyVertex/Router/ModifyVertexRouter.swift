import UIKit

final class ModifyVertexRouter {
    weak var viewController: UIViewController?
}

extension ModifyVertexRouter: ModifyVertexRouterInput {
    func goToBackScreen() {
        viewController?.navigationController?.popViewController(animated: true)
    }

    func goToCanvas() {
        guard let canvas = viewController?.navigationController?.presentingViewController as? CanvasViewController else {
            return
        }

        canvas.updateCanvas()
        viewController?.navigationController?.dismiss(animated: true, completion: nil)
    }
}
