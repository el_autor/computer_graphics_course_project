import UIKit

final class ModifyVertexContainer {
	let viewController: UIViewController
	private(set) weak var router: ModifyVertexRouterInput!

	class func assemble(with context: ModifyVertexContext) -> ModifyVertexContainer {
        let router = ModifyVertexRouter()
        let interactor = ModifyVertexInteractor(vertex: context.vertex)
        let presenter = ModifyVertexPresenter(router: router, interactor: interactor)
	let viewController = ModifyVertexViewController()

        viewController.output = presenter
	presenter.view = viewController
	interactor.output = presenter
	router.viewController = viewController

        return ModifyVertexContainer(view: viewController, router: router)
	}

    private init(view: UIViewController, router: ModifyVertexRouterInput) {
		self.viewController = view
		self.router = router
	}
}

struct ModifyVertexContext {
    var vertex: Vertex
}
