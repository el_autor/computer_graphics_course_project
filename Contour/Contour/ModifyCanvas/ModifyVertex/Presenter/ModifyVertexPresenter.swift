import Foundation

final class ModifyVertexPresenter {

    enum State {
        case move
    }

	weak var view: ModifyVertexViewInput?

	private let router: ModifyVertexRouterInput

	private let interactor: ModifyVertexInteractorInput

    private var state: State?

    init(router: ModifyVertexRouterInput, interactor: ModifyVertexInteractorInput) {
        self.router = router
        self.interactor = interactor
    }
}

extension ModifyVertexPresenter: ModifyVertexViewOutput {
    func didTapBackButton() {
        router.goToBackScreen()
    }

    func didTapApplyButton() {
        guard let param1 = view?.getFirstEntryText(),
              let number1 = Float(param1) else {
            return
        }

        guard let param2 = view?.getSecondEntryText(),
              let number2 = Float(param2) else {
            return
        }

        guard let param3 = view?.getThirdEntryText(),
              let number3 = Float(param3) else {
            return
        }

        switch state {
        case .move:
            interactor.moveVertex(deltaX: number1,
                                  deltaY: number2,
                                  deltaZ: number3)
        default:
            ()
        }

        router.goToCanvas()
    }

    func didLoadView() {
        let point = interactor.getCoordinates()

        view?.setCoordinatesLabelText(text: "Coordinates - (\(lroundf(point.x)), \(lroundf(point.y)), \(lroundf(point.z)))")
    }

    func didTapMoveButton() {
        if state == nil {
            view?.showTextFields()
        }

        state = .move
        view?.setMoveTextFields()
    }
}

extension ModifyVertexPresenter: ModifyVertexInteractorOutput {
}
