import UIKit

class ObjectListRouter {
    weak var viewController: UIViewController?
}

extension ObjectListRouter: ObjectListRouterInput {
    func showModifyPrimitiveScreen(primitive: Primitive) {
        let modifyVC = ModifyPrimitiveContainer.assemble(with: ModifyPrimitiveContext(primitive: primitive)).viewController

        modifyVC.modalPresentationStyle = .popover

        viewController?.navigationController?.pushViewController(modifyVC, animated: true)
    }

    func showModifyVertexScreen(vertex: Vertex) {
        let modifyVC = ModifyVertexContainer.assemble(with: ModifyVertexContext(vertex: vertex)).viewController

        modifyVC.modalPresentationStyle = .popover

        viewController?.navigationController?.pushViewController(modifyVC, animated: true)
    }
}
