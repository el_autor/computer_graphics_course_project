import UIKit

final class ObjectListCell: UITableViewCell {

    @IBOutlet private weak var descriptionLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

        setupView()
        setupSubviews()
    }

    private func setupView() {
        selectionStyle = .none
        backgroundColor = Constants.backgroundColor
    }

    private func setupSubviews() {
        setupDescriptionLabel()
    }

    private func setupDescriptionLabel() {
        descriptionLabel.textAlignment = .left
        descriptionLabel.font = Constants.DescriptionLabel.font
    }

    func configure(object: ObjectWrapper) {
        var labelText = ""

        switch object.type {
        case .vertex:
            if let model = object.object as? Vertex {
                labelText += "\(model.primitive[0].name) vertex - "
                labelText += "(\(lroundf(model.x)), \(lroundf(model.y)), \(lroundf(model.z)))"
            } else {
                labelText += "Unknown"
            }
        case .primitive:
            if let model = object.object as? Primitive {
                labelText += "\(model.name) - "
                labelText += "(\(lroundf(model.center.x)), \(lroundf(model.center.y)), \(lroundf(model.center.z)))"
            } else {
                labelText += "Unknown: "
            }
        }

        descriptionLabel.text = labelText
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension ObjectListCell {
    private struct Constants {
        static let backgroundColor: UIColor = UIColor(red: 1,
                                               green: 211 / 255,
                                               blue: 180 / 255,
                                               alpha: 1)

        struct DescriptionLabel {
            static let font: UIFont? = UIFont(name: "DMSans-Regular", size: 25)
        }
    }
}
