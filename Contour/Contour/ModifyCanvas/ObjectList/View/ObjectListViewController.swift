import UIKit
import PinLayout

class ObjectListViewController: UIViewController {

    var output: ObjectListViewOutput?

    private weak var titleLabel: UILabel!

    private weak var objectsTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupSubviews()

        output?.didLoadView()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        objectsTableView.reloadData()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        layoutTitleLabel()
        layoutObjectsTableView()
    }

    private func layoutObjectsTableView() {
        objectsTableView.pin
            .hCenter()
            .vCenter()
            .width(75%)
            .height(80%)
    }

    private func layoutTitleLabel() {
        titleLabel.pin
            .top(3%)
            .hCenter()
            .height(4%)
            .width(50%)
    }

    private func setupView() {
        navigationController?.navigationBar.isHidden = true
        view.backgroundColor = Constants.backgroundColor
    }

    private func setupSubviews() {
        setupTitleView()
        setupObjectsTableView()
    }

    private func setupObjectsTableView() {
        let tableView = UITableView()

        objectsTableView = tableView
        view.addSubview(objectsTableView)

        objectsTableView.delegate = self
        objectsTableView.dataSource = self

        objectsTableView.register(UINib(nibName: "ObjectListCell", bundle: nil),
                                  forCellReuseIdentifier: Constants.ObjectListTableView.cellReuseIdentifier)

        objectsTableView.tableFooterView = UIView(frame: .zero)
        objectsTableView.separatorInset = UIEdgeInsets(top: .zero, left: .zero, bottom: .zero, right: .zero)
        objectsTableView.layer.cornerRadius = Constants.ObjectListTableView.cornerRadius
        objectsTableView.backgroundColor = Constants.ObjectListTableView.backgroundColor
    }

    private func setupTitleView() {
        let label = UILabel()

        titleLabel = label
        view.addSubview(titleLabel)

        titleLabel.textAlignment = .center
        titleLabel.text = Constants.TitleLabel.title
        titleLabel.font = Constants.TitleLabel.font
        titleLabel.textColor = Constants.TitleLabel.fontColor
    }
}

extension ObjectListViewController: UITableViewDelegate {

}

extension ObjectListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output?.getTableSize() ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ObjectListTableView.cellReuseIdentifier,
                                                       for: indexPath) as? ObjectListCell else {
            return UITableViewCell()
        }

        guard let viewModel = output?.object(indexPath: indexPath) else {
            return UITableViewCell()
        }

        cell.configure(object: viewModel)
        cell.selectionStyle = .none

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        output?.didSelectCell(indexPath: indexPath)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.ObjectListTableView.cellHeight
    }
}

extension ObjectListViewController: ObjectListViewInput {

}

extension ObjectListViewController {
    private struct Constants {

        static let backgroundColor: UIColor = UIColor(red: 94 / 255,
                                                      green: 92 / 255,
                                                      blue: 230 / 255,
                                                      alpha: 1)

        struct ObjectListTableView {
            static let backgroundColor: UIColor = UIColor(red: 1,
                                                   green: 211 / 255,
                                                   blue: 180 / 255,
                                                   alpha: 1)

            static let cellReuseIdentifier: String = "ObjectList"
            static let cornerRadius: CGFloat = 20

            static let cellHeight: CGFloat = 80
        }

        struct TitleLabel {
            static let title: String = "Choose target object"
            static let font: UIFont? = UIFont(name: "DMSans-Bold", size: 25)
            static let fontColor: UIColor = .white
        }
    }
}
