class ObjectListInteractor {

    weak var output: ObjectListInteractorOutput?

    private var model: ObjectListData

    init(model: ObjectListData) {
        self.model = model
    }
}

extension ObjectListInteractor: ObjectListInteractorInput {
    func getObjectListSize() -> Int {
        return model.closestObjects.count
    }

    func getObject(index: Int) -> ObjectWrapper {
        return model.closestObjects[index]
    }
}
