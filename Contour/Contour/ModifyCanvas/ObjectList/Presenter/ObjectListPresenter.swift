import Foundation

class ObjectListPresenter {

    weak var view: ObjectListViewInput?
    var router: ObjectListRouterInput?
    var interactor: ObjectListInteractorInput?

}

extension ObjectListPresenter: ObjectListInteractorOutput {

}

extension ObjectListPresenter: ObjectListViewOutput {
    func getTableSize() -> Int {
        return interactor?.getObjectListSize() ?? 0
    }

    func object(indexPath: IndexPath) -> ObjectWrapper? {
        return interactor?.getObject(index: indexPath.row)
    }

    func didSelectCell(indexPath: IndexPath) {
        guard let object = interactor?.getObject(index: indexPath.row) else {
            return
        }

        switch object.type {
        case .primitive:
            guard let primitive = object.object as? Primitive else {
                return
            }

            router?.showModifyPrimitiveScreen(primitive: primitive)
        case .vertex:
            guard let vertex = object.object as? Vertex else {
                return
            }

            router?.showModifyVertexScreen(vertex: vertex)
        }
    }

    func didLoadView() {

    }
}
