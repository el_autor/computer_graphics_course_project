import UIKit

class ObjectListContainer {
    let viewController: UIViewController

    class func assemble(context: ObjectListContext) -> ObjectListContainer {
        let storyboard = UIStoryboard(name: Storyboard.objectList.name, bundle: nil)

        guard let viewController = storyboard.instantiateViewController(withIdentifier: Storyboard.objectList.name) as? ObjectListViewController else {
            fatalError("ObjectListContainer: view controller must be type AddPrimitiveViewController")
        }

        let presenter = ObjectListPresenter()
        let router = ObjectListRouter()
        let interactor = ObjectListInteractor(model: ObjectListData(closestObjects: context.closestObjects))

        presenter.view = viewController
        presenter.router = router
        presenter.interactor = interactor

        router.viewController = viewController

        interactor.output = presenter

        viewController.output = presenter

        return ObjectListContainer(viewController: viewController)
    }

    private init(viewController: ObjectListViewController) {
        self.viewController = viewController
    }
}

struct ObjectListContext {
    var closestObjects: [ObjectWrapper]
}
