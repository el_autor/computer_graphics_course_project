import UIKit

protocol ObjectListViewInput: AnyObject {

}

protocol ObjectListViewOutput: AnyObject {
    func getTableSize() -> Int

    func object(indexPath: IndexPath) -> ObjectWrapper?

    func didSelectCell(indexPath: IndexPath)

    func didLoadView()
}

protocol ObjectListInteractorInput: AnyObject {
    func getObjectListSize() -> Int

    func getObject(index: Int) -> ObjectWrapper
}

protocol ObjectListInteractorOutput: AnyObject {

}

protocol ObjectListRouterInput: AnyObject {
    func showModifyPrimitiveScreen(primitive: Primitive)

    func showModifyVertexScreen(vertex: Vertex)
}
