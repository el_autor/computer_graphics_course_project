import Foundation

final class ModifyPrimitivePresenter {

    enum State {
        case move
        case scale
        case rotate
        case newCenter
        case color
    }

	weak var view: ModifyPrimitiveViewInput?

	private let router: ModifyPrimitiveRouterInput

	private let interactor: ModifyPrimitiveInteractorInput

    private var state: State?

    init(router: ModifyPrimitiveRouterInput, interactor: ModifyPrimitiveInteractorInput) {
        self.router = router
        self.interactor = interactor
    }
}

extension ModifyPrimitivePresenter: ModifyPrimitiveViewOutput {
    func didTapBackButton() {
        router.goToBackScreen()
    }

    func didTapDeleteButton() {
        interactor.deletePrimitive()
        router.goToCanvas()
    }

    func didTapApplyButton() {
        guard let param1 = view?.getFirstEntryText(),
              let number1 = Float(param1) else {
            return
        }

        guard let param2 = view?.getSecondEntryText(),
              let number2 = Float(param2) else {
            return
        }

        guard let param3 = view?.getThirdEntryText(),
              let number3 = Float(param3) else {
            return
        }

        switch state {
        case .move:
            interactor.movePrimitive(deltaX: number1,
                                     deltaY: number2,
                                     deltaZ: number3)

            let centerPoint = interactor.getPrimitiveCenter()
            let text = "Center - (\(lroundf(centerPoint.x)), \(lroundf(centerPoint.y)), \(lroundf(centerPoint.z)))"

            view?.setCenterPointLabelText(text: text)
        case .scale:
            interactor.scalePrimitive(koefX: number1,
                                      koefY: number2,
                                      koefZ: number3)
        case .rotate:
            interactor.rotatePrimitive(angleX: number1,
                                       angleY: number2,
                                       angleZ: number3)
        case .newCenter:
            interactor.setNewCenter(x: number1,
                                    y: number2,
                                    z: number3)
        case .color:
            interactor.setColor(r: number1,
                                g: number2,
                                b: number3)
        default:
            ()
        }

        router.goToCanvas()
    }

    func didTapMoveButton() {
        if state == nil {
            view?.showTextFields()
        }

        state = .move
        view?.setMoveTextFields()
    }

    func didTapScaleButton() {
        if state == nil {
            view?.showTextFields()
        }

        state = .scale
        view?.setScaleTextFields()
    }

    func didTapRotateButton() {
        if state == nil {
            view?.showTextFields()
        }

        state = .rotate
        view?.setRotateTextFields()
    }

    func didTapNewCenterButton() {
        if state == nil {
            view?.showTextFields()
        }

        state = .newCenter
        view?.setNewCenterTextFields()
    }

    func didTapColorButton() {
        if state == nil {
            view?.showTextFields()
        }

        state = .color
        view?.setColorTextFields()
    }

    func didLoadView() {
        view?.setPrimitiveLabelText(text: interactor.getPrimitiveName())

        let centerPoint = interactor.getPrimitiveCenter()
        let text = "Center - (\(lroundf(centerPoint.x)), \(lroundf(centerPoint.y)), \(lroundf(centerPoint.z)))"

        view?.setCenterPointLabelText(text: text)
    }
}

extension ModifyPrimitivePresenter: ModifyPrimitiveInteractorOutput {
}
