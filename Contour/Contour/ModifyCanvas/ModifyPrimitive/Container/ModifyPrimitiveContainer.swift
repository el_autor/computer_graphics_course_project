import UIKit

final class ModifyPrimitiveContainer {
	let viewController: UIViewController
	private(set) weak var router: ModifyPrimitiveRouterInput!

	class func assemble(with context: ModifyPrimitiveContext) -> ModifyPrimitiveContainer {
        let router = ModifyPrimitiveRouter()
        let interactor = ModifyPrimitiveInteractor(primitive: context.primitive)
        let presenter = ModifyPrimitivePresenter(router: router, interactor: interactor)
        let viewController = ModifyPrimitiveViewController()

        viewController.output = presenter
        presenter.view = viewController
        interactor.output = presenter
        router.viewController = viewController

        return ModifyPrimitiveContainer(view: viewController, router: router)
	}

    private init(view: UIViewController, router: ModifyPrimitiveRouterInput) {
		self.viewController = view
		self.router = router
	}
}

struct ModifyPrimitiveContext {
    var primitive: Primitive
}
