import UIKit

final class ModifyPrimitiveRouter {
    weak var viewController: UIViewController?
}

extension ModifyPrimitiveRouter: ModifyPrimitiveRouterInput {
    func goToBackScreen() {
        viewController?.navigationController?.popViewController(animated: true)
    }

    func goToCanvas() {
        guard let canvas = viewController?.navigationController?.presentingViewController as? CanvasViewController else {
            return
        }

        canvas.updateCanvas()
        viewController?.navigationController?.dismiss(animated: true, completion: nil)
    }
}
