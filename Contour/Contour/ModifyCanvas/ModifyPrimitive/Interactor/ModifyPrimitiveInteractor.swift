import Foundation

final class ModifyPrimitiveInteractor {
	weak var output: ModifyPrimitiveInteractorOutput?

    private var primitive: Primitive

    init(primitive: Primitive) {
        self.primitive = primitive
    }
}

extension ModifyPrimitiveInteractor: ModifyPrimitiveInteractorInput {
    func getPrimitiveName() -> String {
        return primitive.name
    }

    func deletePrimitive() {
        DataService.shared.deletePrimitive(primitive: primitive)
    }

    func getPrimitiveCenter() -> Point {
        return Point(x: primitive.center.x, y: primitive.center.y, z: primitive.center.z)
    }

    func movePrimitive(deltaX: Float, deltaY: Float, deltaZ: Float) {
        DataService.shared.movePrimitive(primitive: primitive,
                                         deltaX: deltaX,
                                         deltaY: deltaY,
                                         deltaZ: deltaZ)
    }

    func scalePrimitive(koefX: Float, koefY: Float, koefZ: Float) {
        DataService.shared.scalePrimitive(primitive: primitive,
                                          koefX: koefX,
                                          koefY: koefY,
                                          koefZ: koefZ)
    }

    func rotatePrimitive(angleX: Float, angleY: Float, angleZ: Float) {
        DataService.shared.rotatePrimitive(primitive: primitive,
                                           angleX: angleX,
                                           angleY: angleY,
                                           angleZ: angleZ)
    }

    func setNewCenter(x: Float, y: Float, z: Float) {
        DataService.shared.setPrimitiveNewCenter(primitive: primitive,
                                                 x: x,
                                                 y: y,
                                                 z: z)
    }

    func setColor(r: Float, g: Float, b: Float) {
        DataService.shared.setPrimitiveColor(primitive: primitive,
                                             r: r,
                                             g: g,
                                             b: b)
    }
}
