import UIKit
import PinLayout

final class ModifyPrimitiveViewController: UIViewController {
	var output: ModifyPrimitiveViewOutput?

    // MARK: UI elements

    private weak var backButton: UIButton!

    private weak var descriptionLabel: UILabel!

    private weak var frameView: UIView!

    private weak var primitiveNameLabel: UILabel!

    private weak var deleteButton: UIButton!

    private weak var centerPointLabel: UILabel!

    private weak var moveButton: UIButton!

    private weak var scaleButton: UIButton!

    private weak var rotateButton: UIButton!

    private weak var newCenterButton: UIButton!

    private weak var colorButton: UIButton!

    private weak var firstParameterTextField: UITextField!

    private weak var secondParameterTextField: UITextField!

    private weak var thirdParameterTextField: UITextField!

    private weak var applyButton: UIButton!

	override func viewDidLoad() {
		super.viewDidLoad()

        setupView()
        setupSubviews()

        output?.didLoadView()
	}

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        layoutBackButton()
        layoutDescriptionLabel()
        layoutFrameView()
        layoutPrimitiveNameLabel()
        layoutCenterPointLabel()
        layoutMoveButton()
        layoutScaleButton()
        layoutRotateButton()
        layoutNewCenterButton()
        layoutColorButton()
        layoutFirstParameterTextField()
        layoutSecondParameterTextField()
        layoutThirdParameterTextField()
        layoutDeleteButton()
        layoutApplyButton()
    }

    private func setupView() {
        view.backgroundColor = Constants.backgroundColor
    }

    private func setupSubviews() {
        setupBackButton()
        setupDescriptionLabel()
        setupFrameView()
        setupPrimitiveNameLabel()
        setupDeleteButton()
        setupCenterPointLabel()
        setupMoveButton()
        setupScaleButton()
        setupRotateButton()
        setupNewCenterButton()
        setupColorButton()
        setupFirstParameterTextField()
        setupSecondParameterTextField()
        setupThirdParameterTextField()
        setupApplyButton()
    }

    private func setupBackButton() {
        let button = UIButton()

        backButton = button
        view.addSubview(backButton)

        backButton.setImage(UIImage(systemName: "chevron.backward",
                                    withConfiguration: UIImage.SymbolConfiguration(weight: .bold)),
                                    for: .normal)

        backButton.tintColor = .white
        backButton.contentVerticalAlignment = .fill
        backButton.contentHorizontalAlignment = .fill

        backButton.addTarget(self, action: #selector(didTapBackButton), for: .touchUpInside)
    }

    private func setupDescriptionLabel() {
        let label = UILabel()

        descriptionLabel = label
        view.addSubview(label)

        descriptionLabel.textAlignment = .center
        descriptionLabel.text = "Modify primitive"
        descriptionLabel.font = UIFont(name: "DMSans-Bold", size: 25)
        descriptionLabel.textColor = .white
    }

    private func setupFrameView() {
        let frame = UIView()

        frameView = frame
        view.addSubview(frame)

        frameView.backgroundColor = Constants.FrameView.backgroundColor
        frameView.layer.cornerRadius = 20
    }

    private func setupDeleteButton() {
        let button = UIButton()

        deleteButton = button
        frameView.addSubview(deleteButton)

        deleteButton.backgroundColor = .systemRed
        deleteButton.setTitle("Delete", for: .normal)
        deleteButton.layer.cornerRadius = 10
        deleteButton.titleLabel?.font = UIFont(name: "DMSans-Regular", size: 20)
        deleteButton.setTitleColor(.white, for: .normal)

        deleteButton.addTarget(self, action: #selector(didTapDeleteButton), for: .touchUpInside)
    }

    private func setupPrimitiveNameLabel() {
        let label = UILabel()

        primitiveNameLabel = label
        frameView.addSubview(primitiveNameLabel)

        primitiveNameLabel.textAlignment = .center
        primitiveNameLabel.font = UIFont(name: "DMSans-Bold", size: 25)
        primitiveNameLabel.textColor = .black
    }

    private func setupCenterPointLabel() {
        let label = UILabel()

        centerPointLabel = label
        frameView.addSubview(centerPointLabel)

        centerPointLabel.textAlignment = .center
        centerPointLabel.font = UIFont(name: "DMSans-Bold", size: 20)
        centerPointLabel.textColor = .black
    }

    private func setupMoveButton() {
        let button = UIButton()

        moveButton = button
        frameView.addSubview(moveButton)

        moveButton.backgroundColor = Constants.backgroundColor
        moveButton.setTitle("Move", for: .normal)
        moveButton.layer.cornerRadius = 10
        moveButton.titleLabel?.font = UIFont(name: "DMSans-Bold", size: 20)
        moveButton.setTitleColor(.white, for: .normal)

        moveButton.addTarget(self, action: #selector(didTapMoveButton), for: .touchUpInside)
    }

    private func setupScaleButton() {
        let button = UIButton()

        scaleButton = button
        frameView.addSubview(scaleButton)

        scaleButton.backgroundColor = Constants.backgroundColor
        scaleButton.setTitle("Scale", for: .normal)
        scaleButton.layer.cornerRadius = 10
        scaleButton.titleLabel?.font = UIFont(name: "DMSans-Bold", size: 20)
        scaleButton.setTitleColor(.white, for: .normal)

        scaleButton.addTarget(self, action: #selector(didTapScaleButton), for: .touchUpInside)
    }

    private func setupRotateButton() {
        let button = UIButton()

        rotateButton = button
        frameView.addSubview(rotateButton)

        rotateButton.backgroundColor = Constants.backgroundColor
        rotateButton.setTitle("Rotate", for: .normal)
        rotateButton.layer.cornerRadius = 10
        rotateButton.titleLabel?.font = UIFont(name: "DMSans-Bold", size: 20)
        rotateButton.setTitleColor(.white, for: .normal)

        rotateButton.addTarget(self, action: #selector(didTapRotateButton), for: .touchUpInside)
    }

    private func setupNewCenterButton() {
        let button = UIButton()

        newCenterButton = button
        frameView.addSubview(newCenterButton)

        newCenterButton.backgroundColor = Constants.backgroundColor
        newCenterButton.setTitle("Center", for: .normal)
        newCenterButton.layer.cornerRadius = 10
        newCenterButton.titleLabel?.font = UIFont(name: "DMSans-Bold", size: 20)
        newCenterButton.setTitleColor(.white, for: .normal)

        newCenterButton.addTarget(self, action: #selector(didTapNewCenterButton), for: .touchUpInside)
    }

    private func setupColorButton() {
        let button = UIButton()

        colorButton = button
        frameView.addSubview(colorButton)

        colorButton.backgroundColor = Constants.backgroundColor
        colorButton.setTitle("Color", for: .normal)
        colorButton.layer.cornerRadius = 10
        colorButton.titleLabel?.font = UIFont(name: "DMSans-Bold", size: 20)
        colorButton.setTitleColor(.white, for: .normal)

        colorButton.addTarget(self, action: #selector(didTapColorButton), for: .touchUpInside)
    }

    private func setupFirstParameterTextField() {
        let textField = UITextField()

        firstParameterTextField = textField
        frameView.addSubview(firstParameterTextField)

        textField.isHidden = true

        textField.attributedPlaceholder = NSAttributedString(string: String(),
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])

        textField.clipsToBounds = true
        textField.textColor = .black
        textField.layer.cornerRadius = 10
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.white.cgColor
        textField.backgroundColor = UIColor.white
        textField.font = UIFont(name: "DMSans-Regular", size: 20)

        textField.leftView = UIView(frame: CGRect(x: .zero,
                                                      y: .zero,
                                                      width: 10,
                                                      height: .zero))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: .zero,
                                                       y: .zero,
                                                       width: 10,
                                                       height: .zero))
        textField.rightViewMode = .unlessEditing

        textField.clearButtonMode = .whileEditing
        textField.autocorrectionType = .no
    }

    private func setupSecondParameterTextField() {
        let textField = UITextField()

        secondParameterTextField = textField
        frameView.addSubview(secondParameterTextField)

        textField.isHidden = true

        textField.attributedPlaceholder = NSAttributedString(string: String(),
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])

        textField.clipsToBounds = true
        textField.textColor = .black
        textField.layer.cornerRadius = 10
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.white.cgColor
        textField.backgroundColor = UIColor.white
        textField.font = UIFont(name: "DMSans-Regular", size: 20)

        textField.leftView = UIView(frame: CGRect(x: .zero,
                                                      y: .zero,
                                                      width: 10,
                                                      height: .zero))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: .zero,
                                                       y: .zero,
                                                       width: 10,
                                                       height: .zero))
        textField.rightViewMode = .unlessEditing

        textField.clearButtonMode = .whileEditing
        textField.autocorrectionType = .no
    }

    private func setupThirdParameterTextField() {
        let textField = UITextField()

        thirdParameterTextField = textField
        frameView.addSubview(thirdParameterTextField)

        textField.isHidden = true

        textField.attributedPlaceholder = NSAttributedString(string: String(),
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])

        textField.clipsToBounds = true
        textField.textColor = .black
        textField.layer.cornerRadius = 10
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.white.cgColor
        textField.backgroundColor = UIColor.white
        textField.font = UIFont(name: "DMSans-Regular", size: 20)

        textField.leftView = UIView(frame: CGRect(x: .zero,
                                                      y: .zero,
                                                      width: 10,
                                                      height: .zero))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: .zero,
                                                       y: .zero,
                                                       width: 10,
                                                       height: .zero))
        textField.rightViewMode = .unlessEditing

        textField.clearButtonMode = .whileEditing
        textField.autocorrectionType = .no
    }

    private func setupApplyButton() {
        let button = UIButton()

        applyButton = button
        view.addSubview(button)

        applyButton.backgroundColor = Constants.FrameView.backgroundColor
        applyButton.setTitle("Apply", for: .normal)
        applyButton.layer.cornerRadius = 10
        applyButton.titleLabel?.font = UIFont(name: "DMSans-Regular", size: 20)
        applyButton.setTitleColor(.black, for: .normal)

        applyButton.addTarget(self, action: #selector(didTapApplyButton), for: .touchUpInside)
    }

    private func layoutBackButton() {
        backButton.pin
            .height(25)
            .width(20)
            .top(2%)
            .left(2%)
    }

    private func layoutDescriptionLabel() {
        descriptionLabel.pin
            .top(3%)
            .height(4%)
            .width(50%)
            .hCenter()
    }

    private func layoutFrameView() {
        frameView.pin
            .hCenter()
            .vCenter()
            .width(75%)
            .height(80%)
    }

    private func layoutPrimitiveNameLabel() {
        primitiveNameLabel.pin
            .top(5%)
            .height(4%)
            .hCenter()
            .width(60%)
    }

    private func layoutDeleteButton() {
        deleteButton.pin
            .below(of: firstParameterTextField).marginTop(3%)
            .width(20%)
            .hCenter()
            .height(5%)
    }

    private func layoutCenterPointLabel() {
        centerPointLabel.pin
            .below(of: primitiveNameLabel).marginTop(5%)
            .height(4%)
            .hCenter()
            .width(90%)
    }

    private func layoutMoveButton() {
        moveButton.pin
            .below(of: centerPointLabel).marginTop(5%)
            .hCenter()
            .width(60%)
            .height(7%)
    }

    private func layoutScaleButton() {
        scaleButton.pin
            .below(of: moveButton).marginTop(5%)
            .hCenter()
            .width(60%)
            .height(7%)
    }

    private func layoutRotateButton() {
        rotateButton.pin
            .below(of: scaleButton).marginTop(5%)
            .hCenter()
            .width(60%)
            .height(7%)
    }

    private func layoutNewCenterButton() {
        newCenterButton.pin
            .below(of: rotateButton).marginTop(5%)
            .hCenter()
            .width(60%)
            .height(7%)
    }

    private func layoutColorButton() {
        colorButton.pin
            .below(of: newCenterButton).marginTop(5%)
            .hCenter()
            .width(60%)
            .height(7%)
    }

    private func layoutFirstParameterTextField() {
        firstParameterTextField.pin
            .left(10%)
            .below(of: colorButton).marginTop(4%)
            .width(20%)
            .height(7%)
    }

    private func layoutSecondParameterTextField() {
        secondParameterTextField.pin
            .right(of: firstParameterTextField).marginLeft(10%)
            .below(of: colorButton).marginTop(4%)
            .width(20%)
            .height(7%)
    }

    private func layoutThirdParameterTextField() {
        thirdParameterTextField.pin
            .right(of: secondParameterTextField).marginLeft(10%)
            .below(of: colorButton).marginTop(4%)
            .width(20%)
            .height(7%)
    }

    private func layoutApplyButton() {
        applyButton.pin
            .below(of: frameView).marginTop(2.5%)
            .height(5%)
            .hCenter()
            .width(30%)
    }

    private func setPlaceholders(first: String, second: String, third: String) {
        firstParameterTextField.placeholder = first
        secondParameterTextField.placeholder = second
        thirdParameterTextField.placeholder = third
    }

    @objc
    private func didTapBackButton() {
        output?.didTapBackButton()
    }

    @objc
    private func didTapDeleteButton() {
        output?.didTapDeleteButton()
    }

    @objc
    private func didTapApplyButton() {
        output?.didTapApplyButton()
    }

    @objc
    private func didTapMoveButton() {
        output?.didTapMoveButton()
    }

    @objc
    private func didTapScaleButton() {
        output?.didTapScaleButton()
    }

    @objc
    private func didTapRotateButton() {
        output?.didTapRotateButton()
    }

    @objc
    private func didTapNewCenterButton() {
        output?.didTapNewCenterButton()
    }

    @objc
    private func didTapColorButton() {
        output?.didTapColorButton()
    }
}

extension ModifyPrimitiveViewController: ModifyPrimitiveViewInput {
    func setPrimitiveLabelText(text: String) {
        primitiveNameLabel.text = text
    }

    func setCenterPointLabelText(text: String) {
        centerPointLabel.text = text
    }

    func getFirstEntryText() -> String? {
        return firstParameterTextField.text
    }

    func getSecondEntryText() -> String? {
        return secondParameterTextField.text
    }

    func getThirdEntryText() -> String? {
        return thirdParameterTextField.text
    }

    func setMoveTextFields() {
        setPlaceholders(first: "dx", second: "dy", third: "dz")
    }

    func setScaleTextFields() {
        setPlaceholders(first: "kx", second: "ky", third: "kz")
    }

    func setRotateTextFields() {
        setPlaceholders(first: "x˚", second: "y˚", third: "z˚")
    }

    func setNewCenterTextFields() {
        setPlaceholders(first: "x", second: "y", third: "z")
    }

    func setColorTextFields() {
        setPlaceholders(first: "r", second: "g", third: "b")
    }

    func showTextFields() {
        firstParameterTextField.isHidden = false
        secondParameterTextField.isHidden = false
        thirdParameterTextField.isHidden = false
    }

}

extension ModifyPrimitiveViewController {
    private struct Constants {
        static let backgroundColor: UIColor = UIColor(red: 94 / 255,
                                                      green: 92 / 255,
                                                      blue: 230 / 255,
                                                      alpha: 1)

        struct FrameView {
            static let backgroundColor: UIColor = UIColor(red: 1,
                                                   green: 211 / 255,
                                                   blue: 180 / 255,
                                                   alpha: 1)
        }
    }
}
