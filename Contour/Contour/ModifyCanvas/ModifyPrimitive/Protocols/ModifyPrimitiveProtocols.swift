import Foundation

protocol ModifyPrimitiveViewInput: AnyObject {
    func setPrimitiveLabelText(text: String)

    func setCenterPointLabelText(text: String)

    func getFirstEntryText() -> String?

    func getSecondEntryText() -> String?

    func getThirdEntryText() -> String?

    func setMoveTextFields()

    func setScaleTextFields()

    func setRotateTextFields()

    func setNewCenterTextFields()

    func setColorTextFields()

    func showTextFields()
}

protocol ModifyPrimitiveViewOutput: AnyObject {
    func didLoadView()

    func didTapBackButton()

    func didTapMoveButton()

    func didTapScaleButton()

    func didTapRotateButton()

    func didTapNewCenterButton()

    func didTapDeleteButton()

    func didTapApplyButton()

    func didTapColorButton()
}

protocol ModifyPrimitiveInteractorInput: AnyObject {
    func getPrimitiveName() -> String

    func getPrimitiveCenter() -> Point

    func deletePrimitive()

    func movePrimitive(deltaX: Float, deltaY: Float, deltaZ: Float)

    func scalePrimitive(koefX: Float, koefY: Float, koefZ: Float)

    func rotatePrimitive(angleX: Float, angleY: Float, angleZ: Float)

    func setNewCenter(x: Float, y: Float, z: Float)

    func setColor(r: Float, g: Float, b: Float)
}

protocol ModifyPrimitiveInteractorOutput: AnyObject {
}

protocol ModifyPrimitiveRouterInput: AnyObject {
    func goToBackScreen()

    func goToCanvas()
}
