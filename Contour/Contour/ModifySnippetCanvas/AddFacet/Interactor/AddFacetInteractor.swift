import Foundation

final class AddFacetInteractor {
	weak var output: AddFacetInteractorOutput?

    private var snippet: Snippet?

    init(snippetName: String, authorName: String) {
        snippet = DataService.shared.getSnippet(name: snippetName,
                                                authorName: authorName)
    }
}

extension AddFacetInteractor: AddFacetInteractorInput {
    func createFacet(pointA: Point, pointB: Point, pointC: Point) {
        guard let snippet = snippet else {
            return
        }

        DataService.shared.addFacetToSnippet(snippet: snippet,
                                             pointA: pointA,
                                             pointB: pointB,
                                             pointC: pointC)
    }
}
