import Foundation

protocol AddFacetViewInput: AnyObject {
    func getFirstXEntryText() -> String?

    func getFirstYEntryText() -> String?

    func getFirstZEntryText() -> String?

    func getSecondXEntryText() -> String?

    func getSecondYEntryText() -> String?

    func getSecondZEntryText() -> String?

    func getThirdXEntryText() -> String?

    func getThirdYEntryText() -> String?

    func getThirdZEntryText() -> String?
}

protocol AddFacetViewOutput: AnyObject {
    func didTapApplyButton()
}

protocol AddFacetInteractorInput: AnyObject {
    func createFacet(pointA: Point, pointB: Point, pointC: Point)
}

protocol AddFacetInteractorOutput: AnyObject {
}

protocol AddFacetRouterInput: AnyObject {
    func goToCanvas()
}
