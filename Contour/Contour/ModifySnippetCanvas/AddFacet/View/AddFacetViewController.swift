import UIKit
import PinLayout

final class AddFacetViewController: UIViewController {
	var output: AddFacetViewOutput?

    private weak var descriptionLabel: UILabel!

    private weak var frameView: UIView!

    private weak var firstVertexLabel: UILabel!

    private weak var firstVertexXTextField: UITextField!

    private weak var firstVertexYTextField: UITextField!

    private weak var firstVertexZTextField: UITextField!

    private weak var secondVertexLabel: UILabel!

    private weak var secondVertexXTextField: UITextField!

    private weak var secondVertexYTextField: UITextField!

    private weak var secondVertexZTextField: UITextField!

    private weak var thirdVertexLabel: UILabel!

    private weak var thirdVertexXTextField: UITextField!

    private weak var thirdVertexYTextField: UITextField!

    private weak var thirdVertexZTextField: UITextField!

    private weak var applyButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupSubviews()

        // output?.didLoadView()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        layoutDescriptionLabel()
        layoutFrameView()
        layoutFirstVertexLabel()
        layoutFirstVertexTextFields()
        layoutSecondVertexLabel()
        layoutSecondVertexTextFields()
        layoutThirdVertexLabel()
        layoutThirdVertexTextFields()
        layoutApplyButton()
    }

    private func setupView() {
        view.backgroundColor = Constants.backgroundColor
    }

    private func setupSubviews() {
        setupDescriptionLabel()
        setupFrameView()
        setupFirstVertexLabel()
        setupFirstVertexTextFields()
        setupSecondVertexLabel()
        setupSecondVertexTextFields()
        setupThirdVertexLabel()
        setupThirdVertexTextFields()
        setupApplyButton()
    }

    private func setupDescriptionLabel() {
        let label = UILabel()

        descriptionLabel = label
        view.addSubview(label)

        descriptionLabel.textAlignment = .center
        descriptionLabel.text = "Create facet"
        descriptionLabel.font = UIFont(name: "DMSans-Bold", size: 25)
        descriptionLabel.textColor = .white
    }

    private func setupFrameView() {
        let frame = UIView()

        frameView = frame
        view.addSubview(frame)

        frameView.backgroundColor = Constants.FrameView.backgroundColor
        frameView.layer.cornerRadius = 20
    }

    private func setupFirstVertexLabel() {
        let label = UILabel()

        firstVertexLabel = label
        frameView.addSubview(firstVertexLabel)

        firstVertexLabel.text = "vertex A"
        firstVertexLabel.textAlignment = .center
        firstVertexLabel.font = UIFont(name: "DMSans-Bold", size: 25)
        firstVertexLabel.textColor = .black
    }

    private func setupFirstVertexTextFields() {
        let xTextField = createTextField(placeholderText: "x")
        let yTextField = createTextField(placeholderText: "y")
        let zTextField = createTextField(placeholderText: "z")

        firstVertexXTextField = xTextField
        firstVertexYTextField = yTextField
        firstVertexZTextField = zTextField

        frameView.addSubview(firstVertexXTextField)
        frameView.addSubview(firstVertexYTextField)
        frameView.addSubview(firstVertexZTextField)
    }

    private func setupSecondVertexLabel() {
        let label = UILabel()

        secondVertexLabel = label
        frameView.addSubview(secondVertexLabel)

        secondVertexLabel.text = "vertex B"
        secondVertexLabel.textAlignment = .center
        secondVertexLabel.font = UIFont(name: "DMSans-Bold", size: 25)
        secondVertexLabel.textColor = .black
    }

    private func setupSecondVertexTextFields() {
        let xTextField = createTextField(placeholderText: "x")
        let yTextField = createTextField(placeholderText: "y")
        let zTextField = createTextField(placeholderText: "z")

        secondVertexXTextField = xTextField
        secondVertexYTextField = yTextField
        secondVertexZTextField = zTextField

        frameView.addSubview(secondVertexXTextField)
        frameView.addSubview(secondVertexYTextField)
        frameView.addSubview(secondVertexZTextField)
    }

    private func setupThirdVertexLabel() {
        let label = UILabel()

        thirdVertexLabel = label
        frameView.addSubview(thirdVertexLabel)

        thirdVertexLabel.text = "vertex C"
        thirdVertexLabel.textAlignment = .center
        thirdVertexLabel.font = UIFont(name: "DMSans-Bold", size: 25)
        thirdVertexLabel.textColor = .black
    }

    private func setupThirdVertexTextFields() {
        let xTextField = createTextField(placeholderText: "x")
        let yTextField = createTextField(placeholderText: "y")
        let zTextField = createTextField(placeholderText: "z")

        thirdVertexXTextField = xTextField
        thirdVertexYTextField = yTextField
        thirdVertexZTextField = zTextField

        frameView.addSubview(thirdVertexXTextField)
        frameView.addSubview(thirdVertexYTextField)
        frameView.addSubview(thirdVertexZTextField)
    }

    private func createTextField(placeholderText: String) -> UITextField {
        let textField = UITextField()

        textField.attributedPlaceholder = NSAttributedString(string: placeholderText,
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])

        textField.clipsToBounds = true
        textField.textColor = .black
        textField.layer.cornerRadius = 10
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.white.cgColor
        textField.backgroundColor = UIColor.white
        textField.font = UIFont(name: "DMSans-Regular", size: 20)

        textField.leftView = UIView(frame: CGRect(x: .zero,
                                                      y: .zero,
                                                      width: 10,
                                                      height: .zero))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: .zero,
                                                       y: .zero,
                                                       width: 10,
                                                       height: .zero))
        textField.rightViewMode = .unlessEditing

        textField.clearButtonMode = .whileEditing
        textField.autocorrectionType = .no

        return textField
    }

    private func setupApplyButton() {
        let button = UIButton()

        applyButton = button
        view.addSubview(button)

        applyButton.backgroundColor = Constants.FrameView.backgroundColor
        applyButton.setTitle("Apply", for: .normal)
        applyButton.layer.cornerRadius = 10
        applyButton.titleLabel?.font = UIFont(name: "DMSans-Regular", size: 20)
        applyButton.setTitleColor(.black, for: .normal)

        applyButton.addTarget(self, action: #selector(didTapApplyButton), for: .touchUpInside)
    }

    private func layoutDescriptionLabel() {
        descriptionLabel.pin
            .top(3%)
            .height(4%)
            .width(50%)
            .hCenter()
    }

    private func layoutFrameView() {
        frameView.pin
            .hCenter()
            .vCenter()
            .width(75%)
            .height(80%)
    }

    private func layoutFirstVertexLabel() {
        firstVertexLabel.pin
            .top(5%)
            .hCenter()
            .width(100%)
            .height(5%)
    }

    private func layoutFirstVertexTextFields() {
        firstVertexXTextField.pin
            .left(10%)
            .below(of: firstVertexLabel).marginTop(5%)
            .width(20%)
            .height(7%)

        firstVertexYTextField.pin
            .right(of: firstVertexXTextField).marginLeft(10%)
            .below(of: firstVertexLabel).marginTop(5%)
            .width(20%)
            .height(7%)

        firstVertexZTextField.pin
            .right(of: firstVertexYTextField).marginLeft(10%)
            .below(of: firstVertexLabel).marginTop(5%)
            .width(20%)
            .height(7%)
    }

    private func layoutSecondVertexLabel() {
        secondVertexLabel.pin
            .below(of: firstVertexYTextField).marginTop(5%)
            .hCenter()
            .width(100%)
            .height(5%)
    }

    private func layoutSecondVertexTextFields() {
        secondVertexXTextField.pin
            .left(10%)
            .below(of: secondVertexLabel).marginTop(5%)
            .width(20%)
            .height(7%)

        secondVertexYTextField.pin
            .right(of: secondVertexXTextField).marginLeft(10%)
            .below(of: secondVertexLabel).marginTop(5%)
            .width(20%)
            .height(7%)

        secondVertexZTextField.pin
            .right(of: secondVertexYTextField).marginLeft(10%)
            .below(of: secondVertexLabel).marginTop(5%)
            .width(20%)
            .height(7%)
    }

    private func layoutThirdVertexLabel() {
        thirdVertexLabel.pin
            .below(of: secondVertexYTextField).marginTop(5%)
            .hCenter()
            .width(100%)
            .height(5%)
    }

    private func layoutThirdVertexTextFields() {
        thirdVertexXTextField.pin
            .left(10%)
            .below(of: thirdVertexLabel).marginTop(5%)
            .width(20%)
            .height(7%)

        thirdVertexYTextField.pin
            .right(of: thirdVertexXTextField).marginLeft(10%)
            .below(of: thirdVertexLabel).marginTop(5%)
            .width(20%)
            .height(7%)

        thirdVertexZTextField.pin
            .right(of: thirdVertexYTextField).marginLeft(10%)
            .below(of: thirdVertexLabel).marginTop(5%)
            .width(20%)
            .height(7%)
    }

    private func layoutApplyButton() {
        applyButton.pin
            .below(of: frameView).marginTop(2.5%)
            .height(5%)
            .hCenter()
            .width(30%)
    }

    @objc
    private func didTapApplyButton() {
        output?.didTapApplyButton()
    }
}

extension AddFacetViewController: AddFacetViewInput {
    func getFirstXEntryText() -> String? {
        return firstVertexXTextField.text
    }

    func getFirstYEntryText() -> String? {
        return firstVertexYTextField.text
    }

    func getFirstZEntryText() -> String? {
        return firstVertexZTextField.text
    }

    func getSecondXEntryText() -> String? {
        return secondVertexXTextField.text
    }

    func getSecondYEntryText() -> String? {
        return secondVertexYTextField.text
    }

    func getSecondZEntryText() -> String? {
        return secondVertexZTextField.text
    }

    func getThirdXEntryText() -> String? {
        return thirdVertexXTextField.text
    }

    func getThirdYEntryText() -> String? {
        return thirdVertexYTextField.text
    }

    func getThirdZEntryText() -> String? {
        return thirdVertexZTextField.text
    }
}

extension AddFacetViewController {
    private struct Constants {
        static let backgroundColor: UIColor = UIColor(red: 94 / 255,
                                                      green: 92 / 255,
                                                      blue: 230 / 255,
                                                      alpha: 1)

        struct FrameView {
            static let backgroundColor: UIColor = UIColor(red: 1,
                                                   green: 211 / 255,
                                                   blue: 180 / 255,
                                                   alpha: 1)
        }
    }
}
