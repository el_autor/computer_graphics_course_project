import UIKit

final class AddFacetRouter {
    weak var viewController: UIViewController?
}

extension AddFacetRouter: AddFacetRouterInput {
    func goToCanvas() {
        guard let canvas = viewController?.navigationController?.presentingViewController as? SnippetCanvasViewController else {
            return
        }

        canvas.updateCanvas()
        viewController?.navigationController?.dismiss(animated: true, completion: nil)
    }
}
