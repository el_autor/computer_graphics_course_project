import UIKit

final class AddFacetContainer {
	let viewController: UIViewController
	private(set) weak var router: AddFacetRouterInput!

	class func assemble(with context: AddFacetContext) -> AddFacetContainer {
        let router = AddFacetRouter()
        let interactor = AddFacetInteractor(snippetName: context.snippetName, authorName: context.authorName)
        let presenter = AddFacetPresenter(router: router, interactor: interactor)
        let viewController = AddFacetViewController()

        viewController.output = presenter
        presenter.view = viewController
        interactor.output = presenter
        router.viewController = viewController

        return AddFacetContainer(view: viewController, router: router)
	}

    private init(view: UIViewController, router: AddFacetRouterInput) {
		self.viewController = view
		self.router = router
	}
}

struct AddFacetContext {
    var snippetName: String

    var authorName: String
}
