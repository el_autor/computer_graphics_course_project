import Foundation

final class AddFacetPresenter {
	weak var view: AddFacetViewInput?

	private let router: AddFacetRouterInput
	private let interactor: AddFacetInteractorInput

    init(router: AddFacetRouterInput, interactor: AddFacetInteractorInput) {
        self.router = router
        self.interactor = interactor
    }
}

extension AddFacetPresenter: AddFacetViewOutput {
    func didTapApplyButton() {
        guard let numberX1 = view?.getFirstXEntryText(),
              let firstX = Float(numberX1) else {
            return
        }

        guard let numberY1 = view?.getFirstYEntryText(),
              let firstY = Float(numberY1) else {
            return
        }

        guard let numberZ1 = view?.getFirstZEntryText(),
              let firstZ = Float(numberZ1) else {
            return
        }

        guard let numberX2 = view?.getSecondXEntryText(),
              let secondX = Float(numberX2) else {
            return
        }

        guard let numberY2 = view?.getSecondYEntryText(),
              let secondY = Float(numberY2) else {
            return
        }

        guard let numberZ2 = view?.getSecondZEntryText(),
              let secondZ = Float(numberZ2) else {
            return
        }

        guard let numberX3 = view?.getThirdXEntryText(),
              let thirdX = Float(numberX3) else {
            return
        }

        guard let numberY3 = view?.getThirdYEntryText(),
              let thirdY = Float(numberY3) else {
            return
        }

        guard let numberZ3 = view?.getThirdZEntryText(),
              let thirdZ = Float(numberZ3) else {
            return
        }

        let pointA = Point(x: firstX, y: firstY, z: firstZ)
        let pointB = Point(x: secondX, y: secondY, z: secondZ)
        let pointC = Point(x: thirdX, y: thirdY, z: thirdZ)

        interactor.createFacet(pointA: pointA,
                               pointB: pointB,
                               pointC: pointC)

        router.goToCanvas()
    }
}

extension AddFacetPresenter: AddFacetInteractorOutput {
}
