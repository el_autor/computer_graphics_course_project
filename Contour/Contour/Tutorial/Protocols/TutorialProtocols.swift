protocol TutorialViewInput: class {

}

protocol TutorialViewOutput: class {
    func didTapBackToMenuButton()
}

protocol TutorialRouterInput: class {
    func showMainScreen()
}
