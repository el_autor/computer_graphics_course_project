import UIKit

class TutorialViewController: UIViewController {

    var output: TutorialViewOutput?

    @IBOutlet private weak var textContainerView: UIView!

    @IBOutlet private weak var textView: UITextView!

    @IBOutlet private weak var backToMenuButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupSubviews()
    }

    private func setupView() {
        view.backgroundColor = Constants.backgroundColor
    }

    private func setupTextContainerView() {
        textContainerView.backgroundColor = Constants.TextContainerView.backgroundColor

        textContainerView.layer.cornerRadius = Constants.TextContainerView.cornerRadius
    }

    private func setupTextView() {
        textView.isEditable = false
        textView.isSelectable = false
        textView.isScrollEnabled = true
        textView.showsVerticalScrollIndicator = false

        textView.font = Constants.TextView.font

        textView.text = Constants.TextView.text
    }

    private func setupBackToMenuButton() {
        backToMenuButton.backgroundColor = Constants.BackToMenuButton.backgroundColor

        backToMenuButton.layer.cornerRadius = Constants.BackToMenuButton.cornerRadius

        backToMenuButton.setTitle(Constants.BackToMenuButton.title, for: .normal)
        backToMenuButton.setTitleColor(.black, for: .normal)
        backToMenuButton.titleLabel?.font = Constants.BackToMenuButton.font

        backToMenuButton.addTarget(self, action: #selector(didTapBackToMenuButton), for: .touchUpInside)
    }

    private func setupSubviews() {
        setupTextContainerView()
        setupTextView()
        setupBackToMenuButton()
    }

    @objc
    private func didTapBackToMenuButton() {
        output?.didTapBackToMenuButton()
    }
}

extension TutorialViewController: TutorialViewInput {

}

extension TutorialViewController {
    private struct Constants {
        static let backgroundColor: UIColor = UIColor(red: 94 / 255,
                                                      green: 92 / 255,
                                                      blue: 230 / 255,
                                                      alpha: 1)

        struct TextContainerView {
            static let backgroundColor: UIColor = UIColor(red: 1,
                                                          green: 211 / 255,
                                                          blue: 180 / 255,
                                                          alpha: 1)

            static let cornerRadius: CGFloat = 40
        }

        struct TextView {
            static let font: UIFont? = UIFont(name: "DMSans-Regular", size: 25)

            static let text: String = """
            1) Создание проекта

            Для создания нового проекта нажмите кнопку Create Project. Далее вам нужно выбрать уникальное имя для своего творения. Также не забудьте указать имя автора.

            2) Работа с редактором

            Примитивы.
            Ваше творение может состоять из примитивов, которые заложены в программу изначально, например - кубы, сферы, конусы, ограниченные плоскости и т.д. Чтобы добавить примитив к проекту - просто в редакторе нажмите на кнопку +Primitive. Вам откроется список доступных примитивов, выберите нужный.

            Сниппеты.
            Сниппеты - это примитивы, которые пользователь может задать сам. Вам не всегда может хватать базового функционала - это тот случай, когда его нужно расширить. Для добавления нажмите на кнопку +Snippet.

            Для изменения объектов(примитивов или сниппетов) просто нажмите на объект в редакторе, вам будут показаны параметры, которые можно будет изменить. Например, -  вы можете менять положения вершин объектов.

            3) Создание сниппетов
            Для создание сниппета в стартовом меню перейдите по кнопке Create Snippet. Откроется специальный редактор.
            Ваш сниппет может быть создан произвольным набором ограниченных плоскостей, не обязательно соединенных между собой.
            """
        }

        struct BackToMenuButton {
            static let backgroundColor: UIColor = UIColor(red: 1,
                                                          green: 211 / 255,
                                                          blue: 180 / 255,
                                                          alpha: 1)

            static let font: UIFont? = UIFont(name: "DMSans-Regular", size: 25)
            static let cornerRadius: CGFloat = 15
            static let title: String = "To menu"
        }
    }
}
