import UIKit

class TutorialRouter {
    weak var viewController: UIViewController?
}

extension TutorialRouter: TutorialRouterInput {
    func showMainScreen() {
        viewController?.dismiss(animated: true, completion: nil)
    }
}
