import UIKit

class TutorialPresenter {
    weak var view: TutorialViewInput?
    var router: TutorialRouterInput?

    init() {

    }
}

extension TutorialPresenter: TutorialViewOutput {
    func didTapBackToMenuButton() {
        router?.showMainScreen()
    }
}
