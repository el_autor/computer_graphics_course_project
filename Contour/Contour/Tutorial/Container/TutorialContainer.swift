import UIKit

class TutorialContainer {
    let viewController: UIViewController

    class func assemble() -> TutorialContainer {
        let storyboard = UIStoryboard(name: Storyboard.tutorial.name, bundle: nil)

        guard let viewController = storyboard.instantiateViewController(withIdentifier: Storyboard.tutorial.name) as? TutorialViewController else {
            fatalError("Tutorial Container: view controller must be type TutorialViewController")
        }

        let presenter = TutorialPresenter()
        let router = TutorialRouter()

        viewController.output = presenter

        presenter.view = viewController
        presenter.router = router

        router.viewController = viewController

        return TutorialContainer(viewController: viewController)
    }

    private init(viewController: TutorialViewController) {
        self.viewController = viewController
    }
}
