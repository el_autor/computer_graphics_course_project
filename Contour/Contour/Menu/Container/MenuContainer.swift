import UIKit

class MenuContainer {
    let viewController: MenuViewController

    class func assemble() -> MenuContainer {
        let storyboard = UIStoryboard(name: Storyboard.menu.name, bundle: nil)

        guard let viewController = storyboard.instantiateViewController(withIdentifier: Storyboard.menu.name) as? MenuViewController else {
                fatalError("MenuContainer: viewController must be type MenuViewController")
        }

        let presenter = MenuPresenter()
        let router = MenuRouter()

        viewController.output = presenter

        presenter.view = viewController
        presenter.router = router

        router.viewController = viewController

        return MenuContainer(view: viewController)
    }

    private init(view: MenuViewController) {
        self.viewController = view
    }
}
