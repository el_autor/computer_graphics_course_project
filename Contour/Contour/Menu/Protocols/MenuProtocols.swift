protocol MenuViewOutput: AnyObject {

    func didTapCreateProjectButton()

    func didTapOpenExistingProjectButton()

    func didTapCreateSnippetButton()

    func didTapViewSnippetsButton()

    func didTapTutorialButton()
}

protocol MenuViewInput: AnyObject {

}

protocol MenuRouterInput: AnyObject {

    func showTutorialScreen()

    func showCreateProjectScreen()

    func showCreateSnippetScreen()

    func showExistingProjectsScreen()

    func showExistingSnippetsScreen()
}
