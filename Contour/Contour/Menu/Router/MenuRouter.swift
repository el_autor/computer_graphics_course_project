import UIKit

class MenuRouter {
    weak var viewController: UIViewController?
}

extension MenuRouter: MenuRouterInput {
    func showTutorialScreen() {
        let container = TutorialContainer.assemble()

        container.viewController.modalPresentationStyle = .fullScreen

        viewController?.present(container.viewController,
                                animated: true,
                                completion: nil)
    }

    func showCreateProjectScreen() {
        let container = CreateProjectContainer.assemble()

        container.viewController.modalPresentationStyle = .fullScreen

        viewController?.present(container.viewController,
                                animated: true,
                                completion: nil)
    }

    func showCreateSnippetScreen() {
        let container = CreateSnippetContainer.assemble(with: CreateSnippetContext())

        container.viewController.modalPresentationStyle = .fullScreen

        viewController?.present(container.viewController,
                                animated: true,
                                completion: nil)
    }

    func showExistingProjectsScreen() {
        let container = OpenProjectContainer.assemble(context: OpenProjectContext())

        container.viewController.modalPresentationStyle = .fullScreen

        viewController?.present(container.viewController,
                                animated: true,
                                completion: nil)
    }

    func showExistingSnippetsScreen() {
        let container = OpenSnippetContainer.assemble(with: OpenSnippetContext())

        container.viewController.modalPresentationStyle = .fullScreen

        viewController?.present(container.viewController,
                                animated: true,
                                completion: nil)
    }
}
