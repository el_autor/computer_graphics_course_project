import UIKit

class MenuViewController: UIViewController {

    var output: MenuViewOutput?

    @IBOutlet private weak var createProjectButton: UIButton!

    @IBOutlet private weak var openExistingProjectButton: UIButton!

    @IBOutlet private weak var createSnippetButton: UIButton!

    @IBOutlet private weak var viewSnippetsButton: UIButton!

    @IBOutlet private weak var tutorialButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupSubviews()
    }

    private func setupView() {
        view.backgroundColor = Contstants.backgroundColor
    }

    private func setupCreateProjectButton() {
        createProjectButton.backgroundColor = Contstants.CreateProjectButton.backgroundColor

        createProjectButton.setTitle(Contstants.CreateProjectButton.title, for: .normal)
        createProjectButton.setTitleColor(.black, for: .normal)
        createProjectButton.titleLabel?.font = Contstants.font
        createProjectButton.titleLabel?.lineBreakMode = .byWordWrapping
        createProjectButton.titleLabel?.textAlignment = .center

        createProjectButton.layer.cornerRadius = Contstants.cornerRadius

        createProjectButton.addTarget(self, action: #selector(didTapCreateProjectButton), for: .touchUpInside)
    }

    private func setupOpenExistingProjectButton() {
        openExistingProjectButton.backgroundColor = Contstants.OpenExistingProjectButton.backgroundColor

        openExistingProjectButton.setTitle(Contstants.OpenExistingProjectButton.title, for: .normal)
        openExistingProjectButton.setTitleColor(.black, for: .normal)
        openExistingProjectButton.titleLabel?.font = Contstants.font
        openExistingProjectButton.titleLabel?.lineBreakMode = .byWordWrapping
        openExistingProjectButton.titleLabel?.textAlignment = .center

        openExistingProjectButton.layer.cornerRadius = Contstants.cornerRadius

        openExistingProjectButton.addTarget(self, action: #selector(didTapOpenExistingProjectButton), for: .touchUpInside)
    }

    private func setupCreateSnippetButton() {
        createSnippetButton.backgroundColor = Contstants.CreateSnippetButton.backgroundColor

        createSnippetButton.setTitle(Contstants.CreateSnippetButton.title, for: .normal)
        createSnippetButton.setTitleColor(.black, for: .normal)
        createSnippetButton.titleLabel?.font = Contstants.font
        createSnippetButton.titleLabel?.lineBreakMode = .byWordWrapping
        createSnippetButton.titleLabel?.textAlignment = .center

        createSnippetButton.layer.cornerRadius = Contstants.cornerRadius

        createSnippetButton.addTarget(self, action: #selector(didTapCreateSnippetButton), for: .touchUpInside)
    }

    private func setupViewSnippetButton() {
        viewSnippetsButton.backgroundColor = Contstants.ViewSnippetsButton.backgroundColor

        viewSnippetsButton.setTitle(Contstants.ViewSnippetsButton.title, for: .normal)
        viewSnippetsButton.setTitleColor(.black, for: .normal)
        viewSnippetsButton.titleLabel?.font = Contstants.font
        viewSnippetsButton.titleLabel?.lineBreakMode = .byWordWrapping
        viewSnippetsButton.titleLabel?.textAlignment = .center

        viewSnippetsButton.layer.cornerRadius = Contstants.cornerRadius

        viewSnippetsButton.addTarget(self, action: #selector(didTapViewSnippetsButton), for: .touchUpInside)
    }

    private func setupTutorialButton() {
        tutorialButton.backgroundColor = Contstants.TutorialButton.backgroundColor

        tutorialButton.setTitle(Contstants.TutorialButton.title, for: .normal)
        tutorialButton.setTitleColor(.black, for: .normal)
        tutorialButton.titleLabel?.font = Contstants.font
        tutorialButton.titleLabel?.lineBreakMode = .byWordWrapping
        tutorialButton.titleLabel?.textAlignment = .center

        tutorialButton.layer.cornerRadius = Contstants.cornerRadius

        tutorialButton.addTarget(self, action: #selector(didTapTutorialButton), for: .touchUpInside)
    }

    private func setupSubviews() {
        setupCreateProjectButton()
        setupOpenExistingProjectButton()
        setupCreateSnippetButton()
        setupViewSnippetButton()
        setupTutorialButton()
    }

    @objc
    private func didTapCreateProjectButton() {
        output?.didTapCreateProjectButton()
    }

    @objc
    private func didTapOpenExistingProjectButton() {
        output?.didTapOpenExistingProjectButton()
    }

    @objc
    private func didTapCreateSnippetButton() {
        output?.didTapCreateSnippetButton()
    }

    @objc
    private func didTapViewSnippetsButton() {
        output?.didTapViewSnippetsButton()
    }

    @objc
    private func didTapTutorialButton() {
        output?.didTapTutorialButton()
    }
}

extension MenuViewController: MenuViewInput {

}

extension MenuViewController {
    private struct Contstants {
        static let backgroundColor: UIColor = UIColor(red: 94 / 255,
                                                      green: 92 / 255,
                                                      blue: 230 / 255,
                                                      alpha: 1)

        struct CreateProjectButton {
            static let backgroundColor: UIColor = UIColor(red: 255 / 255,
                                                          green: 214 / 255,
                                                          blue: 10 / 255,
                                                          alpha: 1)
            static let title: String = "Create project"
        }

        struct OpenExistingProjectButton {
            static let backgroundColor: UIColor = UIColor(red: 48 / 255,
                                                          green: 209 / 255,
                                                          blue: 88 / 255,
                                                          alpha: 1)

            static let title: String = "Open existing"
        }

        struct CreateSnippetButton {
            static let backgroundColor: UIColor = UIColor(red: 255 / 255,
                                                          green: 55 / 255,
                                                          blue: 95 / 255,
                                                          alpha: 1)

            static let title: String = "Create snippet"
        }

        struct ViewSnippetsButton {
            static let backgroundColor: UIColor = UIColor(red: 10 / 255,
                                                          green: 132 / 255,
                                                          blue: 255 / 255,
                                                          alpha: 1)

            static let title: String = "View Snippets"
        }

        struct TutorialButton {
            static let backgroundColor: UIColor = UIColor(red: 255 / 255,
                                                          green: 211 / 255,
                                                          blue: 180 / 255,
                                                          alpha: 1)

            static let title: String = "Tutorial"
        }

        static let cornerRadius: CGFloat = 15
        static let font: UIFont? = UIFont(name: "Chalkduster", size: 25)
    }
}
