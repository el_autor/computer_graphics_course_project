import Foundation

class MenuPresenter {
    weak var view: MenuViewInput?
    var router: MenuRouterInput?

    init() {

    }
}

extension MenuPresenter: MenuViewOutput {
    func didTapCreateProjectButton() {
        router?.showCreateProjectScreen()
    }

    func didTapOpenExistingProjectButton() {
        router?.showExistingProjectsScreen()
    }

    func didTapCreateSnippetButton() {
        router?.showCreateSnippetScreen()
    }

    func didTapViewSnippetsButton() {
        router?.showExistingSnippetsScreen()
    }

    func didTapTutorialButton() {
        router?.showTutorialScreen()
    }
}
