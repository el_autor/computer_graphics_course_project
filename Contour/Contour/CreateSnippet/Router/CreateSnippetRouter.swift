import UIKit

final class CreateSnippetRouter {
    weak var viewController: UIViewController?
}

extension CreateSnippetRouter: CreateSnippetRouterInput {
    func goToMenu() {
        viewController?.dismiss(animated: true, completion: nil)
    }

    func showCanvas(name: String, authorName: String) {
        let container = SnippetCanvasContainer.assemble(with: SnippetCanvasContext(name: name,
                                                                                   authorName: authorName))

        container.viewController.modalPresentationStyle = .fullScreen

        viewController?.present(container.viewController, animated: true, completion: nil)
    }
}
