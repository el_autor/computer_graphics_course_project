import Foundation

protocol CreateSnippetViewInput: AnyObject {
    func showAlert(with: String, message: String)

    func getSnippetName() -> String?

    func getAuthorName() -> String?
}

protocol CreateSnippetViewOutput: AnyObject {
    func didTapBackToMenuButton()

    func didTapCreateButton()
}

protocol CreateSnippetInteractorInput: AnyObject {
    func createNewSnippet(name: String, authorName: String)
}

protocol CreateSnippetInteractorOutput: AnyObject {
    func snippetSuccesfullyCreated(name: String, authorName: String)

    func showAlert(with: String, message: String)
}

protocol CreateSnippetRouterInput: AnyObject {
    func goToMenu()

    func showCanvas(name: String, authorName: String)
}
