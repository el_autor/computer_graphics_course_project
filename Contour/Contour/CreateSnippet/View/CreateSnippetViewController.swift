import UIKit
import PinLayout

final class CreateSnippetViewController: UIViewController {
	var output: CreateSnippetViewOutput?

    // MARK: UI elements

    private weak var titleLabel: UILabel!

    private weak var snippetNameTextField: UITextField!

    private weak var authorNameTextField: UITextField!

    private weak var createButton: UIButton!

    private weak var backToMenuButton: UIButton!

	override func viewDidLoad() {
		super.viewDidLoad()

        setupView()
        setupSubviews()
	}

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        layoutTitleLabel()
        layoutSnippetNameTextField()
        layoutAuthorNameTextField()
        layoutCreateButton()
        layoutBackToMenuButton()
    }

    private func setupView() {
        view.backgroundColor = Contstants.backgroundColor
    }

    private func setupSubviews() {
        setupTitleLabel()
        setupSnippetNameTextField()
        setupAuthorNameTextField()
        setupCreateButton()
        setupBackToMenuButton()
    }

    private func setupTitleLabel() {
        let label = UILabel()

        titleLabel = label
        view.addSubview(titleLabel)

        titleLabel.textAlignment = .center
        titleLabel.font = UIFont(name: "Chalkduster", size: 35)
        titleLabel.text = "Create new snippet"
    }

    private func setupSnippetNameTextField() {
        let textField = UITextField()

        snippetNameTextField = textField
        view.addSubview(snippetNameTextField)

        snippetNameTextField.attributedPlaceholder = NSAttributedString(string: "Name",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])

        snippetNameTextField.backgroundColor = .white
        snippetNameTextField.clipsToBounds = true
        snippetNameTextField.textColor = .black
        snippetNameTextField.layer.cornerRadius = 15
        snippetNameTextField.layer.borderWidth = 1
        snippetNameTextField.layer.borderColor = UIColor.white.cgColor
        snippetNameTextField.font = UIFont(name: "DMSans-Regular", size: 15)

        snippetNameTextField.leftView = UIView(frame: CGRect(x: .zero,
                                                      y: .zero,
                                                      width: 20,
                                                      height: .zero))
        snippetNameTextField.leftViewMode = .always
        snippetNameTextField.rightView = UIView(frame: CGRect(x: .zero,
                                                       y: .zero,
                                                       width: 10,
                                                       height: .zero))
        snippetNameTextField.rightViewMode = .unlessEditing

        snippetNameTextField.clearButtonMode = .whileEditing
        snippetNameTextField.autocorrectionType = .no
    }

    private func setupAuthorNameTextField() {
        let textField = UITextField()

        authorNameTextField = textField
        view.addSubview(authorNameTextField)

        authorNameTextField.attributedPlaceholder = NSAttributedString(string: "Author",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])

        authorNameTextField.backgroundColor = .white
        authorNameTextField.clipsToBounds = true
        authorNameTextField.textColor = .black
        authorNameTextField.layer.cornerRadius = 15
        authorNameTextField.layer.borderWidth = 1
        authorNameTextField.layer.borderColor = UIColor.white.cgColor
        authorNameTextField.font = UIFont(name: "DMSans-Regular", size: 15)

        authorNameTextField.leftView = UIView(frame: CGRect(x: .zero,
                                                      y: .zero,
                                                      width: 20,
                                                      height: .zero))
        authorNameTextField.leftViewMode = .always
        authorNameTextField.rightView = UIView(frame: CGRect(x: .zero,
                                                       y: .zero,
                                                       width: 10,
                                                       height: .zero))
        authorNameTextField.rightViewMode = .unlessEditing

        authorNameTextField.clearButtonMode = .whileEditing
        authorNameTextField.autocorrectionType = .no
    }

    private func setupCreateButton() {
        let button = UIButton()

        createButton = button
        view.addSubview(createButton)

        createButton.setTitle("Create", for: .normal)
        createButton.setTitleColor(.black, for: .normal)
        createButton.titleLabel?.font = UIFont(name: "DMSans-Regular", size: 25)
        createButton.backgroundColor = UIColor(red: 255 / 255,
                                                   green: 55 / 255,
                                                   blue: 95 / 255,
                                                   alpha: 1)
        createButton.layer.cornerRadius = 15

        createButton.addTarget(self, action: #selector(didTapCreateButton), for: .touchUpInside)
    }

    private func setupBackToMenuButton() {
        let button = UIButton()

        backToMenuButton = button
        view.addSubview(backToMenuButton)

        backToMenuButton.setTitle("To menu", for: .normal)
        backToMenuButton.setTitleColor(.black, for: .normal)
        backToMenuButton.titleLabel?.font = UIFont(name: "DMSans-Regular", size: 25)
        backToMenuButton.backgroundColor = UIColor(red: 255 / 255,
                                                   green: 55 / 255,
                                                   blue: 95 / 255,
                                                   alpha: 1)
        backToMenuButton.layer.cornerRadius = 15

        backToMenuButton.addTarget(self, action: #selector(didTapBackToMenuButton), for: .touchUpInside)
    }

    private func layoutTitleLabel() {
        titleLabel.pin
            .top(10%)
            .hCenter()
            .width(100%)
            .height(4%)
    }

    private func layoutSnippetNameTextField() {
        snippetNameTextField.pin
            .below(of: titleLabel).marginTop(9%)
            .hCenter()
            .width(30%)
            .height(4.8%)
    }

    private func layoutAuthorNameTextField() {
        authorNameTextField.pin
            .below(of: snippetNameTextField).marginTop(5%)
            .hCenter()
            .width(30%)
            .height(4.8%)
    }

    private func layoutCreateButton() {
        createButton.pin
            .below(of: authorNameTextField).marginTop(5%)
            .hCenter()
            .width(30%)
            .height(4.8%)
    }

    private func layoutBackToMenuButton() {
        backToMenuButton.pin
            .right(5%)
            .bottom(5%)
            .height(4.8%)
            .width(15%)
    }

    @objc
    private func didTapBackToMenuButton() {
        output?.didTapBackToMenuButton()
    }

    @objc
    private func didTapCreateButton() {
        output?.didTapCreateButton()
    }
}

extension CreateSnippetViewController: CreateSnippetViewInput {
    func showAlert(with: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "ОК", style: .default, handler: nil))

        present(alert, animated: true)
    }

    func getSnippetName() -> String? {
        return snippetNameTextField.text
    }

    func getAuthorName() -> String? {
        return authorNameTextField.text
    }
}

extension CreateSnippetViewController {
    private struct Contstants {
        static let backgroundColor: UIColor = UIColor(red: 94 / 255,
                                                      green: 92 / 255,
                                                      blue: 230 / 255,
                                                      alpha: 1)
    }
}
