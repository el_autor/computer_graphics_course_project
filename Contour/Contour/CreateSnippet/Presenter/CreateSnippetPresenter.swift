import Foundation

final class CreateSnippetPresenter {
	weak var view: CreateSnippetViewInput?

	private let router: CreateSnippetRouterInput
	private let interactor: CreateSnippetInteractorInput

    init(router: CreateSnippetRouterInput, interactor: CreateSnippetInteractorInput) {
        self.router = router
        self.interactor = interactor
    }
}

extension CreateSnippetPresenter: CreateSnippetViewOutput {
    func didTapBackToMenuButton() {
        router.goToMenu()
    }

    func didTapCreateButton() {
        guard let name = view?.getSnippetName(),
              !name.isEmpty else {
            view?.showAlert(with: "Oops!", message: "Snippet name not entered!")
            return
        }

        guard let authorName = view?.getAuthorName(),
              !authorName.isEmpty else {
            view?.showAlert(with: "Oops!", message: "Author name not entered!")
            return
        }

        interactor.createNewSnippet(name: name, authorName: authorName)
    }
}

extension CreateSnippetPresenter: CreateSnippetInteractorOutput {
    func snippetSuccesfullyCreated(name: String, authorName: String) {
        router.showCanvas(name: name, authorName: authorName)
    }

    func showAlert(with: String, message: String) {
        view?.showAlert(with: with, message: message)
    }
}
