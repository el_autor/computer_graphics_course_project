import UIKit

final class CreateSnippetContainer {
	let viewController: UIViewController
	private(set) weak var router: CreateSnippetRouterInput!

	class func assemble(with context: CreateSnippetContext) -> CreateSnippetContainer {
        let router = CreateSnippetRouter()
        let interactor = CreateSnippetInteractor()
        let presenter = CreateSnippetPresenter(router: router, interactor: interactor)
	let viewController = CreateSnippetViewController()

        viewController.output = presenter
	presenter.view = viewController
	interactor.output = presenter
	router.viewController = viewController

        return CreateSnippetContainer(view: viewController, router: router)
	}

    private init(view: UIViewController, router: CreateSnippetRouterInput) {
		self.viewController = view
		self.router = router
	}
}

struct CreateSnippetContext {
}
