import Foundation

final class CreateSnippetInteractor {
	weak var output: CreateSnippetInteractorOutput?
}

extension CreateSnippetInteractor: CreateSnippetInteractorInput {
    func createNewSnippet(name: String, authorName: String) {
        guard DataService.shared.snippetIsUnique(name: name, authorName: authorName) else {
            output?.showAlert(with: "Oops!", message: "Such snippet already exists")
            return
        }

        DataService.shared.createSnippet(name: name, authorName: authorName)

        output?.snippetSuccesfullyCreated(name: name, authorName: authorName)
    }
}
