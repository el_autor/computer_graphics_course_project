import Foundation

protocol AddSnippetViewInput: AnyObject {
    func updateTable()
}

protocol AddSnippetViewOutput: AnyObject {
    func didLoadView()

    func getTableSize() -> Int

    func object(indexPath: IndexPath) -> SnippetData?

    func didSelectCell(indexPath: IndexPath)
}

protocol AddSnippetInteractorInput: AnyObject {
    func getAllSnippets() -> [SnippetData]

    func appendSnippetToProject(snippetName: String, authorName: String)
}

protocol AddSnippetInteractorOutput: AnyObject {
}

protocol AddSnippetRouterInput: AnyObject {
    func goToCanvas()
}
