import Foundation

struct SnippetData {
    var name: String

    var authorName: String
}

final class AddSnippetPresenter {
	weak var view: AddSnippetViewInput?

	private let router: AddSnippetRouterInput

	private let interactor: AddSnippetInteractorInput

    private var model: [SnippetData]?

    init(router: AddSnippetRouterInput, interactor: AddSnippetInteractorInput) {
        self.router = router
        self.interactor = interactor
    }
}

extension AddSnippetPresenter: AddSnippetViewOutput {
    func didLoadView() {
        model = interactor.getAllSnippets()
        view?.updateTable()
    }

    func getTableSize() -> Int {
        return model?.count ?? 0
    }

    func object(indexPath: IndexPath) -> SnippetData? {
        return model?[indexPath.row]
    }

    func didSelectCell(indexPath: IndexPath) {
        guard let viewModel = model?[indexPath.row] else {
            return
        }

        interactor.appendSnippetToProject(snippetName: viewModel.name,
                                          authorName: viewModel.authorName)

        router.goToCanvas()
    }
}

extension AddSnippetPresenter: AddSnippetInteractorOutput {
}
