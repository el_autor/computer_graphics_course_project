import Foundation

final class AddSnippetInteractor {
	weak var output: AddSnippetInteractorOutput?

    private var project: Project?

    init(projectName: String, authorName: String) {
        project = DataService.shared.getProject(name: projectName,
                                                authorName: authorName)
    }
}

extension AddSnippetInteractor: AddSnippetInteractorInput {
    func getAllSnippets() -> [SnippetData] {
        return DataService.shared.getAllSnippets().map { snippet in
            return SnippetData(name: snippet.name ?? String(),
                               authorName: snippet.authorName ?? String())
        }
    }

    func appendSnippetToProject(snippetName: String, authorName: String) {
        guard let project = project else {
            return
        }

        DataService.shared.appendSnippetToProject(project: project,
                                                  snippetName: snippetName,
                                                  authorName: authorName)
    }
}
