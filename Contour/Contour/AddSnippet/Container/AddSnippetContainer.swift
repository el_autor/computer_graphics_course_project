import UIKit

final class AddSnippetContainer {
	let viewController: UIViewController
	private(set) weak var router: AddSnippetRouterInput!

	class func assemble(with context: AddSnippetContext) -> AddSnippetContainer {
        let router = AddSnippetRouter()
        let interactor = AddSnippetInteractor(projectName: context.projectName, authorName: context.authorName)
        let presenter = AddSnippetPresenter(router: router, interactor: interactor)
	    let viewController = AddSnippetViewController()

        viewController.output = presenter
        presenter.view = viewController
        interactor.output = presenter
        router.viewController = viewController

        return AddSnippetContainer(view: viewController, router: router)
	}

    private init(view: UIViewController, router: AddSnippetRouterInput) {
		self.viewController = view
		self.router = router
	}
}

struct AddSnippetContext {
    var projectName: String

    var authorName: String
}
