import UIKit
import PinLayout

final class AddSnippetViewController: UIViewController {
	var output: AddSnippetViewOutput?

    private weak var titleLabel: UILabel!

    private weak var snippetsTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupSubviews()

        output?.didLoadView()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        layoutTitleLabel()
        layoutObjectsTableView()
    }

    private func layoutObjectsTableView() {
        snippetsTableView.pin
            .hCenter()
            .vCenter()
            .width(75%)
            .height(80%)
    }

    private func layoutTitleLabel() {
        titleLabel.pin
            .top(3%)
            .hCenter()
            .height(4%)
            .width(50%)
    }

    private func setupView() {
        view.backgroundColor = Constants.backgroundColor
    }

    private func setupSubviews() {
        setupTitleView()
        setupSnippetsTableView()
    }

    private func setupSnippetsTableView() {
        let tableView = UITableView()

        snippetsTableView = tableView
        view.addSubview(snippetsTableView)

        snippetsTableView.delegate = self
        snippetsTableView.dataSource = self

        snippetsTableView.register(AddSnippetCell.self,
                                   forCellReuseIdentifier: Constants.SnippetListTableView.cellReuseIdentifier)

        snippetsTableView.tableFooterView = UIView(frame: .zero)
        snippetsTableView.separatorInset = UIEdgeInsets(top: .zero, left: .zero, bottom: .zero, right: .zero)
        snippetsTableView.layer.cornerRadius = Constants.SnippetListTableView.cornerRadius
        snippetsTableView.backgroundColor = Constants.SnippetListTableView.backgroundColor
    }

    private func setupTitleView() {
        let label = UILabel()

        titleLabel = label
        view.addSubview(titleLabel)

        titleLabel.textAlignment = .center
        titleLabel.text = Constants.TitleLabel.title
        titleLabel.font = Constants.TitleLabel.font
        titleLabel.textColor = Constants.TitleLabel.fontColor
    }
}

extension AddSnippetViewController: AddSnippetViewInput {
    func updateTable() {
        snippetsTableView.reloadData()
    }
}

extension AddSnippetViewController: UITableViewDelegate {

}

extension AddSnippetViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output?.getTableSize() ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.SnippetListTableView.cellReuseIdentifier,
                                                       for: indexPath) as? AddSnippetCell else {
            return UITableViewCell()
        }

        guard let viewModel = output?.object(indexPath: indexPath) else {
            return UITableViewCell()
        }

        cell.configure(viewModel: viewModel)
        cell.selectionStyle = .none

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        output?.didSelectCell(indexPath: indexPath)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.SnippetListTableView.cellHeight
    }
}

extension AddSnippetViewController {
    private struct Constants {

        static let backgroundColor: UIColor = UIColor(red: 94 / 255,
                                                      green: 92 / 255,
                                                      blue: 230 / 255,
                                                      alpha: 1)

        struct SnippetListTableView {
            static let backgroundColor: UIColor = UIColor(red: 1,
                                                   green: 211 / 255,
                                                   blue: 180 / 255,
                                                   alpha: 1)

            static let cellReuseIdentifier: String = "SnippetCell"
            static let cornerRadius: CGFloat = 20

            static let cellHeight: CGFloat = 80
        }

        struct TitleLabel {
            static let title: String = "Choose snippet"
            static let font: UIFont? = UIFont(name: "DMSans-Bold", size: 25)
            static let fontColor: UIColor = .white
        }
    }
}
