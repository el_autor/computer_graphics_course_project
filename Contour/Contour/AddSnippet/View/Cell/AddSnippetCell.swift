import UIKit
import PinLayout

final class AddSnippetCell: UITableViewCell {

    private weak var snippetNameLabel: UILabel!

    private weak var authorNameLabel: UILabel!

    required init?(coder: NSCoder) {
        return nil
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        setupView()
        setupSubviews()
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        layoutSnippetNameLabel()
        layoutAuthorNameLabel()
    }

    private func setupView() {
        backgroundColor = Constants.backgroundColor
    }

    private func setupSubviews() {
        setupSnippetNameLabel()
        setupAuthorNameLabel()
    }

    private func setupSnippetNameLabel() {
        let label = UILabel()

        snippetNameLabel = label
        addSubview(snippetNameLabel)

        snippetNameLabel.textAlignment = .left
        snippetNameLabel.font = UIFont(name: "DMSans-Regular", size: 25)
    }

    private func setupAuthorNameLabel() {
        let label = UILabel()

        authorNameLabel = label
        addSubview(authorNameLabel)

        authorNameLabel.textAlignment = .right
        authorNameLabel.font = UIFont(name: "DMSans-Regular", size: 25)
    }

    private func layoutSnippetNameLabel() {
        snippetNameLabel.pin
            .left(5%)
            .width(45%)
            .height(100%)
    }

    private func layoutAuthorNameLabel() {
        authorNameLabel.pin
            .width(45%)
            .right(5%)
            .height(100%)
    }
}

extension AddSnippetCell {
    func configure(viewModel: SnippetData) {
        snippetNameLabel.text = viewModel.name
        authorNameLabel.text = "Author: " + viewModel.authorName
    }
}

extension AddSnippetCell {
    private struct Constants {
        static let backgroundColor: UIColor = UIColor(red: 1,
                                               green: 211 / 255,
                                               blue: 180 / 255,
                                               alpha: 1)
    }
}
