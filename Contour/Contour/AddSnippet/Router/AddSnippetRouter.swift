import UIKit

final class AddSnippetRouter {
    weak var viewController: UIViewController?
}

extension AddSnippetRouter: AddSnippetRouterInput {
    func goToCanvas() {
        guard let canvas = viewController?.navigationController?.presentingViewController as? CanvasViewController else {
            return
        }

        canvas.updateCanvas()
        viewController?.navigationController?.dismiss(animated: true, completion: nil)
    }
}
