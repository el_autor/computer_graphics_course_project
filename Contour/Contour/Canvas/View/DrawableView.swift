import UIKit
import CoreGraphics
import MetalKit

final class DrawableView: MTKView {

    enum CanvasType {
        case project
        case snippet
    }

    struct Point {
        var color: UIColor

        var primitiveID: Int

        var vergeID: Int

        var intensity: Float

        var z: Float
    }

    struct Vector {
        var x: Float

        var y: Float

        var z: Float
    }

    private var vertices: [MetalVertex] = []

    private var projectName: String!

    private var authorName: String!

    private var commandQueue: MTLCommandQueue!

    private var pipelineState: MTLRenderPipelineState!

    private var vertexBuffer: MTLBuffer!

    private var type: CanvasType!

    required init(coder: NSCoder) {
        super.init(coder: coder)

        device = MTLCreateSystemDefaultDevice()
        clearColor = MTLClearColor(red: 1,
                                   green: 1,
                                   blue: 1,
                                   alpha: 1)
        delegate = self
        commandQueue = device?.makeCommandQueue()
        enableSetNeedsDisplay = true

        buildPipelineState()
    }

    override init(frame frameRect: CGRect, device: MTLDevice?) {
        super.init(frame: frameRect, device: device)

        self.device = MTLCreateSystemDefaultDevice()
        clearColor = MTLClearColor(red: 1,
                                   green: 1,
                                   blue: 1,
                                   alpha: 1)
        delegate = self
        commandQueue = self.device?.makeCommandQueue()
        enableSetNeedsDisplay = true

        buildPipelineState()
    }

    func setProjectName(name: String) {
        projectName = name
    }

    func setAuthorName(name: String) {
        authorName = name
    }

    func setType(type: CanvasType) {
        self.type = type
    }

    private func buildModel() {
        vertexBuffer = device?.makeBuffer(bytes: vertices,
                                          length: vertices.count * MemoryLayout<MetalVertex>.stride,
                                          options: [])
    }

    private func buildPipelineState() {
        let library = device?.makeDefaultLibrary()
        let vertexFunction = library?.makeFunction(name: "vertex_shader")
        let fragmentFunction = library?.makeFunction(name: "fragment_shader")

        let pipelineDescriptor = MTLRenderPipelineDescriptor()
        pipelineDescriptor.vertexFunction = vertexFunction
        pipelineDescriptor.fragmentFunction = fragmentFunction
        pipelineDescriptor.colorAttachments[0].pixelFormat = .bgra8Unorm

        let vertexDescriptor = MTLVertexDescriptor()

        vertexDescriptor.attributes[0].format = .float3
        vertexDescriptor.attributes[0].offset = 0
        vertexDescriptor.attributes[0].bufferIndex = 0

        vertexDescriptor.attributes[1].format = .float4
        vertexDescriptor.attributes[1].offset = MemoryLayout<SIMD3<Float>>.stride
        vertexDescriptor.attributes[1].bufferIndex = 0

        vertexDescriptor.layouts[0].stride = MemoryLayout<MetalVertex>.stride

        pipelineDescriptor.vertexDescriptor = vertexDescriptor

        do {
            pipelineState = try device?.makeRenderPipelineState(descriptor: pipelineDescriptor)
        } catch let error as NSError {
            print(error)
        }
    }

    private func createZBuffer(primitives: [Primitive], size: CGSize) -> [[Point]] {
        var buffer: [[Point]] = [[Point]](repeating: [Point](repeating: Point(color: .white,
                                                                              primitiveID: .zero,
                                                                              vergeID: .zero,
                                                                              intensity: 1,
                                                                              z: Float(Int.min)),
                                                                       count: Int(size.width)), count: Int(size.height))

        for primitive in primitives {
            for verge in primitive.verges {
                appendVergeToBuffer(primitiveColor: primitive.color,
                                    primitiveID: primitive.id,
                                    verge: verge,
                                    buffer: &buffer)
            }
        }

        return buffer
    }

    private func vectorProduct(vectorA: Vector, vectorB: Vector) -> Vector {
        return Vector(x: vectorA.y * vectorB.z - vectorA.z * vectorB.y,
                      y: -(vectorA.x * vectorB.z - vectorA.z * vectorB.x),
                      z: vectorA.x * vectorB.y - vectorA.y * vectorB.x)
    }

    private func vectorAngle(vectorA: Vector, vectorB: Vector) -> Float {
        return vectorProduct(vectorA: vectorA, vectorB: vectorB) /
            (vectorModule(vector: vectorA) * vectorModule(vector: vectorB))
    }

    private func vectorProduct(vectorA: Vector, vectorB: Vector) -> Float {
        return vectorA.x * vectorB.x + vectorA.y * vectorB.y + vectorA.z * vectorB.z
    }

    private func vectorModule(vector: Vector) -> Float {
        return sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z)
    }
}

extension DrawableView {
    private func addIntersectionsForEdge(list: inout [CGPoint],
                                         startVertex: Vertex,
                                         endVertex: Vertex,
                                         maxY: Float,
                                         minY: Float) {
        var change: Int = .zero

        var deltaX = lroundf(endVertex.x) - lroundf(startVertex.x)
        var deltaY = lroundf(endVertex.y) - lroundf(startVertex.y)

        let signX = deltaX.signum()
        let signY = deltaY.signum()

        deltaX = abs(deltaX)
        deltaY = abs(deltaY)

        if deltaX > deltaY {
            change = .zero
        } else {
            let temp = deltaX

            deltaX = deltaY
            deltaY = temp
            change = 1
        }

        var error = 2 * deltaY - deltaX
        let deltaErrorX = 2 * deltaX
        let deltaErrorY = 2 * deltaY

        var currentX = lroundf(startVertex.x)
        var currentY = lroundf(startVertex.y)

        if lroundf(startVertex.y) == lroundf(endVertex.y) {
            return
        }

        var lastY = currentY

        if currentY == lroundf(maxY) || currentY == lroundf(minY) {
            list.append(CGPoint(x: currentX, y: currentY))
        }

        for _ in .zero...deltaX {
            if currentY != lastY {
                list.append(CGPoint(x: currentX, y: currentY))
                lastY = currentY
            }

            if error > 0 {
                if change == 1 {
                    currentX += signX
                } else {
                    currentY += signY
                }

                error -= deltaErrorX
            }

            if change == 0 {
                currentX += signX
            } else {
                currentY += signY
            }

            error += deltaErrorY
        }
    }

    private func appendVergeToBuffer(primitiveColor: RealmColor, primitiveID: Int, verge: Verge, buffer: inout [[Point]]) {
        var intersectionList: [CGPoint] = []
        var vergeVertices: [Vertex] = []

        var minY = verge.connections[0].startVertex.y
        var maxY = verge.connections[0].startVertex.y

        for i in 0..<verge.connections.count {
            if verge.connections[i].startVertex.y < minY {
                minY = verge.connections[i].startVertex.y
            }

            if verge.connections[i].startVertex.y > maxY {
                maxY = verge.connections[i].startVertex.y
            }
        }

        for connection in verge.connections {
            guard let start = connection.startVertex,
                  let end = connection.endVertex else {
                continue
            }

            addIntersectionsForEdge(list: &intersectionList,
                                    startVertex: start,
                                    endVertex: end,
                                    maxY: maxY,
                                    minY: minY)

            vergeVertices.append(start)
        }

        if lroundf(vergeVertices[0].x) == lroundf(vergeVertices[1].x) &&
            lroundf(vergeVertices[0].y) == lroundf(vergeVertices[1].y) ||
            lroundf(vergeVertices[1].x) == lroundf(vergeVertices[2].x) &&
            lroundf(vergeVertices[1].y) == lroundf(vergeVertices[2].y) ||
            lroundf(vergeVertices[2].x) == lroundf(vergeVertices[0].x) &&
            lroundf(vergeVertices[2].y) == lroundf(vergeVertices[0].y) {
            return
        }

        intersectionList.sort { (pointLeft, pointRight) in
            return pointLeft.y > pointRight.y
        }

        intersectionList.sort { (pointLeft, pointRight) in
            return pointLeft.y == pointRight.y && pointLeft.x <= pointRight.x
        }

        // считаем нормаль к грани
        let vectorAB = Vector(x: vergeVertices[1].x - vergeVertices[0].x,
                              y: vergeVertices[1].y - vergeVertices[0].y,
                              z: vergeVertices[1].z - vergeVertices[0].z)

        let vectorBC = Vector(x: vergeVertices[2].x - vergeVertices[1].x,
                              y: vergeVertices[2].y - vergeVertices[1].y,
                              z: vergeVertices[2].z - vergeVertices[1].z)

        let lightVector = Vector(x: 0, y: 0, z: -1)
        let normal: Vector = vectorProduct(vectorA: vectorAB, vectorB: vectorBC)
        let intensity: Float = abs(vectorAngle(vectorA: normal, vectorB: lightVector))

        let const1 = vergeVertices[2].z - vergeVertices[0].z
        let const2 = vergeVertices[1].z - vergeVertices[0].z
        let const3 = vergeVertices[2].y - vergeVertices[0].y
        let const4 = vergeVertices[1].y - vergeVertices[0].y
        let const5 = vergeVertices[2].x - vergeVertices[0].x
        let const6 = vergeVertices[1].x - vergeVertices[0].x
        let const7 = const4 * const1 - const3 * const2

        for i in stride(from: .zero, to: intersectionList.count, by: 2) {
            if intersectionList[i].y <= 0 || intersectionList[i].y >= bounds.height - 1 {
                continue
            }
            for j in Int(intersectionList[i].x)...Int(intersectionList[i + 1].x) {
                if j <= 0 || j >= llroundf(Float(bounds.width)) {
                    continue
                }

                var z: Float = (Float(j) - Float(vergeVertices[0].x)) * const7 -
                                const6 * (Float(intersectionList[i].y) - vergeVertices[0].y) * const1 +
                                const5 * (Float(intersectionList[i].y) - vergeVertices[0].y) * const2

                z = z / (const5 * const4 - const6 * const3) + vergeVertices[0].z

                if z > buffer[Int(intersectionList[i].y)][j].z {
                    buffer[Int(intersectionList[i].y)][j].color = UIColor(red: CGFloat(primitiveColor.r),
                                                                              green: CGFloat(primitiveColor.g),
                                                                              blue: CGFloat(primitiveColor.b),
                                                                              alpha: 1)
                    buffer[Int(intersectionList[i].y)][j].primitiveID = primitiveID
                    buffer[Int(intersectionList[i].y)][j].vergeID = verge.id
                    buffer[Int(intersectionList[i].y)][j].z = z
                    buffer[Int(intersectionList[i].y)][j].intensity = intensity
                }
            }
        }
    }
}

extension DrawableView: MTKViewDelegate {
    func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {

    }

    func draw(in view: MTKView) {
        vertices = []
        var primitives: [Primitive] = []

        switch type {
        case .project:
            primitives = DataService.shared.getAllPrimitives(projectName: projectName,
                                                             authorName: authorName)
        case .snippet:
            primitives = DataService.shared.getSnippetPrimitives(name: projectName,
                                                                 authorName: authorName)
        default:
            primitives = []
        }

        let buffer: [[Point]] = createZBuffer(primitives: primitives, size: bounds.size)

        guard !primitives.isEmpty else {
            return
        }

        let stabWidth = Int(bounds.size.width / 2)
        let stabHeight = Int(bounds.size.height / 2)

        var red: CGFloat = 0
        var green: CGFloat = 0
        var blue: CGFloat = 0
        var alpha: CGFloat = 0

        for i in .zero..<buffer.count {
            for j in .zero..<buffer[i].count where buffer[i][j].color != UIColor.white {
                buffer[i][j].color.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
                let instensity = buffer[i][j].intensity

                vertices.append(MetalVertex(position: SIMD3<Float>(Float(j - stabWidth) / Float(stabWidth),
                                                                   -Float(i - stabHeight) / Float(stabHeight), 0),
                                            color: SIMD4<Float>(Float(red) * instensity,
                                                                Float(green) * instensity,
                                                                Float(blue) * instensity,
                                                                Float(alpha))))
            }
        }

        buildModel()

        guard let drawable = view.currentDrawable,
              let descriptor = view.currentRenderPassDescriptor else {
            return
        }

        let commandBuffer = commandQueue.makeCommandBuffer()
        let commandEncoder = commandBuffer?.makeRenderCommandEncoder(descriptor: descriptor)

        commandEncoder?.setRenderPipelineState(pipelineState)
        commandEncoder?.setVertexBuffer(vertexBuffer, offset: 0, index: 0)

        commandEncoder?.drawPrimitives(type: .point, vertexStart: 0, vertexCount: vertices.count, instanceCount: 1)

        commandEncoder?.endEncoding()
        commandBuffer?.present(drawable)
        commandBuffer?.commit()
    }
}
