import UIKit

final class CanvasViewController: UIViewController {

    var output: CanvasViewOutput?

    @IBOutlet private weak var canvasView: DrawableView!

    @IBOutlet private weak var projectTitleLabel: UILabel!

    @IBOutlet private weak var toMenuButton: UIButton!

    @IBOutlet private weak var addPrimitiveButton: UIButton!

    @IBOutlet private weak var addSnippetButton: UIButton!

    @IBOutlet private weak var plusScaleButton: UIButton!

    @IBOutlet private weak var minusScaleButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupSubviews()

        output?.didLoadView()
    }

    private func setupView() {

    }

    private func setupCanvasView() {
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(canvasDidTap))

        tapRecognizer.numberOfTapsRequired = 2
        canvasView.addGestureRecognizer(tapRecognizer)
        canvasView.isUserInteractionEnabled = true
    }

    private func setupProjectTitle() {
        projectTitleLabel.textAlignment = .center
        projectTitleLabel.font = Constants.ProjectTitleLabel.font
    }

    private func setupToMenuButton() {
        toMenuButton.backgroundColor = Constants.ToMenuButton.backgroundColor
        toMenuButton.layer.cornerRadius = Constants.cornerRadius

        toMenuButton.titleLabel?.font = Constants.ToMenuButton.font
        toMenuButton.setTitleColor(Constants.ToMenuButton.fontColor, for: .normal)
        toMenuButton.setTitle(Constants.ToMenuButton.title, for: .normal)

        toMenuButton.addTarget(self, action: #selector(didTapToMenuButton), for: .touchUpInside)
    }

    private func setupPlusScaleButton() {
        plusScaleButton.backgroundColor = Constants.ScaleButton.backgroundColor

        plusScaleButton.titleLabel?.font = Constants.ScaleButton.font
        plusScaleButton.setTitleColor(Constants.ScaleButton.fontColor, for: .normal)

        plusScaleButton.layer.cornerRadius = Constants.ScaleButton.cornerRadius

        plusScaleButton.addTarget(self, action: #selector(didTapPlusScaleButton), for: .touchUpInside)
    }

    private func setupMinusScaleButton() {
        minusScaleButton.backgroundColor = Constants.ScaleButton.backgroundColor

        minusScaleButton.titleLabel?.font = Constants.ScaleButton.font
        minusScaleButton.setTitleColor(Constants.ScaleButton.fontColor, for: .normal)

        minusScaleButton.layer.cornerRadius = Constants.ScaleButton.cornerRadius

        minusScaleButton.addTarget(self, action: #selector(didTapMinusScaleButton), for: .touchUpInside)
    }

    private func setupAddPrimitiveButton() {
        addPrimitiveButton.backgroundColor = Constants.AddPrimitiveButton.backgroundColor

        addPrimitiveButton.setTitle(Constants.AddPrimitiveButton.title, for: .normal)
        addPrimitiveButton.titleLabel?.font = Constants.AddPrimitiveButton.font
        addPrimitiveButton.titleLabel?.textAlignment = .center
        addPrimitiveButton.setTitleColor(Constants.AddPrimitiveButton.fontColor, for: .normal)

        addPrimitiveButton.layer.cornerRadius = Constants.AddPrimitiveButton.cornerRadius

        addPrimitiveButton.addTarget(self, action: #selector(didTapAddPrimitiveButton), for: .touchUpInside)
    }

    private func setupAddSnippetButton() {
        addSnippetButton.backgroundColor = Constants.AddSnippetButton.backgroundColor

        addSnippetButton.setTitle(Constants.AddSnippetButton.title, for: .normal)
        addSnippetButton.titleLabel?.font = Constants.AddSnippetButton.font
        addSnippetButton.titleLabel?.textAlignment = .center
        addSnippetButton.setTitleColor(Constants.AddSnippetButton.fontColor, for: .normal)

        addSnippetButton.layer.cornerRadius = Constants.AddSnippetButton.cornerRadius

        addSnippetButton.addTarget(self, action: #selector(didTapAddSnippetButton), for: .touchUpInside)
    }

    private func setupSubviews() {
        setupCanvasView()
        setupProjectTitle()
        setupToMenuButton()
        setupAddPrimitiveButton()
        setupAddSnippetButton()
        setupPlusScaleButton()
        setupMinusScaleButton()
    }

    @objc
    private func didTapToMenuButton() {
        output?.didTapToMenuButton()
    }

    @objc
    private func didTapAddPrimitiveButton() {
        output?.didTapAddPrimitiveButton()
    }

    @objc
    private func didTapAddSnippetButton() {
        output?.didTapAddSnippetButton()
    }

    @objc
    private func canvasDidTap(_ sender: UITapGestureRecognizer) {
        output?.didTapCanvas(location: sender.location(in: canvasView))
    }

    @objc
    private func didTapPlusScaleButton() {
        output?.didTapPlusScaleButton()
    }

    @objc
    private func didTapMinusScaleButton() {
        output?.didTapMinusScaleButton()
    }
}

extension CanvasViewController: CanvasViewInput {
    func setupProjectTitleLabel(with title: String) {
        projectTitleLabel.text = title
    }

    func updateCanvas() {
        canvasView.setNeedsDisplay()
        canvasView.layoutIfNeeded()
    }

    func setProject(name: String, authorName: String) {
        canvasView.setProjectName(name: name)
        canvasView.setAuthorName(name: authorName)
        canvasView.setType(type: .project)
    }

    func getCanvasCenter() -> CGPoint {
        return CGPoint(x: canvasView.bounds.width / 2, y: canvasView.bounds.height / 2)
    }
}

extension CanvasViewController {
    private struct Constants {

        static let cornerRadius: CGFloat = 10

        struct ProjectTitleLabel {
            static let font: UIFont? = UIFont(name: "DMSans-Regular", size: 25)
        }

        struct ToMenuButton {
            static let backgroundColor: UIColor = UIColor(red: 94 / 255,
                                                          green: 92 / 255,
                                                          blue: 230 / 255,
                                                          alpha: 1)

            static let fontColor: UIColor = .white
            static let title: String = "To menu"
            static let font: UIFont? = UIFont(name: "DMSans-Regular", size: 15)
        }

        struct AddPrimitiveButton {
            static let backgroundColor: UIColor = UIColor(red: 94 / 255,
                                                          green: 92 / 255,
                                                          blue: 230 / 255,
                                                          alpha: 1)

            static let font: UIFont? = UIFont(name: "DMSans-Regular", size: 15)
            static let fontColor: UIColor = .white
            static let cornerRadius: CGFloat = 10

            static let title: String = "+ Primitive"
        }

        struct AddSnippetButton {
            static let backgroundColor: UIColor = UIColor(red: 94 / 255,
                                                          green: 92 / 255,
                                                          blue: 230 / 255,
                                                          alpha: 1)

            static let font: UIFont? = UIFont(name: "DMSans-Regular", size: 15)
            static let fontColor: UIColor = .white
            static let cornerRadius: CGFloat = 10

            static let title: String = "+ Snippet"
        }

        struct ScaleButton {
            static let backgroundColor: UIColor = UIColor(red: 94 / 255,
                                                          green: 92 / 255,
                                                          blue: 230 / 255,
                                                          alpha: 1)

            static let font: UIFont? = UIFont(name: "DMSans-Regular", size: 15)
            static let fontColor: UIColor = .white
            static let cornerRadius: CGFloat = 10
        }
    }
}
