import simd

struct MetalVertex {
    var position: SIMD3<Float>

    var color: SIMD4<Float>
}
