import UIKit

final class CanvasContainer {
    let viewController: UIViewController!

    class func assemble(context: CanvasContext) -> CanvasContainer {
        let storyboard = UIStoryboard(name: Storyboard.canvas.name, bundle: nil)

        guard let viewController = storyboard.instantiateViewController(withIdentifier: Storyboard.canvas.name) as? CanvasViewController else {
            fatalError("CanvasContainer: view controller must be type CanvasViewController")
        }

        let presenter = CanvasPresenter()
        let router = CanvasRouter()
        let interactor = CanvasInteractor(model: CanvasData(projectName: context.projectName, authorName: context.authorName))

        presenter.view = viewController
        presenter.router = router
        presenter.interactor = interactor

        router.viewController = viewController
        viewController.output = presenter
        interactor.output = presenter

        return CanvasContainer(viewController: viewController)
    }

    private init(viewController: CanvasViewController) {
        self.viewController = viewController
    }
}

struct CanvasContext {
    var projectName: String
    var authorName: String
}
