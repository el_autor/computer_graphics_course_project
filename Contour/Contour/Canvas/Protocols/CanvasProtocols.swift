import UIKit

protocol CanvasViewInput: AnyObject {
    func setupProjectTitleLabel(with title: String)

    func updateCanvas()

    func setProject(name: String, authorName: String)

    func getCanvasCenter() -> CGPoint
}

protocol CanvasViewOutput: AnyObject {
    func didTapToMenuButton()

    func didTapAddPrimitiveButton()

    func didTapAddSnippetButton()

    func didLoadView()

    func didAddedPrimitive(_ primitive: Shape)

    func didTapCanvas(location: CGPoint)

    func didTapPlusScaleButton()

    func didTapMinusScaleButton()
}

protocol CanvasRouterInput: AnyObject {
    func showMenuScreen()

    func showAddPrimitiveScreen()

    func showObjectList(objects: [ObjectWrapper])

    func showAllSnippetsScreen(projectName: String, authorName: String)
}

protocol CanvasInteractorInput: AnyObject {
    func getProjectName() -> String?

    func getAuthorName() -> String?

    func addPrimitive(_ primitiveName: String)

    func didTapCanvas(location: CGPoint)

    func plusScaleProject(center: Point)

    func minusScaleProject(center: Point)
}

protocol CanvasInteractorOutput: AnyObject {
    func showObjectList(objects: [ObjectWrapper])

    func updateCanvas()
}
