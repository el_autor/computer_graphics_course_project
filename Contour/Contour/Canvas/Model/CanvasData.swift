import RealmSwift

struct CanvasData {
    var projectName: String?
    var authorName: String?
}

struct ObjectWrapper {
    var object: Object
    var type: ObjectType
}

enum ObjectType {
    case vertex
    case primitive
}
