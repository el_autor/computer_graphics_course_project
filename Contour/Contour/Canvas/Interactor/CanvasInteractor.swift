import UIKit
import RealmSwift

final class CanvasInteractor {
    weak var output: CanvasInteractorOutput?

    private var model: CanvasData

    init(model: CanvasData) {
        self.model = model
    }

    private func pointInPolygon(position: CGPoint, connections: [Connection]) -> Bool {
        var plus = false
        var minus = false

        for connection in connections {
            let part1: Float = (connection.endVertex.x - connection.startVertex.x) * (Float(position.y) - connection.startVertex.y)
            let part2: Float = (connection.endVertex.y - connection.startVertex.y) * (Float(position.x) - connection.startVertex.x)
            let product: Float = part1 - part2

            if product >= 0 {
                plus = true
            } else {
                minus = true
            }
        }

        if plus && minus {
            return false
        }

        return true
    }

    private func distanceBetweenPoints(pointA: CGPoint, pointB: CGPoint) -> CGFloat {
        return sqrt((pointA.x - pointB.x) * (pointA.x - pointB.x) + (pointA.y - pointB.y) * (pointA.y - pointB.y))
    }

    private func primitiveIsUnique(primitive: Primitive, objects: [ObjectWrapper]) -> Bool {
        for object in objects where object.type == .primitive {
            guard let primitiveObject = object.object as? Primitive else {
                return false
            }

            if primitive.id == primitiveObject.id {
                return false
            }
        }

        return true
    }

    private func vertexIsUnique(vertex: Vertex, objects: [ObjectWrapper]) -> Bool {
        for object in objects where object.type == .vertex {
            guard let vertexObject = object.object as? Vertex else {
                return false
            }

            if vertex.id == vertexObject.id {
                return false
            }
        }

        return true
    }

    private func degenerateVerge(connections: [Connection]) -> Bool {
        guard let vertexA = connections[0].startVertex,
              let vertexB = connections[1].startVertex,
              let vertexC = connections[2].startVertex else {
            return true
        }

        if llroundf(vertexA.x) == llroundf(vertexB.x) &&
            llroundf(vertexA.y) == llroundf(vertexB.y) {
            return true
        }

        if llroundf(vertexB.x) == llroundf(vertexC.x) &&
            llroundf(vertexB.y) == llroundf(vertexC.y) {
            return true
        }

        if llroundf(vertexC.x) == llroundf(vertexA.x) &&
            llroundf(vertexC.y) == llroundf(vertexA.y) {
            return true
        }

        return false
    }

    private func getClosestObjects(maxDistance: CGFloat, touchPosition: CGPoint) -> [ObjectWrapper] {
        guard let projectName = model.projectName,
              let authorName = model.authorName else {
            return []
        }

        var closestObjects: [ObjectWrapper] = []

        let primitives = DataService.shared.getAllPrimitives(projectName: projectName, authorName: authorName)

        for primitive in primitives {
            for verge in primitive.verges {
                if degenerateVerge(connections: Array(verge.connections)) {
                    continue
                }

                if pointInPolygon(position: touchPosition, connections: Array(verge.connections)) && primitiveIsUnique(primitive: primitive, objects: closestObjects) {
                    closestObjects.append(ObjectWrapper(object: primitive, type: .primitive))
                }

                for connection in verge.connections {
                    if distanceBetweenPoints(pointA: touchPosition,
                                             pointB: CGPoint(x: CGFloat(connection.startVertex.x), y: CGFloat(connection.startVertex.y))) <= maxDistance && vertexIsUnique(vertex: connection.startVertex, objects: closestObjects) {
                        closestObjects.append(ObjectWrapper(object: connection.startVertex, type: .vertex))
                    }

                    if distanceBetweenPoints(pointA: touchPosition,
                                                    pointB: CGPoint(x: CGFloat(connection.endVertex.x), y: CGFloat(connection.endVertex.y))) <= maxDistance && vertexIsUnique(vertex: connection.endVertex, objects: closestObjects) {
                        closestObjects.append(ObjectWrapper(object: connection.endVertex, type: .vertex))
                    }
                }
            }
        }

        return closestObjects
    }
}

extension CanvasInteractor: CanvasInteractorInput {
    func addPrimitive(_ primitiveName: String) {
        guard let projectName = model.projectName,
              let authorName = model.authorName else {
            return
        }

        var newPrimitive: Primitive?

        switch primitiveName {
        case "Cube":
            newPrimitive = DataService.shared.createCube(center: CGPoint(x: 700, y: 500),
                                                         side: 100)
        case "Pyramid":
            newPrimitive = DataService.shared.createPyramid(center: CGPoint(x: 700, y: 500),
                                                            height: 200,
                                                            baseSide: 100)
        case "Facet":
            newPrimitive = DataService.shared.createFacet(pointA: Point(x: 650, y: 450, z: 0),
                                                          pointB: Point(x: 750, y: 450, z: 0),
                                                          pointC: Point(x: 700, y: 550, z: 0))
        case "Sphere":
            newPrimitive = DataService.shared.createSphere(center: Point(x: 700, y: 500, z: 0),
                                                           radius: 100)
        case "Cone":
            newPrimitive = DataService.shared.createCone(center: Point(x: 700, y: 500, z: 0),
                                                         height: 200,
                                                         baseRadius: 100)
        default:
            ()
        }

        guard let primitive = newPrimitive else {
            return
        }

        DataService.shared.addPrimitive(projectName: projectName, authorName: authorName, primitive: primitive)

        output?.updateCanvas()
    }

    func getProjectName() -> String? {
        return model.projectName
    }

    func getAuthorName() -> String? {
        return model.authorName
    }

    func didTapCanvas(location: CGPoint) {
        let objects = getClosestObjects(maxDistance: Constants.maxDistance, touchPosition: location)

        if !objects.isEmpty {
            output?.showObjectList(objects: objects)
        }
    }

    func plusScaleProject(center: Point) {
        guard let projectName = model.projectName,
              let authorName = model.authorName else {
            return
        }

        DataService.shared.scaleProject(projectName: projectName,
                                        authorName: authorName,
                                        scaleFactor: Constants.plusScaleFactor,
                                        center: center)
    }

    func minusScaleProject(center: Point) {
        guard let projectName = model.projectName,
              let authorName = model.authorName else {
            return
        }

        DataService.shared.scaleProject(projectName: projectName,
                                        authorName: authorName,
                                        scaleFactor: Constants.minusScaleFactor,
                                        center: center)
    }
}

extension CanvasInteractor {
    private struct Constants {
        static let maxDistance: CGFloat = 5

        static let plusScaleFactor: Float = 2

        static let minusScaleFactor: Float = 0.5
    }
}
