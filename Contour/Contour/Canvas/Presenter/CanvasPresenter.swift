import UIKit

final class CanvasPresenter {
    weak var view: CanvasViewInput?

    var router: CanvasRouterInput?

    var interactor: CanvasInteractorInput?

    init() {

    }
}

extension CanvasPresenter: CanvasViewOutput {
    func didTapToMenuButton() {
        router?.showMenuScreen()
    }

    func didTapAddPrimitiveButton() {
        router?.showAddPrimitiveScreen()
    }

    func didTapAddSnippetButton() {
        guard let name = interactor?.getProjectName(),
              let authorName = interactor?.getAuthorName() else {
            return
        }

        router?.showAllSnippetsScreen(projectName: name,
                                      authorName: authorName)
    }

    func didLoadView() {
        guard let name = interactor?.getProjectName(),
              let authorName = interactor?.getAuthorName() else {
            return
        }

        view?.setupProjectTitleLabel(with: name)
        view?.setProject(name: name, authorName: authorName)
    }

    func didAddedPrimitive(_ primitive: Shape) {
        interactor?.addPrimitive(primitive.name ?? String())
    }

    func didTapCanvas(location: CGPoint) {
        interactor?.didTapCanvas(location: location)
    }

    func didTapPlusScaleButton() {
        guard let center = view?.getCanvasCenter() else {
            return
        }

        interactor?.plusScaleProject(center: Point(x: Float(center.x), y: Float(center.y), z: 0))
        view?.updateCanvas()
    }

    func didTapMinusScaleButton() {
        guard let center = view?.getCanvasCenter() else {
            return
        }

        interactor?.minusScaleProject(center: Point(x: Float(center.x), y: Float(center.y), z: 0))
        view?.updateCanvas()
    }
}

extension CanvasPresenter: CanvasInteractorOutput {
    func updateCanvas() {
        view?.updateCanvas()
    }

    func showObjectList(objects: [ObjectWrapper]) {
        router?.showObjectList(objects: objects)
    }
}
