import UIKit

final class CanvasRouter {
    weak var viewController: UIViewController?
}

extension CanvasRouter: CanvasRouterInput {
    func showMenuScreen() {
        let menuVC = viewController?.presentingViewController?.presentingViewController

        menuVC?.dismiss(animated: false, completion: nil)
    }

    func showAddPrimitiveScreen() {
        let container = AddPrimitiveContainer.assemble()

        container.viewController.modalPresentationStyle = .fullScreen

        viewController?.present(container.viewController, animated: true, completion: nil)
    }

    func showObjectList(objects: [ObjectWrapper]) {
        let container = ObjectListContainer.assemble(context: ObjectListContext(closestObjects: objects))

        let navigationVC = UINavigationController(rootViewController: container.viewController)

        viewController?.present(navigationVC, animated: true, completion: nil)
    }

    func showAllSnippetsScreen(projectName: String, authorName: String) {
        let container = AddSnippetContainer.assemble(with: AddSnippetContext(projectName: projectName,
                                                                             authorName: authorName))

        let navigationVC = UINavigationController(rootViewController: container.viewController)
        navigationVC.isNavigationBarHidden = true

        viewController?.present(navigationVC, animated: true, completion: nil)
    }
}
