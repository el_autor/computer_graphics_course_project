import Foundation
import CoreGraphics

protocol SnippetCanvasViewInput: AnyObject {
    func setSnippetNameLabelText(text: String)

    func setSnippet(name: String, authorName: String)

    func updateCanvas()

    func getCanvasCenter() -> CGPoint
}

protocol SnippetCanvasViewOutput: AnyObject {
    func didLoadView()

    func didTapToMenuButton()

    func didTapAddFacetButton()

    func didTapPlusScaleButton()

    func didTapMinusScaleButton()
}

protocol SnippetCanvasInteractorInput: AnyObject {
    func getSnippetName() -> String

    func getAuthorName() -> String

    func plusScaleSnippet(center: Point)

    func minusScaleSnippet(center: Point)
}

protocol SnippetCanvasInteractorOutput: AnyObject {
}

protocol SnippetCanvasRouterInput: AnyObject {
    func showMenuScreen()

    func showCreateFacetScreen(name: String, authorName: String)
}
