import Foundation

final class SnippetCanvasInteractor {
	weak var output: SnippetCanvasInteractorOutput?

    private var model: CanvasData

    init(model: CanvasData) {
        self.model = model
    }
}

extension SnippetCanvasInteractor: SnippetCanvasInteractorInput {
    func getSnippetName() -> String {
        return model.projectName ?? "Default"
    }

    func getAuthorName() -> String {
        return model.authorName ?? "Default"
    }

    func plusScaleSnippet(center: Point) {
        DataService.shared.scaleSnippet(name: model.projectName ?? "Default",
                                        authorName: model.authorName ?? "Default",
                                        scaleFactor: 2,
                                        center: center)
    }

    func minusScaleSnippet(center: Point) {
        DataService.shared.scaleSnippet(name: model.projectName ?? "Default",
                                        authorName: model.authorName ?? "Default",
                                        scaleFactor: 0.5,
                                        center: center)
    }
}
