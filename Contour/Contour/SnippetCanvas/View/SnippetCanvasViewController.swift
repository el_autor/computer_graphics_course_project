import UIKit
import PinLayout

final class SnippetCanvasViewController: UIViewController {
	var output: SnippetCanvasViewOutput?

    // MARK: UI elements

    private weak var canvasView: DrawableView!

    private weak var snippetTitleLabel: UILabel!

    private weak var toMenuButton: UIButton!

    private weak var addFacetButton: UIButton!

    private weak var plusScaleButton: UIButton!

    private weak var minusScaleButton: UIButton!

	override func viewDidLoad() {
		super.viewDidLoad()

        setupView()
        setupSubviews()

        output?.didLoadView()
	}

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        layoutCanvasView()
        layoutSnippetTitleLabel()
        layoutToMenuButton()
        layoutMinusScaleButton()
        layoutPlusScaleButton()
        layoutAddFacetButton()
    }

    private func setupView() {
        view.backgroundColor = .white
    }

    private func setupSubviews() {
        setupCanvasView()
        setupSnippetTitleLabel()
        setupToMenuButton()
        setupAddFacetButton()
        setupPlusScaleButton()
        setupMinusScaleButton()
    }

    private func setupCanvasView() {
        let canvas = DrawableView(frame: .zero, device: nil)

        canvasView = canvas
        view.addSubview(canvasView)
    }

    private func setupSnippetTitleLabel() {
        let label = UILabel()

        snippetTitleLabel = label
        canvasView.addSubview(snippetTitleLabel)

        snippetTitleLabel.textAlignment = .center
        snippetTitleLabel.font = UIFont(name: "DMSans-Regular", size: 25)
    }

    private func setupToMenuButton() {
        let button = UIButton()

        toMenuButton = button
        view.addSubview(toMenuButton)

        toMenuButton.backgroundColor = UIColor(red: 94 / 255,
                                               green: 92 / 255,
                                               blue: 230 / 255,
                                               alpha: 1)
        toMenuButton.setTitleColor(.white, for: .normal)
        toMenuButton.setTitle("To menu", for: .normal)
        toMenuButton.titleLabel?.font = UIFont(name: "DMSans-Regular", size: 15)
        toMenuButton.layer.cornerRadius = 10

        toMenuButton.addTarget(self, action: #selector(didTapToMenuButton), for: .touchUpInside)
    }

    private func setupAddFacetButton() {
        let button = UIButton()

        addFacetButton = button
        view.addSubview(addFacetButton)

        addFacetButton.backgroundColor = UIColor(red: 94 / 255,
                                               green: 92 / 255,
                                               blue: 230 / 255,
                                               alpha: 1)
        addFacetButton.setTitleColor(.white, for: .normal)
        addFacetButton.setTitle("+ facet", for: .normal)
        addFacetButton.titleLabel?.font = UIFont(name: "DMSans-Regular", size: 15)
        addFacetButton.layer.cornerRadius = 10

        addFacetButton.addTarget(self, action: #selector(didTapAddFacetButton), for: .touchUpInside)
    }

    private func setupPlusScaleButton() {
        let button = UIButton()

        plusScaleButton = button
        view.addSubview(plusScaleButton)

        plusScaleButton.backgroundColor = UIColor(red: 94 / 255,
                                               green: 92 / 255,
                                               blue: 230 / 255,
                                               alpha: 1)
        plusScaleButton.setTitleColor(.white, for: .normal)
        plusScaleButton.setTitle("+", for: .normal)
        plusScaleButton.titleLabel?.font = UIFont(name: "DMSans-Regular", size: 15)
        plusScaleButton.layer.cornerRadius = 10

        plusScaleButton.addTarget(self, action: #selector(didTapPlusScaleButton), for: .touchUpInside)
    }

    private func setupMinusScaleButton() {
        let button = UIButton()

        minusScaleButton = button
        view.addSubview(minusScaleButton)

        minusScaleButton.backgroundColor = UIColor(red: 94 / 255,
                                               green: 92 / 255,
                                               blue: 230 / 255,
                                               alpha: 1)
        minusScaleButton.setTitleColor(.white, for: .normal)
        minusScaleButton.setTitle("-", for: .normal)
        minusScaleButton.titleLabel?.font = UIFont(name: "DMSans-Regular", size: 15)
        minusScaleButton.layer.cornerRadius = 10

        minusScaleButton.addTarget(self, action: #selector(didTapMinusScaleButton), for: .touchUpInside)
    }

    private func layoutCanvasView() {
        canvasView.pin
            .top()
            .left()
            .right()
            .bottom(7%)
    }

    private func layoutSnippetTitleLabel() {
        snippetTitleLabel.pin
            .top(5%)
            .hCenter()
            .height(5%)
            .width(50%)
    }

    private func layoutToMenuButton() {
        toMenuButton.pin
            .left(3%)
            .height(3%)
            .width(9%)
            .bottom(2%)
    }

    private func layoutAddFacetButton() {
        addFacetButton.pin
            .left(of: plusScaleButton).marginRight(5%)
            .height(3%)
            .width(9%)
            .bottom(2%)
    }

    private func layoutPlusScaleButton() {
        plusScaleButton.pin
            .left(of: minusScaleButton)
            .height(3%)
            .width(4%)
            .bottom(2%)
    }

    private func layoutMinusScaleButton() {
        minusScaleButton.pin
            .right(3%)
            .height(3%)
            .width(4%)
            .bottom(2%)
    }

    @objc
    private func didTapToMenuButton() {
        output?.didTapToMenuButton()
    }

    @objc
    private func didTapAddFacetButton() {
        output?.didTapAddFacetButton()
    }

    @objc
    private func didTapPlusScaleButton() {
        output?.didTapPlusScaleButton()
    }

    @objc
    private func didTapMinusScaleButton() {
        output?.didTapMinusScaleButton()
    }
}

extension SnippetCanvasViewController: SnippetCanvasViewInput {
    func setSnippetNameLabelText(text: String) {
        snippetTitleLabel.text = text
    }

    func setSnippet(name: String, authorName: String) {
        canvasView.setProjectName(name: name)
        canvasView.setAuthorName(name: authorName)
        canvasView.setType(type: .snippet)
    }

    func updateCanvas() {
        canvasView.setNeedsDisplay()
        canvasView.layoutIfNeeded()
    }

    func getCanvasCenter() -> CGPoint {
        return CGPoint(x: canvasView.bounds.width / 2, y: canvasView.bounds.height / 2)
    }
}
