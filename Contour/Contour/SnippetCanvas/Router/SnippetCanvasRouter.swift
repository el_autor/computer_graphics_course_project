import UIKit

final class SnippetCanvasRouter {
    weak var viewController: UIViewController?
}

extension SnippetCanvasRouter: SnippetCanvasRouterInput {
    func showMenuScreen() {
        let menuVC = viewController?.presentingViewController?.presentingViewController

        menuVC?.dismiss(animated: false, completion: nil)
    }

    func showCreateFacetScreen(name: String, authorName: String) {
        let container = AddFacetContainer.assemble(with: AddFacetContext(snippetName: name, authorName: authorName))

        let navigationVC = UINavigationController(rootViewController: container.viewController)
        navigationVC.isNavigationBarHidden = true

        viewController?.present(navigationVC, animated: true, completion: nil)
    }
}
