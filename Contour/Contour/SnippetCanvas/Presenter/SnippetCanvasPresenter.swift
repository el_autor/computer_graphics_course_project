import Foundation

final class SnippetCanvasPresenter {
	weak var view: SnippetCanvasViewInput?

	private let router: SnippetCanvasRouterInput
	private let interactor: SnippetCanvasInteractorInput

    init(router: SnippetCanvasRouterInput, interactor: SnippetCanvasInteractorInput) {
        self.router = router
        self.interactor = interactor
    }
}

extension SnippetCanvasPresenter: SnippetCanvasViewOutput {
    func didLoadView() {
        let name = interactor.getSnippetName()
        let authorName = interactor.getAuthorName()

        view?.setSnippetNameLabelText(text: interactor.getSnippetName())
        view?.setSnippet(name: name, authorName: authorName)
    }

    func didTapToMenuButton() {
        router.showMenuScreen()
    }

    func didTapAddFacetButton() {
        router.showCreateFacetScreen(name: interactor.getSnippetName(),
                                     authorName: interactor.getAuthorName())
    }

    func didTapPlusScaleButton() {
        guard let center = view?.getCanvasCenter() else {
            return
        }

        interactor.plusScaleSnippet(center: Point(x: Float(center.x), y: Float(center.y), z: 0))
        view?.updateCanvas()
    }

    func didTapMinusScaleButton() {
        guard let center = view?.getCanvasCenter() else {
            return
        }

        interactor.minusScaleSnippet(center: Point(x: Float(center.x), y: Float(center.y), z: 0))
        view?.updateCanvas()
    }
}

extension SnippetCanvasPresenter: SnippetCanvasInteractorOutput {
}
