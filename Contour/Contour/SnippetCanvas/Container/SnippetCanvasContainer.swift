import UIKit

final class SnippetCanvasContainer {
	let viewController: UIViewController
	private(set) weak var router: SnippetCanvasRouterInput!

	class func assemble(with context: SnippetCanvasContext) -> SnippetCanvasContainer {
        let router = SnippetCanvasRouter()
        let interactor = SnippetCanvasInteractor(model: CanvasData(projectName: context.name,
                                                                   authorName: context.authorName))
        let presenter = SnippetCanvasPresenter(router: router, interactor: interactor)
	let viewController = SnippetCanvasViewController()

        viewController.output = presenter
	presenter.view = viewController
	interactor.output = presenter
	router.viewController = viewController

        return SnippetCanvasContainer(view: viewController, router: router)
	}

    private init(view: UIViewController, router: SnippetCanvasRouterInput) {
		self.viewController = view
		self.router = router
	}
}

struct SnippetCanvasContext {
    var name: String

    var authorName: String
}
