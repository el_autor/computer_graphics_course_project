import Foundation

class CreateProjectPresenter {
    weak var view: CreateProjectViewInput?
    var router: CreateProjectRouterInput?
    var interactor: CreateProjectInteractorInput?

    init() {

    }
}

extension CreateProjectPresenter: CreateProjectViewOutput {
    func didTapToMenuButton() {
        router?.showMenuScreen()
    }

    func didTapCreateButton(projectName: String?, authorName: String?) {
        guard let projectName = projectName,
              !projectName.isEmpty else {
            view?.showAlert(with: "Oops!", message: "Can't get project name")
            return
        }

        guard let authorName = authorName,
              !authorName.isEmpty else {
            view?.showAlert(with: "Oops!", message: "Can't get author name")
            return
        }

        interactor?.createNewProject(name: projectName, authorName: authorName)
    }

    func didTapView() {
        view?.disableKeyboard()
    }
}

extension CreateProjectPresenter: CreateProjectInteractorOutput {
    func showAlert(with title: String, message: String) {
        view?.showAlert(with: title, message: message)
    }

    func projectSuccesfullyCreated(projectName: String, authorName: String) {
        router?.showCanvas(projectName: projectName, authorName: authorName)
    }
}
