protocol CreateProjectViewInput: class {
    func disableKeyboard()

    func showAlert(with: String, message: String)
}

protocol CreateProjectViewOutput: class {
    func didTapToMenuButton()

    func didTapCreateButton(projectName: String?, authorName: String?)

    func didTapView()
}

protocol CreateProjectRouterInput: class {
    func showMenuScreen()

    func showCanvas(projectName: String, authorName: String)
}

protocol CreateProjectInteractorInput: class {
    func createNewProject(name: String, authorName: String)
}

protocol CreateProjectInteractorOutput: class {
    func showAlert(with title: String, message: String)

    func projectSuccesfullyCreated(projectName: String, authorName: String)
}
