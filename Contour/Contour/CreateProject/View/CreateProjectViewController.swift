import UIKit

class CreateProjectViewController: UIViewController {

    var output: CreateProjectViewOutput?

    @IBOutlet private weak var nameTextField: UITextField!

    @IBOutlet private weak var authorNameTextField: UITextField!

    @IBOutlet private weak var createButton: UIButton!

    @IBOutlet private weak var toMenuButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupSubviews()
    }

    private func setupView() {
        let viewTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapView))

        viewTapRecognizer.numberOfTapsRequired = Constants.tapsRequired
        view.addGestureRecognizer(viewTapRecognizer)

        view.backgroundColor = Constants.backgroundColor
    }

    private func setupNameTextField() {
        nameTextField.attributedPlaceholder = NSAttributedString(string: Constants.NameTextField.placeholderText,
                                                                 attributes: [NSAttributedString.Key.foregroundColor: Constants.textColor])

        nameTextField.backgroundColor = Constants.NameTextField.backgroundColor

        nameTextField.clipsToBounds = true
        nameTextField.layer.cornerRadius = Constants.cornerRadius
    }

    private func setupAuthorNameTextField() {
        authorNameTextField.attributedPlaceholder = NSAttributedString(string: Constants.AuthorNameTextField.placeholderText,
                                                                 attributes: [NSAttributedString.Key.foregroundColor: Constants.textColor])

        authorNameTextField.backgroundColor = Constants.AuthorNameTextField.backgroundColor

        authorNameTextField.clipsToBounds = true
        authorNameTextField.layer.cornerRadius = Constants.cornerRadius
    }

    private func setupCreateButton() {
        createButton.backgroundColor = Constants.CreateButton.backgroundColor
        createButton.setTitle(Constants.CreateButton.title, for: .normal)

        createButton.layer.cornerRadius = Constants.cornerRadius

        createButton.titleLabel?.font = Constants.font
        createButton.setTitleColor(Constants.textColor, for: .normal)

        createButton.addTarget(self, action: #selector(didTapCreateButton), for: .touchUpInside)
    }

    private func setupToMenuButton() {
        toMenuButton.backgroundColor = Constants.ToMenuButton.backgroundColor
        toMenuButton.setTitle(Constants.ToMenuButton.title, for: .normal)

        toMenuButton.layer.cornerRadius = Constants.cornerRadius

        toMenuButton.titleLabel?.font = Constants.font
        toMenuButton.setTitleColor(Constants.textColor, for: .normal)

        toMenuButton.addTarget(self, action: #selector(didTapToMenuButton), for: .touchUpInside)
    }

    private func setupSubviews() {
        setupNameTextField()
        setupAuthorNameTextField()
        setupCreateButton()
        setupToMenuButton()
    }

    @objc
    private func didTapToMenuButton() {
        output?.didTapToMenuButton()
    }

    @objc
    private func didTapCreateButton() {
        output?.didTapCreateButton(projectName: nameTextField.text, authorName: authorNameTextField.text)
    }

    @objc
    private func didTapView() {
        output?.didTapView()
    }

    deinit {
        print("Create project vc deinited")
    }
}

extension CreateProjectViewController: CreateProjectViewInput {
    func disableKeyboard() {
        view.endEditing(true)
    }

    func showAlert(with: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "ОК", style: .default, handler: nil))

        present(alert, animated: true)
    }
}

extension CreateProjectViewController {
    private struct Constants {
        static let backgroundColor: UIColor = UIColor(red: 94 / 255,
                                                      green: 92 / 255,
                                                      blue: 230 / 255,
                                                      alpha: 1)

        static let cornerRadius: CGFloat = 15
        static let font: UIFont? = UIFont(name: "DMSans-Regular", size: 25)
        static let textColor: UIColor = .black
        static let tapsRequired: Int = 1

        struct NameTextField {
            static let backgroundColor: UIColor = .white
            static let placeholderText: String = String(repeating: " ", count: 5) + "Name"
        }

        struct AuthorNameTextField {
            static let backgroundColor: UIColor = .white
            static let placeholderText: String = String(repeating: " ", count: 5) + "Author"
        }

        struct CreateButton {
            static let backgroundColor: UIColor = UIColor(red: 1,
                                                          green: 214 / 255,
                                                          blue: 10 / 255,
                                                          alpha: 1)

            static let title: String = "Create"
        }

        struct ToMenuButton {
            static let backgroundColor: UIColor = UIColor(red: 1,
                                                          green: 214 / 255,
                                                          blue: 10 / 255,
                                                          alpha: 1)

            static let title: String = "To menu"
        }

    }
}
