class CreateProjectInteractor {
    weak var output: CreateProjectInteractorOutput?

    init() {

    }
}

extension CreateProjectInteractor: CreateProjectInteractorInput {
    func createNewProject(name: String, authorName: String) {
        guard DataService.shared.projectIsUnique(name: name, authorName: authorName) else {
            output?.showAlert(with: "Oops!", message: "Such project already exists")
            return
        }

        DataService.shared.createProject(name: name, authorName: authorName)

        output?.projectSuccesfullyCreated(projectName: name, authorName: authorName)
    }
}
