import UIKit

class CreateProjectContainer {
    let viewController: UIViewController

    class func assemble() -> CreateProjectContainer {
        let storyboard = UIStoryboard(name: Storyboard.createProject.name, bundle: nil)

        guard let viewController = storyboard.instantiateViewController(withIdentifier: Storyboard.createProject.name) as? CreateProjectViewController else {
            fatalError("CreateProjectContainer: viewController must be type CreateProjectViewController")
        }

        let presenter = CreateProjectPresenter()
        let router = CreateProjectRouter()
        let interactor = CreateProjectInteractor()

        presenter.view = viewController
        presenter.router = router
        presenter.interactor = interactor

        router.viewController = viewController

        viewController.output = presenter

        interactor.output = presenter

        return CreateProjectContainer(viewController: viewController)
    }

    private init(viewController: CreateProjectViewController) {
        self.viewController = viewController
    }
}
