import UIKit

class CreateProjectRouter {
    weak var viewController: UIViewController?
}

extension CreateProjectRouter: CreateProjectRouterInput {
    func showMenuScreen() {
        viewController?.dismiss(animated: true, completion: nil)
    }

    func showCanvas(projectName: String, authorName: String) {
        let container = CanvasContainer.assemble(context: CanvasContext(projectName: projectName,
                                                                        authorName: authorName))

        container.viewController.modalPresentationStyle = .fullScreen

        viewController?.present(container.viewController, animated: true, completion: nil)
    }
}
